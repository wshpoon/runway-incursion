import { Component, Input, OnInit } from "@angular/core";
import * as numeral from "numeral";

@Component({
  selector: "app-dy-scatter-chart",
  templateUrl: "./dy-scatter-chart.component.html",
  styleUrls: ["./dy-scatter-chart.component.scss"],
})
export class DyScatterChartComponent implements OnInit {
  private _data = [];
  private _options = {
    width: "100%",
    height: 600,
    title: "",
    ylabel: "",
    xlabel: "",
    drawPoints: true,
    drawAxesAtZero: true,
    strokeWidth: 0.0,
    labels: [],
    axes: {
      y: {
        // valueFormatter: (value: any) => {
        //   return !!Number(value) ? numeral(value).format("0.0a") : value;
        // },
      },
      x: {
        // valueFormatter: (value: any) => {
        //   return !!Number(value) ? numeral(value).format("0.0a") : value;
        // },
      },
    },
  };

  @Input("data") set data(value: any[]) {
    this._data = value;
  }

  @Input("title") set title(value: string) {
    this._options.title = value;
  }

  @Input("labels") set labels(value: string[]) {
    this._options.labels = value;
  }

  @Input("height") set height(value: any) {
    this._options.height = value;
  }

  @Input("width") set width(value: any) {
    this._options.width = value;
  }

  @Input("options") set options(value: object) {
    this._options = {
      ...this._options,
      ...value
    };
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this._options.ylabel = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this._options.xlabel = value;
  }

  @Input("x-axis-number-format") set formatXAxisLabel(value: boolean) {
    if (value) {
      this._options.axes.x = {
        ...this._options.axes.x,
        ...{
          axisLabelFormatter: (value: any) => {
            return numeral(value).format("0.0a");
          },
        },
      };
    }
  }

  @Input("y-axis-number-format") set formatYAxisLabel(value: boolean) {
    if (value) {
      this._options.axes.y = {
        ...this._options.axes.y,
        ...{
          axisLabelFormatter: (value: any) => {
            return numeral(value).format("0.0a");
          },
        },
      };
    }
  }

  get data() {
    return this._data;
  }

  get options() {
    return this._options;
  }

  constructor() {
    // where first element in each array element will be for x-axis label
    // then the other elements would be on different series / group
    this._data = [
      [215, 13, 155, 123, 176],
      [445, 23, 255, 124, 23],
      [135, 24, 355, 126, 44],
      [115, 55, 454, 111, 5],
      [15, 160, 444, 1001, 66],
      [66, 76, 333, 555, 77],
      [25, 772, 112, 233, 223],
      [35, 55, 333, 557, 44],
      [20, 15, 112, 888, 11],
    ];

    this._options = {
      ...this._options,
      ...{
        // rollPeriod: 7,
        labels: ["X", "GREAT 552", "OSEM 123", "TEST 124", "STARWARS 555"],
      },
    };
  }

  ngOnInit() {}
}
