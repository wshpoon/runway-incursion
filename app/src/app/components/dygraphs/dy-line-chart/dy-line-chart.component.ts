import { Component, Input, OnInit } from "@angular/core";

import * as numeral from "numeral";

@Component({
  selector: "app-dy-line-chart",
  templateUrl: "./dy-line-chart.component.html",
  styleUrls: ["./dy-line-chart.component.scss"],
})
export class DyLineChartComponent implements OnInit {
  private _data = [];
  private _options = {
    width: "100%",
    height: 600,
    title: "",
    ylabel: "",
    xlabel: "",
    labels: [],
    highlightSeriesOpts: {
      strokeWidth: 3,
      strokeBorderWidth: 1,
      highlightCircleSize: 5,
    },
    axes: {
      y: {
        // valueFormatter: (value: any) => {
        //   return !!Number(value) ? numeral(value).format("0.0a") : value;
        // },
      },
      x: {
        // valueFormatter: (value: any) => {
        //   return !!Number(value) ? numeral(value).format("0.0a") : value;
        // },
      },
    },
  };

  @Input("data") set data(value: any[]) {
    this._data = value;
  }

  @Input("title") set title(value: string) {
    this._options.title = value;
  }

  @Input("labels") set labels(value: string[]) {
    this._options.labels = value;
  }

  @Input("height") set height(value: any) {
    this._options.height = value;
  }

  @Input("width") set width(value: any) {
    this._options.width = value;
  }

  @Input("options") set options(value) {
    this._options = value;
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this._options.ylabel = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this._options.xlabel = value;
  }

  @Input("x-axis-number-format") set formatXAxisLabel(value: boolean) {
    if (value) {
      this._options.axes.x = {
        ...this._options.axes.x,
        ...{
          axisLabelFormatter: (value: any) => {
            return numeral(value).format("0.0a");
          },
        },
      };
    }
  }

  @Input("y-axis-number-format") set formatYAxisLabel(value: boolean) {
    if (value) {
      this._options.axes.y = {
        ...this._options.axes.y,
        ...{
          axisLabelFormatter: (value: any) => {
            return numeral(value).format("0.0a");
          },
        },
      };
    }
  }

  get data() {
    return this._data;
  }

  get options() {
    return this._options;
  }

  constructor() {
    // where first element in each array element will be for x-axis label
    // then the other elements would be on different series / group
    this._data = [
      [new Date("2001-01-01"), 35000, 215000, 13000],
      [new Date("2001-01-02"), 45000, 445000, 23000],
      [new Date("2001-01-03"), 55000, 135000, 24000],
      [new Date("2001-01-04"), 25000, 115000, 55000],
      [new Date("2001-01-05"), 23000, 15000, 160000],
      [new Date("2001-01-06"), 24000, 66000, 76000],
      [new Date("2001-01-07"), 15000, 25000, 772000],
      [new Date("2001-01-08"), 12000, 35000, 55000],
      [new Date("2001-01-08"), 10000, 20000, 15000],
    ];

    this._options = {
      ...this._options,
      ...{
        // rollPeriod: 7,
        labels: ["Date", "GROUP A", "GROUP B", "GROUP C"],
      },
    };
  }

  ngOnInit() {}
}
