import { Component, OnInit, ViewChild, Input } from "@angular/core";

import * as numeral from "numeral";

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid,
  ApexLegend,
  ApexYAxis,
  ApexTheme,
} from "ng-apexcharts";
import { ThemeService } from "@app/services/theme.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
  legend?: ApexLegend;
  theme: ApexTheme;
};

@Component({
  selector: "app-apex-line-chart",
  templateUrl: "./apex-line-chart.component.html",
  styleUrls: ["./apex-line-chart.component.scss"],
})
export class ApexLineChartComponent implements OnInit {
  private _yAxisRoundOff = false;
  private _formatNumber = false;

  @Input("series") set series(value: ApexAxisChartSeries) {
    this.chartOptions.series = value;
  }

  @Input("categories") set categories(value: string[]) {
    this.chartOptions.xaxis.categories = value;
  }

  @Input("title") set title(value: string) {
    this.chartOptions.title.text = value;
  }

  @Input("height") set height(value: number) {
    this.chartOptions.chart.height = value;
  }

  @Input("width") set width(value: number) {
    this.chartOptions.chart.width = value;
  }

  @Input("x-axis-type") set xAxisType(
    value: "category" | "datetime" | "numeric"
  ) {
    this.chartOptions.xaxis.type = value;
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this.chartOptions.yaxis.title.text = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this.chartOptions.xaxis.title.text = value;
  }

  @Input("show-x-axis-labels") set showXAxisLabels(value: boolean) {
    this.chartOptions.xaxis.labels.show = value;
  }

  @Input("y-axis-round-off") set yAxisFormatter(value: boolean) {
    this._yAxisRoundOff = value;
  }

  @Input("format-number") set formatNumber(value: boolean) {
    this._formatNumber = value;
  }

  @ViewChild("lineChart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private themeService: ThemeService) {
    this.chartOptions = {
      series: [
        {
          name: "Desktops",
          data: [10, 41, 35, 51, 49, 62, 69, 91, 148],
        },
      ],
      // legend: {
      //   show: false
      // },
      chart: {
        height: 500,
        width: "100%",
        type: "line",
        zoom: {
          enabled: true,
        },
        animations: {
          enabled: false,
        },
        toolbar: {
          tools: {
            pan: true,
            download: true,
            reset: true,
            selection: true,
            zoom: true,
            zoomin: true,
            zoomout: true,
          },
        },
        foreColor: this.themeService.isDark ? "#fff" : "#373d3f",
      },
      theme: {
        mode: this.themeService.isDark ? "dark" : "light",
      },
      dataLabels: {
        enabled: false,
        formatter: (value) => {
          return numeral(value).format("0.0a");
        },
      },
      stroke: {
        curve: "smooth",
      },
      title: {
        text: "",
        align: "left",
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5,
        },
        //#region
      },
      yaxis: {
        title: {
          text: "",
        },
        labels: {
          formatter: (value) => {
            return numeral(value).format("0.0a");
          },
        },
      },
      xaxis: {
        tickAmount: 10,
        title: {
          text: "",
        },
        labels: {
          show: true,
          formatter: (value) => {
            if (!this._formatNumber) {
              return value;
            }

            return numeral(value).format("0.0a");
          },
        },
        categories: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
        ],
      },
    };
  }

  ngOnInit() {}
}
