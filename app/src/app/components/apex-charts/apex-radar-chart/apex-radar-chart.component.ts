import { Component, OnInit, ViewChild, Input } from "@angular/core";

import * as numeral from "numeral";

import {
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexChart,
  ApexXAxis,
  ApexFill,
  ChartComponent,
  ApexStroke,
  ApexMarkers,
  ApexTheme,
  ApexDataLabels,
  ApexYAxis,
} from "ng-apexcharts";
import { ThemeService } from "@app/services/theme.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  title: ApexTitleSubtitle;
  stroke: ApexStroke;
  fill: ApexFill;
  markers: ApexMarkers;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  theme: ApexTheme;
  dataLabels: ApexDataLabels;
};

@Component({
  selector: "app-apex-radar-chart",
  templateUrl: "./apex-radar-chart.component.html",
  styleUrls: ["./apex-radar-chart.component.scss"],
})
export class ApexRadarChartComponent implements OnInit {
  @Input("series") set series(value: ApexAxisChartSeries) {
    this.chartOptions.series = value;
  }

  @Input("categories") set categories(value: string[]) {
    this.chartOptions.xaxis.categories = value;
  }

  @Input("title") set title(value: string) {
    this.chartOptions.title.text = value;
  }

  @Input("height") set height(value: number) {
    this.chartOptions.chart.height = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this.chartOptions.xaxis.title.text = value;
  }

  @Input("show-x-axis-labels") set showXAxisLabels(value: boolean) {
    this.chartOptions.xaxis.labels.show = value;
  }

  @Input("show-y-axis-labels") set showYAxisLabels(value: boolean) {
    this.chartOptions.yaxis.labels.show = value;
  }

  @ViewChild("radarChart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private themeService: ThemeService) {
    this.chartOptions = {
      series: [
        {
          name: "Series Blue",
          data: [80, 50, 30, 40, 100, 20],
        },
        {
          name: "Series Green",
          data: [20, 30, 40, 80, 20, 80],
        },
        {
          name: "Series Orange",
          data: [44, 76, 78, 13, 43, 10],
        },
      ],
      chart: {
        height: 450,
        type: "radar",
        foreColor: this.themeService.isDark ? "#fff" : "#373d3f",
        dropShadow: {
          enabled: true,
          blur: 1,
          left: 1,
          top: 1,
        },
        animations: {
          enabled: false,
        },
      },
      theme: {
        mode: this.themeService.isDark ? "dark" : "light",
      },
      title: {
        text: "",
      },
      stroke: {
        width: 2,
        show: true,
      },
      fill: {
        opacity: 0.3,
      },
      markers: {
        size: 3,
        hover: {
          size: 6,
        },
      },
      xaxis: {
        labels: {
          show: true,
          formatter: (value) => {
            return numeral(value).format("0.0a");
          },
        },
        categories: ["2011", "2012", "2013", "2014", "2015", "2016"],
      },
      dataLabels: {
        enabled: true,
        background: {
          enabled: true,
          borderRadius: 2,
        },
        formatter: (value) => {
          return numeral(value).format("0.0a");
        },
      },
      yaxis: {
        showAlways: false,
        show: false,
        labels: {
          show: false,
          formatter: (value) => {
            return numeral(value).format("0.0a");
          },
        },
      },
    };
  }

  ngOnInit() {}
}
