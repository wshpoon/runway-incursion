import { Component, OnInit, ViewChild, Input } from "@angular/core";

import * as numeral from "numeral";

import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
  ChartComponent,
  ApexTitleSubtitle,
  ApexLegend,
  ApexTheme,
  ApexDataLabels,
} from "ng-apexcharts";
import { ThemeService } from "@app/services/theme.service";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  title: ApexTitleSubtitle;
  legend: ApexLegend;
  theme: ApexTheme;
  dataLabels: ApexDataLabels;
};

@Component({
  selector: "app-apex-pie-chart",
  templateUrl: "./apex-pie-chart.component.html",
  styleUrls: ["./apex-pie-chart.component.scss"],
})
export class ApexPieChartComponent implements OnInit {
  private _series: ApexNonAxisChartSeries = [];
  private _labels: string[] = [];

  @Input("title") set title(value: string) {
    if (!!value) {
      this.chartOptions.title.text = value;
    }
  }

  @Input("series") set series(value: ApexNonAxisChartSeries) {
    this._series = value;
    this.chartOptions.series = value;
  }

  @Input("labels") set labels(value: string[]) {
    this._labels = value;
    this.chartOptions.labels = value;
  }

  @ViewChild("operationDemographicPieChart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private themeService: ThemeService) {
    this.chartOptions = {
      title: {
        text: "...",
        align: "left",
        margin: 10,
        offsetX: 0,
        offsetY: 0,
        floating: false,
        style: {
          fontSize: "14px",
          fontWeight: "bold",
          fontFamily: undefined,
          color: "#263238",
        },
      },
      series: [44, 55, 13, 43, 22],
      chart: {
        height: 500,
        type: "pie",
        foreColor: this.themeService.isDark ? "#fff" : "#373d3f",
        animations: {
          enabled: false,
        },
      },
      theme: {
        mode: this.themeService.isDark ? "dark" : "light",
      },
      labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
      legend: {
        position: "bottom",
      },
      dataLabels: {
        formatter: (value) => {
          return numeral(value).format("0.0a");
        },
      },
      // responsive: [
      //   {
      //     breakpoint: 480,
      //     options: {
      //       chart: {
      //         width: 200,
      //       },
      //       legend: {
      //         position: "bottom",
      //       },
      //     },
      //   },
      // ],
    };
  }

  ngOnInit() {}
}
