import { Component, OnInit, ViewChild, Input, OnDestroy } from "@angular/core";

import * as numeral from "numeral";

import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexResponsive,
  ApexXAxis,
  ApexLegend,
  ApexFill,
  ApexYAxis,
  ApexTheme,
} from "ng-apexcharts";
import { ThemeService } from "@app/services/theme.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  legend: ApexLegend;
  fill: ApexFill;
  theme: ApexTheme;
};

@Component({
  selector: "app-apex-stacked-bar-chart",
  templateUrl: "./apex-stacked-bar-chart.component.html",
  styleUrls: ["./apex-stacked-bar-chart.component.scss"],
})
export class ApexStackedBarChartComponent implements OnInit {
  private _xAxisFormatNumber = false;
  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];

  @Input("series") set series(value: ApexAxisChartSeries) {
    this._series = value;
    this.chartOptions.series = value;
  }

  @Input("categories") set categories(value: string[]) {
    this._categories = value;
    this.chartOptions.xaxis.categories = value;
  }

  @Input("is-horizontal") set horizontal(value: boolean) {
    this.chartOptions.plotOptions.bar.horizontal = value;
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this.chartOptions.yaxis.title.text = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this.chartOptions.xaxis.title.text = value;
  }

  @Input("show-x-axis-labels") set showXAxisLabels(value: boolean) {
    this.chartOptions.xaxis.labels.show = value;
  }

  @Input("show-y-axis-labels") set showYAxisLabels(value: boolean) {
    this.chartOptions.yaxis.labels.show = value;
  }

  @Input("x-axis-format-number") set xAxisFormatNumber(value: boolean) {
    this._xAxisFormatNumber = value;
  }

  @ViewChild("stackedBarChart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private themeService: ThemeService) {
    this.chartOptions = {
      series: [
        {
          name: "PRODUCT A",
          data: [44, 55, 41, 67, 22, 43],
        },
        {
          name: "PRODUCT B",
          data: [13, 23, 20, 8, 13, 27],
        },
        {
          name: "PRODUCT C",
          data: [11, 17, 15, 15, 21, 14],
        },
        {
          name: "PRODUCT D",
          data: [21, 7, 25, 13, 22, 8],
        },
        {
          name: "PRODUCT E",
          data: [4, 5, 3, 2, 1, 3],
        },
      ],
      chart: {
        type: "bar",
        height: 500,
        stacked: true,
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
        },
        foreColor: this.themeService.isDark ? "#fff" : "#373d3f",
        animations: {
          enabled: false,
        },
      },
      theme: {
        mode: this.themeService.isDark ? "dark" : "light",
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0,
            },
          },
        },
      ],
      plotOptions: {
        bar: {
          horizontal: false,
        },
      },
      xaxis: {
        type: "category",
        categories: [
          "01/2011",
          "02/2011",
          "03/2011",
          "04/2011",
          "05/2011",
          "06/2011",
        ],
        title: {
          text: "",
        },
        labels: {
          show: true,
          formatter: (value) => {
            if (this._xAxisFormatNumber) {
              return numeral(value).format("0.0a");
            }

            return value;
          },
        },
      },
      yaxis: {
        show: true,
        title: {
          text: "",
        },
        labels: {
          show: true,
          formatter: (value) => {
            return numeral(value).format("0.0a");
          },
        },
      },
      dataLabels: {
        formatter: (value) => {
          return numeral(value).format("0.0a");
        },
      },
      legend: {
        position: "right",
        offsetY: 40,
      },
      fill: {
        opacity: 1,
      },
    };
  }

  ngOnInit() {}
}
