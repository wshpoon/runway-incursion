import { Component, Input, OnInit } from "@angular/core";

import {
  ApexAxisChartSeries,
  ApexChart,
  ApexTitleSubtitle,
  ApexDataLabels,
  ApexFill,
  ApexMarkers,
  ApexYAxis,
  ApexXAxis,
  ApexTooltip,
  ApexStroke
} from "ng-apexcharts";

@Component({
  selector: "app-apex-line-chart-alt",
  templateUrl: "./apex-line-chart-alt.component.html",
  styleUrls: ["./apex-line-chart-alt.component.scss"],
})
export class ApexLineChartAltComponent implements OnInit {
  private _yAxisRoundOff = false;
  private _formatNumber = false;

  @Input("series") set series(value: ApexAxisChartSeries) {
    this._series = value;
  }

  @Input("categories") set categories(value: string[]) {
    this._xaxis.categories = value;
  }

  @Input("title") set chartTitle(value: string) {
    this._title.text = value;
  }

  @Input("height") set height(value: number) {
    this._chart.height = value;
  }

  @Input("width") set width(value: number) {
    this._chart.width = value;
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this._yaxis.title.text = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this._xaxis.title.text = value;
  }

  @Input("show-x-axis-labels") set showXAxisLabels(value: boolean) {
    this._xaxis.labels.show = value;
  }

  @Input("y-axis-round-off") set yAxisFormatter(value: boolean) {
    this._yAxisRoundOff = value;
  }

  @Input("format-number") set formatNumber(value: boolean) {
    this._formatNumber = value;
  }

  private _series: ApexAxisChartSeries;
  private _chart: ApexChart;
  private _dataLabels: ApexDataLabels;
  private _markers: ApexMarkers;
  private _title: ApexTitleSubtitle;
  private _fill: ApexFill;
  private _yaxis: ApexYAxis;
  private _xaxis: ApexXAxis;
  private _tooltip: ApexTooltip;
  private _stroke: ApexStroke;

  get series(): ApexAxisChartSeries {
    return this._series;
  }

  get chart(): ApexChart {
    return this._chart;
  }

  get dataLabels(): ApexDataLabels {
    return this._dataLabels;
  }

  get markers(): ApexMarkers {
    return this._markers;
  }

  get title(): ApexTitleSubtitle {
    return this._title;
  }

  get chartTitle(): string {
    return this._title.text;
  }

  get fill(): ApexFill {
    return this._fill;
  }

  get yaxis(): ApexYAxis {
    return this._yaxis;
  }

  get xaxis(): ApexXAxis {
    return this._xaxis;
  }

  get tooltip(): ApexTooltip {
    return this._tooltip;
  }

  get stroke(): ApexStroke {
    return this._stroke;
  }

  constructor() {
    this.initChartData();
  }

  public initChartData(): void {
    this._series = [
      {
        name: "XYZ MOTORS",
        data: [],
      },
    ];

    this._stroke = {
      curve: "smooth",
      width: 2
    }

    this._series = [];

    this._chart = {
      type: "line",
      stacked: false,
      height: 350,
      zoom: {
        type: "x",
        enabled: true,
        autoScaleYaxis: true,
      },
      toolbar: {
        autoSelected: "zoom",
      },
      animations: {
        enabled: false,
      },
    };

    this._dataLabels = {
      enabled: false,
    };

    this._markers = {
      size: 0,
    };

    this._title = {
      text: "Stock Price Movement",
      align: "left",
    };

    // this._fill = {
    //   type: "gradient",
    //   gradient: {
    //     shadeIntensity: 1,
    //     inverseColors: false,
    //     opacityFrom: 0.5,
    //     opacityTo: 0,
    //     stops: [0, 90, 100],
    //   },
    // };

    this._yaxis = {
      labels: {},
      title: {
        text: "Price",
      },
    };

    this._xaxis = {
      type: "datetime",
      title: {
        text: "",
      },
      categories: []
    };

    this._tooltip = {
      shared: false,
    };
  }

  ngOnInit() {}
}
