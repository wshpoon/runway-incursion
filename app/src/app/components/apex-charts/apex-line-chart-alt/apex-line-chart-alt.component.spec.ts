import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApexLineChartAltComponent } from './apex-line-chart-alt.component';

describe('ApexLineChartAltComponent', () => {
  let component: ApexLineChartAltComponent;
  let fixture: ComponentFixture<ApexLineChartAltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApexLineChartAltComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApexLineChartAltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
