import { Component, OnInit, ViewChild, Input } from "@angular/core";

import * as numeral from "numeral";

import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexTitleSubtitle,
  ApexDataLabels,
  ApexStroke,
  ApexYAxis,
  ApexXAxis,
  ApexPlotOptions,
  ApexTooltip,
  ApexTheme,
} from "ng-apexcharts";
import { ThemeService } from "@app/services/theme.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  tooltip: ApexTooltip;
  colors: string[];
  title: ApexTitleSubtitle;
  subtitle: ApexTitleSubtitle;
  theme: ApexTheme;
};

@Component({
  selector: "app-apex-bar-chart",
  templateUrl: "./apex-bar-chart.component.html",
  styleUrls: ["./apex-bar-chart.component.scss"],
})
export class ApexBarChartComponent implements OnInit {
  @Input("series") set series(value: ApexAxisChartSeries) {
    if (value.length > 0) {
      this.chartOptions.series = value;
    }
  }

  @Input("categories") set categories(value: string[]) {
    if (value.length > 0) {
      this.chartOptions.xaxis.categories = value;
    }
  }

  @Input("title") set title(value: string) {
    if (!!value) {
      this.chartOptions.title.text = value;
    }
  }

  @Input("is-horizontal") set horizontal(value: boolean) {
    this.chartOptions.plotOptions.bar.horizontal = value;
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this.chartOptions.yaxis.title.text = value;
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this.chartOptions.xaxis.title.text = value;
  }

  @Input("height") set height(value: number) {
    this.chartOptions.chart.height = value;
  }

  @Input("show-y-axis-labels") set showYAxisLabels(value: boolean) {
    this.chartOptions.yaxis.labels.show = value;
  }

  @Input("show-x-axis-labels") set showXAxisLabels(value: boolean) {
    this.chartOptions.xaxis.labels.show = value;
  }

  @ViewChild("barChart") chart: ChartComponent;

  public chartOptions: Partial<ChartOptions>;

  constructor(private themeService: ThemeService) {
    this.chartOptions = {
      series: [
        {
          name: "basic",
          data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380],
        },
      ],
      chart: {
        type: "bar",
        height: 500,
        foreColor: this.themeService.isDark ? "#fff" : "#373d3f",
        animations: {
          enabled: false,
        },
        toolbar: {
          tools: {
            pan: true,
            download: true,
            reset: true,
            selection: true,
            zoom: true,
            zoomin: true,
            zoomout: true,
          },
        },
      },
      theme: {
        mode: this.themeService.isDark ? "dark" : "light",
      },
      plotOptions: {
        bar: {
          horizontal: false,
          barHeight: "100%",
          columnWidth: "100%",
        },
      },
      title: {
        text: "Custom DataLabels",
        align: "center",
        floating: true,
      },
      dataLabels: {
        enabled: false,
        formatter: (value) => {
          return numeral(value).format("0.0a");
        },
      },
      yaxis: {
        title: {
          text: "",
        },
        labels: {
          show: true,
          formatter: (value) => {
            return numeral(value).format("0.0a");
          },
        },
      },
      xaxis: {
        categories: ["South Korea", "Canada"],
        title: {
          text: "",
        },
        labels: {
          show: true,
          formatter: (value) => {
            return numeral(value).format("0.0a");
          },
        },
      },
    };
  }

  ngOnInit() {}
}
