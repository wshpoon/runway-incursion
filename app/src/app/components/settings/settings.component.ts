import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent implements OnInit {
  constructor(private modalController: ModalController) {}

  ngOnInit() {}

  get isDark() {
    return localStorage.getItem("preferences.theme") === "dark";
  }

  toggleTheme(event) {
    console.log(event);

    let systemDark = window.matchMedia("(prefers-color-scheme: dark)");
    systemDark.addListener(this.colorTest);

    if (event.detail.checked) {
      document.body.setAttribute("data-theme", "dark");
      localStorage.setItem("preferences.theme", "dark");
    } else {
      document.body.setAttribute("data-theme", "light");
      localStorage.setItem("preferences.theme", "light");
    }
  }

  colorTest(systemInitiatedDark) {
    if (systemInitiatedDark.matches) {
      document.body.setAttribute("data-theme", "dark");
      localStorage.setItem("preferences.theme", "dark");
    } else {
      document.body.setAttribute("data-theme", "light");
      localStorage.setItem("preferences.theme", "light");
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
