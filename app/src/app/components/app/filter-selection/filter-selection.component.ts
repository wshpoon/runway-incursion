import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastService } from "@app/services/toast.service";

export interface DropdownField {
  autocomplete: boolean;
  label: string;
  data?: string[];
}

@Component({
  selector: "app-filter-selection",
  templateUrl: "./filter-selection.component.html",
  styleUrls: ["./filter-selection.component.scss"],
})
export class FilterSelectionComponent implements OnInit {
  private _title = "Filter Options";
  private _allRequired = false;
  private _dropdown: DropdownField = {
    autocomplete: true,
    label: "Dropdown Field",
    data: [],
  };

  @Input("dropdown") set dropdown(value: DropdownField) {
    this._dropdown = { ...this._dropdown, ...value };
  }

  @Input("title") set title(value: string) {
    this._title = value;
  }

  @Input("defaultValues") set defaultValues(value: object) {
    console.log("defaultValues", value);

    this.filtersFormGroup.setValue(value);
  }

  @Input("allRequired") set allRequired(value: boolean) {
    this._allRequired = value;
  }

  get title(): string {
    return this._title;
  }

  get dropdown(): DropdownField {
    return this._dropdown;
  }

  get allRequired(): boolean {
    return this._allRequired;
  }

  public filtersFormGroup: FormGroup;

  constructor(
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private toastService: ToastService
  ) {
    this.filtersFormGroup = this.formBuilder.group({
      item: [null, this.allRequired ? Validators.required : ""],
      minDate: [null, this.allRequired ? Validators.required : ""],
      maxDate: [null, this.allRequired ? Validators.required : ""],
    });
  }

  ngOnInit() {}

  done() {
    if (this.filtersFormGroup.valid) {
      this.modalController.dismiss({
        values: this.filtersFormGroup.value,
        reload: true,
      });
    } else {
      this.toastService.showToast({
        message:
          "Please fill in the required fields before dispatching your query.",
      });
    }
  }

  closeModal() {
    this.modalController.dismiss({
      reload: false,
    });
  }
}
