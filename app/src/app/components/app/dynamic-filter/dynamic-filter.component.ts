import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastService } from "@app/services/toast.service";
import { ModalController } from "@ionic/angular";

export interface DynamicFilterComponentProps {
  title?: string;
  fields: InputField[];
}

export interface InputFieldOption {
  text: string;
  value: any;
}

export interface InputField {
  label: string;
  placeholder?: string;
  formControlName: string;
  type: "text" | "dropdown" | "number";
  options?: InputFieldOption[];
  required?: boolean;
  autocomplete: boolean;
  initialValue: string | number;
}

@Component({
  selector: "app-dynamic-filter",
  templateUrl: "./dynamic-filter.component.html",
  styleUrls: ["./dynamic-filter.component.scss"],
})
export class DynamicFilterComponent implements OnInit {
  private _title: string = "";
  private _fields: InputField[] = [];
  private _formGroup: FormGroup;

  @Input() set title(value: string) {
    this._title = value;
  }

  @Input() set fields(value: InputField[]) {
    this._fields = value;
  }

  get title() {
    return this._title;
  }

  get fields() {
    return this._fields;
  }

  get formGroup() {
    return this._formGroup;
  }

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    let formGroup = {};

    this._fields.forEach((field) => {
      formGroup = {
        ...formGroup,
        ...{
          [field.formControlName]: [
            !!field.initialValue
              ? field.initialValue
              : field.type === "number"
              ? 0
              : null,
            field.required ? Validators.required : null,
          ],
        },
      };
    });

    this._formGroup = this.formBuilder.group(formGroup);
  }

  done() {
    if (this._formGroup.valid) {
      this.modalController.dismiss({
        values: this._formGroup.value,
        reload: true,
      });
    } else {
      this.toastService.showToast({
        message:
          "Please fill in the required fields before dispatching your query.",
      });
    }
  }

  closeModal() {
    this.modalController.dismiss({
      reload: false,
    });
  }
}
