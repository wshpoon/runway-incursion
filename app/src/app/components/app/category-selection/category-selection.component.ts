import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { RunwayAndGradeChartService } from "@app/services/runway-and-grade-chart.service";
import { Subscription } from "rxjs";
import { ModalController, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-category-selection",
  templateUrl: "./category-selection.component.html",
  styleUrls: ["./category-selection.component.scss"],
})
export class CategorySelectionComponent implements OnInit {
  private _categories: any[] = [];
  private _title = "";

  @Input("categories") set categories(value: any[]) {
    this._categories = value;
    this.filteredCategories = value;
  }

  @Input("title") set title(value: string) {
    this._title = value;
  }

  get categories(): any[] {
    return this._categories;
  }

  get title(): string {
    return this._title;
  }

  public search: string = "";

  public filteredCategories: any[] = [];

  constructor(public modalController: ModalController) {}

  ngOnInit() {}

  done() {
    this.modalController.dismiss({
      categories: this._categories,
      reload: true,
    });
  }

  closeModal() {
    this.modalController.dismiss({
      reload: false,
    });
  }

  deselectAllCategories() {
    this._categories = this._categories.map((value) => {
      return {
        name: value.name,
        selected: false,
      };
    });
  }

  onChange(event: Event) {
    this.filteredCategories = this._categories;

    this.filteredCategories = this.filteredCategories.filter((item) => {
      // const pattern = new RegExp(`[^${this.search}]`, "i");

      // return pattern.test(String(item.name));

      return String(item.name)
        .toLowerCase()
        .includes(String(this.search).toLowerCase());
    });
  }
}
