import { Component, Input, OnInit } from "@angular/core";

import * as Highcharts from "highcharts";

@Component({
  selector: "app-hc-scatter-chart",
  templateUrl: "./hc-scatter-chart.component.html",
  styleUrls: ["./hc-scatter-chart.component.scss"],
})
export class HcScatterChartComponent implements OnInit {
  @Input("series") set series(value: any[]) {
    this.chartOptions.series = value;
  }

  @Input("height") set height(value: any) {
    this.chartOptions.chart.height = value;
  }

  @Input("width") set width(value: any) {
    this.chartOptions.chart.width = value;
  }

  @Input("x-min") set xMin(value: number) {
    this.chartOptions.xAxis = {
      ...this.chartOptions.xAxis,
      ...{
        min: value,
      },
    };
  }

  @Input("x-max") set xMax(value: number) {
    this.chartOptions.xAxis = {
      ...this.chartOptions.xAxis,
      ...{
        max: value,
      },
    };
  }

  @Input("y-min") set yMin(value: number) {
    this.chartOptions.yAxis = {
      ...this.chartOptions.yAxis,
      ...{
        min: value,
      },
    };
  }

  @Input("y-max") set yMax(value: number) {
    this.chartOptions.yAxis = {
      ...this.chartOptions.yAxis,
      ...{
        max: value,
      },
    };
  }

  @Input("y-axis-title") set yAxisTitle(value: string) {
    this.chartOptions.yAxis = {
      ...this.chartOptions.yAxis,
      ...{ title: { text: value } },
    };
  }

  @Input("x-axis-title") set xAxisTitle(value: string) {
    this.chartOptions.xAxis = {
      ...this.chartOptions.xAxis,
      ...{ title: { text: value } },
    };
  }

  @Input("title") set title(value: string) {
    this.chartOptions.title.text = value;
  }

  public highcharts = Highcharts;
  public chartOptions: Highcharts.Options;

  constructor() {
    this.chartOptions = {
      chart: {
        renderTo: "chart",
        height: 600,
        zoomType: "xy",
        animation: false,
        panning: {
          enabled: true,
        },
      },
      plotOptions: {
        scatter: {
          marker: {
            symbol: 'circle'
          }
        }
      },
      boost: {
        useGPUTranslations: true,
        usePreallocated: true,
      },
      yAxis: {
        min: 0,
        max: 10000,
        scrollbar: {
          enabled: true,
        },
        title: {
          text: "Y-AXIS",
        },
      },
      xAxis: {
        min: 0,
        max: 10000,
        title: {
          text: "X-AXIS",
        },
      },
      title: {
        text: "",
      },
      series: [
        {
          type: "scatter",
          // zoomType: "xy",
          name: "Browser share",
          data: [
            [1, 1.5],
            [2.8, 3.5],
            [3.9, 4.2],
          ],
        },
      ],
    };
  }

  ngOnInit() {}

  onUpdate(event: Event) {
    console.log(event);
  }
}
