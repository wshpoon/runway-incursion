import { Component, OnInit, Input } from "@angular/core";

import { ChartDataSets, ChartType, ChartOptions } from "chart.js";
import { Label } from "ng2-charts";

@Component({
  selector: "app-ng2-scatter-chart",
  templateUrl: "./ng2-scatter-chart.component.html",
  styleUrls: ["./ng2-scatter-chart.component.scss"],
})
export class Ng2ScatterChartComponent implements OnInit {
  @Input("data") set scatterChartData(value: ChartDataSets[]) {
    console.log("[Ng2ScatterChartComponent] set scatterChartData()", value);

    this._scatterChartData = value;
  }

  // scatter
  private _scatterChartOptions: ChartOptions = {
    responsive: true,
  };
  private _scatterChartLabels: Label[] = [
    "Eating",
    "Drinking",
    "Sleeping",
    "Designing",
    "Coding",
    "Cycling",
    "Running",
  ];

  private _scatterChartData: ChartDataSets[] = [
    {
      data: [
        { x: 1, y: 1 },
        { x: 2, y: 3 },
        { x: 3, y: -2 },
        { x: 4, y: 4 },
        { x: 5, y: -3 },
      ],
      label: "Series A",
      pointRadius: 10,
    },
  ];

  private _scatterChartType: ChartType = "scatter";

  get scatterChartData(): ChartDataSets[] {
    return this._scatterChartData;
  }

  get scatterChartOptions(): ChartOptions {
    return this._scatterChartOptions;
  }

  get scatterChartLabels(): Label[] {
    return this._scatterChartLabels;
  }

  get scatterChartType(): ChartType {
    return this._scatterChartType;
  }

  constructor() {}

  // events
  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  ngOnInit() {}
}
