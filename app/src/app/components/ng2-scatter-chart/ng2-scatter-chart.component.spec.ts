import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ng2ScatterChartComponent } from './ng2-scatter-chart.component';

describe('Ng2ScatterChartComponent', () => {
  let component: Ng2ScatterChartComponent;
  let fixture: ComponentFixture<Ng2ScatterChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ng2ScatterChartComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ng2ScatterChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
