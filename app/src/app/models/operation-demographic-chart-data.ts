export interface OperationDemographic {
  commercial?: number;
  general_aviation?: number;
  military?: number;
}

export interface OperationDemographicChartData {
  data?: {
    [key: string]: number;
  };
  airports: string[];
}
