export interface SevenDayRollingOperation {
  date?: string;
  year?: string;
  day?: string;
  total_operations?: string;
  average_operations?: string;
}

export interface SevenDayRollingOperationChartData {
  // data?: SevenDayRollingOperation[];
  years?: {
    [key: string]: SevenDayRollingOperation[];
  };
  labels?: string[];
  dates?: string[];
}
