export interface StateIncursion {
  state?: string;
  city?: string;
  runway?: string;
  latitude?: number;
  longitude?: number;
  total_incursions?: number;
}

export interface StateIncursionChartData {
  data?: StateIncursion[];
  states?: string[];
}
