export interface IncursionTotal {
  acft_1_type?: string;
  facility?: string;
  acft_1_rwy_sfty_type?: string;
  total_incursions?: number;
}

export interface IncursionTotalChartData {
  data?: {
    [key: string]: IncursionTotal[];
  };
  facilities?: string[];
}
