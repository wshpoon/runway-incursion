export interface HourlyAccuracy {
  facility?: string;
  hour?: number;
  label?: string;
  commercial_departures?: number;
  ga_departures?: number;
  military_departures?: number;
  total_departures?: number;
  commercial_arrivals?: number;
  ga_arrivals?: number;
  military_arrivals?: number;
  total_arrivals?: number;
  average_operations?: number;
  total_incursions?: number;
  incursion_rates?: number;
  total_events?: number;
}

export class HourlyAccuracyChartData {
  data?: HourlyAccuracy[];
  facilities?: string[];
}
