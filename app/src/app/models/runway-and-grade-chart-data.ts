export interface RunwayAndGrade {
  date?: any;
  facility?: any;
  event_tkof_lndg_desc?: any;
  acft_1_type?: any;
  rwy_sfty_ri_cat_rnk_code?: any;
  total_incursions?: any;
}

export interface RunwayAndGradeChartData {
  labels?: string[];
  official_runways?: string[];
  airports?: string[];
  codes?: {
    [key: string]: RunwayAndGrade[];
  };
}
