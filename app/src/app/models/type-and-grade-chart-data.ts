export interface TypeAndGrade {
  facility?: string;
  label?: string;
  incdnt_type_faa_code?: string;
  total_incursions?: string;
  rwy_sfty_ri_cat_rnk_code?: string;
  acft_1_type?: string;
  date?: string;
}

export interface TypeAndGradeChartData {
  data?: {
    [key: string]: TypeAndGrade[];
  };
  labels?: string[];
  types?: string[];
}
