export interface HourlyOperation1 {
  hour?: number;
  total_departures?: number;
  total_arrivals?: number;
  average_operations?: number;
}

export interface HourlyOperation2 {
  hour?: number;
  commercial_departures?: number;
  commercial_arrivals?: number;
  ga_departures?: number;
  ga_arrivals?: number;
  military_departures?: number;
  military_arrivals?: number;
  average_operations?: number;
}

export interface HourlyOperationChartData {
  data?: {
    average_hourly_operations_1?: {
      [key: string]: HourlyOperation1[];
    };
    average_hourly_operations_2?: {
      [key: string]: HourlyOperation2[];
    };
  };
  hours?: number[];
}
