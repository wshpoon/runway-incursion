export interface OneYearRollingOperation {
  label?: string;
  date?: string;
  facility?: string;
  state?: string;
  total_events?: number;
  total_incursions?: number;
  average_incursions?: number;
  total_operations?: number;
  average_operations?: number;
  incursion_rate?: number;
}

export interface OneYearRollingOperationChartData {
  data?: OneYearRollingOperation[];
  years?: string[];
  facilities?: string[];
}
