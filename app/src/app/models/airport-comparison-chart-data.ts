export interface AirportComparison {
  facility?: string;
  label?: string;
  total_operations?: number;
  total_incursions?: number;
  total_incursion_rate?: number;
}

export interface AirportComparisonChartData {
  incursion_rates?: {
    [key: string]: AirportComparison[];
  };
  operation_totals?: {
    [key: string]: AirportComparison[];
  };
  facilities?: string[];
}
