export interface IncursionRate {
  date?: string;
  year?: number;
  month?: number;
  day?: number;
  incursion_rate?: number;
}

export interface IncursionRateChartData {
  labels?: string[];
  dates?: string[];
  years?: {
    [key: string]: IncursionRate[];
  };
}
