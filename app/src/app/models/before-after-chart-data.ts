export interface BeforeAfter {
  // label?: string;
  // date?: Date;
  // state?: string;
  // facility?: string;
  // total_events?: number;
  // total_incursions?: number;
  // total_operations?: number;
  // average_incursions?: number;
  // average_operations?: number;
  // incursion_rate?: number;

  a_date?: string;
  b_date?: string;
  average_operations?: number;
  label?: string;
  x?: number;
  y?: number;

  // before_incursion_rate?: number;
  // after_incursion_rate?: number;
}

export interface BeforeAfterChartData {
  // before?: BeforeAfter[];
  // after?: BeforeAfter[];
  series?: any[];
  labels?: string[];
  xaxis?: number[];
  data?: {
    [key: string]: BeforeAfter[];
  };
  // data?: [number, number][];
}
