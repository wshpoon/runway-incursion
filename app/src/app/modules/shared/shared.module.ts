import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgApexchartsModule } from "ng-apexcharts";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { ChartsModule } from "ng2-charts";
import { NgDygraphsModule } from "ng-dygraphs";
import { HighchartsChartModule } from "highcharts-angular";
import { GoogleChartsModule } from "angular-google-charts";

// ng2charts
import { Ng2ScatterChartComponent } from "@app/components/ng2-scatter-chart/ng2-scatter-chart.component";

// app components
import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";
import { FilterSelectionComponent } from "@app/components/app/filter-selection/filter-selection.component";
import { SettingsComponent } from "@app/components/settings/settings.component";
import { DynamicFilterComponent } from "@app/components/app/dynamic-filter/dynamic-filter.component";

// amcharts
import { SampleChartComponent } from "@app/components/amcharts/sample-chart/sample-chart.component";
import { MapChartComponent } from "@app/components/amcharts/map-chart/map-chart.component";

// apexcharts
import { ApexLineChartComponent } from "@app/components/apex-charts/apex-line-chart/apex-line-chart.component";
import { ApexBarChartComponent } from "@app/components/apex-charts/apex-bar-chart/apex-bar-chart.component";
import { ApexStackedBarChartComponent } from "@app/components/apex-charts/apex-stacked-bar-chart/apex-stacked-bar-chart.component";
import { ApexPieChartComponent } from "@app/components/apex-charts/apex-pie-chart/apex-pie-chart.component";
import { ApexScatterChartComponent } from "@app/components/apex-charts/apex-scatter-chart/apex-scatter-chart.component";
import { ApexRadarChartComponent } from "@app/components/apex-charts/apex-radar-chart/apex-radar-chart.component";
import { ApexLineChartAltComponent } from "@app/components/apex-charts/apex-line-chart-alt/apex-line-chart-alt.component";
import { DyLineChartComponent } from "@app/components/dygraphs/dy-line-chart/dy-line-chart.component";
import { DyScatterChartComponent } from "@app/components/dygraphs/dy-scatter-chart/dy-scatter-chart.component";

// highcharts
import { HcScatterChartComponent } from "@app/components/highcharts/hc-scatter-chart/hc-scatter-chart.component";

// import * as PlotlyJS from "plotly.js/dist/plotly.js";
// import { PlotlyModule } from "angular-plotly.js";

// PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    NgApexchartsModule,
    ChartsModule,
    NgDygraphsModule,
    GoogleChartsModule,
    HighchartsChartModule,
    // PlotlyModule,
  ],
  exports: [
    ApexLineChartComponent,
    ApexLineChartAltComponent,
    ApexBarChartComponent,
    ApexStackedBarChartComponent,
    ApexPieChartComponent,
    ApexScatterChartComponent,
    ApexRadarChartComponent,
    Ng2ScatterChartComponent,
    SettingsComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgDygraphsModule,
    SampleChartComponent,
    MapChartComponent,
    GoogleChartsModule,
    DyLineChartComponent,
    DyScatterChartComponent,
    HcScatterChartComponent,
    // PlotlyModule,
  ],
  declarations: [
    ApexLineChartComponent,
    ApexLineChartAltComponent,
    ApexBarChartComponent,
    ApexStackedBarChartComponent,
    ApexPieChartComponent,
    ApexScatterChartComponent,
    ApexRadarChartComponent,
    CategorySelectionComponent,
    DynamicFilterComponent,
    FilterSelectionComponent,
    Ng2ScatterChartComponent,
    SettingsComponent,
    SampleChartComponent,
    MapChartComponent,
    DyLineChartComponent,
    DyScatterChartComponent,
    HcScatterChartComponent,
    // HighchartsChartComponent
  ],
})
export class SharedModule {}
