export const Charts = {
  LineChart: "Line Chart",
  BarChart: "Bar Chart",
  StackedBarChart: "Stacked Bar Chart",
  RadarChart: "Radar Chart",
  ScatterChart: "Scatter Chart",
  PieChart: "Pie Chart",
  MapChart: "Map Chart",
};

export const AppCharts = [
  Charts.LineChart,
  Charts.BarChart,
  Charts.StackedBarChart,
  Charts.RadarChart,
  Charts.ScatterChart,
  Charts.PieChart,
  Charts.MapChart,
]