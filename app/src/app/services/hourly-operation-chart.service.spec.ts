import { TestBed } from '@angular/core/testing';

import { HourlyOperationChartService } from './hourly-operation-chart.service';

describe('HourlyOperationChartService', () => {
  let service: HourlyOperationChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HourlyOperationChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
