import { Injectable } from "@angular/core";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { HourlyIncursionChartData } from "@app/models/hourly-incursion-chart-data";

@Injectable({
  providedIn: "root",
})
export class HourlyIncursionChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/hourly-incursion-chart";
  private _dataSubject = new BehaviorSubject<HourlyIncursionChartData>({
    data: {},
    hours: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<HourlyIncursionChartData> {
    console.log("[HourlyAccuracyChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<HourlyIncursionChartData>(this._endpoint)
        .pipe(
          map((value: HourlyIncursionChartData) => {
            console.log(
              "[HourlyAccuracyChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
