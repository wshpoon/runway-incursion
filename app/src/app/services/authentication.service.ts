import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

import { environment } from "./../../environments/environment";
import { User } from "@app/models/user";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  /**
   * @param http { HttpClient }
   */
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  /**
   * Get the current user value from BehaviorSubject user.
   *
   * @returns { User }
   */
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  /**
   * Check if user is logged in.
   *
   * @returns boolean
   */
  public get loggedIn(): boolean {
    return this.currentUserSubject.value !== null;
  }

  /**
   * Request login to API with credentials.
   *
   * @param email     The email address of the user.
   * @param password  The password of the user.
   * @return { Observable }
   */
  public login(email: string, password: string): Observable<any> {
    console.log(
      `[Authentication Service] login(email: ${email}, password: ${password})`
    );

    return this.http
      .post<any>(`/api/auth/login`, { email, password })
      .pipe(
        map((response) => {
          console.log("[Authentication Service]", response);

          let user = response.user;

          user["access_token"] = response.access_token;

          console.log("[Authentication Service]", user);

          // store user details and jwt token in local storage
          // to keep user logged in between page refreshes
          localStorage.setItem("currentUser", JSON.stringify(user));
          this.currentUserSubject.next(user);

          return user;
        })
      );
  }

  /**
   * Request logout from API then remove user state,
   * and change the BehaviorSubject to null.
   *
   * @return void
   */
  public logout() {
    console.log(`[Authentication Service] logout()`);

    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
    this.currentUserSubject.next(null);
  }
}
