import { BeforeAfterChartData } from "./../models/before-after-chart-data";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, share } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class BeforeAfterChartService {
  private _endpoint: string = "/api/incursions/before-after-chart";
  private _dataSubject = new BehaviorSubject<BeforeAfterChartData>(null);

  public get data() {
    return this._dataSubject.value;
  }

  constructor(private http: HttpClient) {}

  public getData(): Observable<BeforeAfterChartData> {
    // console.log(
    //   "[BeforeAfterChartService] getBeforeAfterChartData() called..."
    // );

    if (this._dataSubject.value === null) {
      return this.http.get<BeforeAfterChartData>(this._endpoint).pipe(
        map((value: BeforeAfterChartData) => {
          // console.log("[BeforeAfterChartService] in pipe() -> map()", value);

          this._dataSubject.next(value);

          return value;
        }),
        share()
      );
    } else {
      return of(this._dataSubject.value);
    }
  }
}
