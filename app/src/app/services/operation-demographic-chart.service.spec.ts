import { TestBed } from '@angular/core/testing';

import { OperationDemographicChartService } from './operation-demographic-chart.service';

describe('OperationDemographicChartService', () => {
  let service: OperationDemographicChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OperationDemographicChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
