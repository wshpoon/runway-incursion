import { TestBed } from '@angular/core/testing';

import { AirportComparisonChartService } from './airport-comparison-chart.service';

describe('AirportComparisonChartService', () => {
  let service: AirportComparisonChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AirportComparisonChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
