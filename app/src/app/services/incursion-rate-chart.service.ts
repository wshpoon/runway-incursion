import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { IncursionRateChartData } from "@app/models/incursion-rate-chart-data";
import { BehaviorSubject, Observable, of } from "rxjs";
import { map, share } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class IncursionRateChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/incursion-rate-chart";
  private _dataSubject = new BehaviorSubject<IncursionRateChartData>({
    dates: [],
    years: {},
    labels: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<IncursionRateChartData> {
    if (!this._requested) {
      const response = this.http
        .get<IncursionRateChartData>(this._endpoint)
        .pipe(
          map((value: IncursionRateChartData) => {
            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
