import { TestBed } from '@angular/core/testing';

import { HourlyAccuracyChartService } from './hourly-accuracy-chart.service';

describe('HourlyAccuracyChartService', () => {
  let service: HourlyAccuracyChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HourlyAccuracyChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
