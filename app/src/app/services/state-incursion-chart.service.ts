import { Injectable } from "@angular/core";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { StateIncursionChartData } from "@app/models/state-incursion-chart-data";

@Injectable({
  providedIn: "root",
})
export class StateIncursionChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/state-incursion-chart";
  private _dataSubject = new BehaviorSubject<StateIncursionChartData>({
    data: [],
    states: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<StateIncursionChartData> {
    console.log("[StateIncursionChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<StateIncursionChartData>(this._endpoint)
        .pipe(
          map((value: StateIncursionChartData) => {
            console.log(
              "[StateIncursionChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
