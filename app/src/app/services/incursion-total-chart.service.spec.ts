import { TestBed } from '@angular/core/testing';

import { IncursionTotalChartService } from './incursion-total-chart.service';

describe('IncursionTotalChartService', () => {
  let service: IncursionTotalChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncursionTotalChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
