import { TestBed } from '@angular/core/testing';

import { SevenDayRollingOperationChartService } from './seven-day-rolling-operation-chart.service';

describe('SevenDayRollingOperationChartService', () => {
  let service: SevenDayRollingOperationChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SevenDayRollingOperationChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
