import { Injectable } from "@angular/core";
import { HourlyOperationChartData } from "@app/models/hourly-operation-chart-data";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class HourlyOperationChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/hourly-operation-chart";
  private _dataSubject = new BehaviorSubject<HourlyOperationChartData>({
    data: {
      average_hourly_operations_1: {},
      average_hourly_operations_2: {},
    },
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<HourlyOperationChartData> {
    console.log("[HourlyOperationChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<HourlyOperationChartData>(this._endpoint)
        .pipe(
          map((value: HourlyOperationChartData) => {
            console.log(
              "[HourlyOperationChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
