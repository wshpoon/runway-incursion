import { Injectable } from "@angular/core";
import { IncursionTotalChartData } from "@app/models/incursion-total-chart-data";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class IncursionTotalChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/incursion-totals-chart";
  private _dataSubject = new BehaviorSubject<IncursionTotalChartData>({
    data: {},
    facilities: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<IncursionTotalChartData> {
    console.log("[IncursionTotalChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<IncursionTotalChartData>(this._endpoint)
        .pipe(
          map((value: IncursionTotalChartData) => {
            console.log(
              "[IncursionTotalChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
