import { TestBed } from '@angular/core/testing';

import { IncursionRateChartService } from './incursion-rate-chart.service';

describe('IncursionRateChartService', () => {
  let service: IncursionRateChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncursionRateChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
