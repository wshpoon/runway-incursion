import { TestBed } from '@angular/core/testing';

import { StateIncursionService } from './state-incursion.service';

describe('StateIncursionService', () => {
  let service: StateIncursionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StateIncursionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
