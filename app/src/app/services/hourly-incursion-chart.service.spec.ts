import { TestBed } from '@angular/core/testing';

import { HourlyIncursionChartService } from './hourly-incursion-chart.service';

describe('HourlyIncursionChartService', () => {
  let service: HourlyIncursionChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HourlyIncursionChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
