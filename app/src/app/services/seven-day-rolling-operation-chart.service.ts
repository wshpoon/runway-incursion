import { Injectable } from "@angular/core";
import {
  SevenDayRollingOperationChartData,
  SevenDayRollingOperation,
} from "@app/models/seven-day-rolling-operation";
import { BehaviorSubject, Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { share, map } from "rxjs/operators";
import staticData from "./static-data/seven-day-rolling-operation-data.json";

@Injectable({
  providedIn: "root",
})
export class SevenDayRollingOperationChartService {
  private _requested: boolean = false;
  private _endpoint: string =
    "/api/incursions/seven-day-rolling-operation-chart";
  private _dataSubject = new BehaviorSubject<SevenDayRollingOperationChartData>(
    { years: {}, labels: [] }
  );
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<SevenDayRollingOperationChartData> {
    console.log("[SevenDayRollingOperationChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<SevenDayRollingOperationChartData>(this._endpoint)
        .pipe(
          map((value: SevenDayRollingOperationChartData) => {
            console.log(
              "[SevenDayRollingOperationChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
