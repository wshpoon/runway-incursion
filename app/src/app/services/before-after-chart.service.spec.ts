import { TestBed } from '@angular/core/testing';

import { BeforeAfterChartService } from './before-after-chart.service';

describe('RunwayIncursionService', () => {
  let service: BeforeAfterChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BeforeAfterChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
