import { Injectable } from "@angular/core";
import { Observable, of, BehaviorSubject } from "rxjs";
import { HourlyAccuracyChartData } from "@app/models/hourly-accuracy-chart-data";
import { map, share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class HourlyAccuracyChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/hourly-accuracy-chart";
  private _dataSubject = new BehaviorSubject<HourlyAccuracyChartData>({
    data: [],
    facilities: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<HourlyAccuracyChartData> {
    console.log("[HourlyAccuracyChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<HourlyAccuracyChartData>(this._endpoint)
        .pipe(
          map((value: HourlyAccuracyChartData) => {
            console.log(
              "[HourlyAccuracyChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
