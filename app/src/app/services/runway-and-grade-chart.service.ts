import { Injectable } from "@angular/core";
import { RunwayAndGradeChartData } from "@app/models/runway-and-grade-chart-data";
import { BehaviorSubject, Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";
import staticData from "./static-data/runway-and-grade-chart-data.json";
import { map, share } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class RunwayAndGradeChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/runway-and-grade-chart";
  private _dataSubject = new BehaviorSubject<RunwayAndGradeChartData>({
    codes: {},
    labels: [],
    official_runways: [],
  });
  public data = this._dataSubject.asObservable();
  public categories: any[] = [];

  private _filterOptionState = new BehaviorSubject<{ categories: any[] }>({
    categories: [],
  });

  public get filterOptionState() {
    return this._filterOptionState;
  }

  constructor(private http: HttpClient) {}

  public getData(): Observable<RunwayAndGradeChartData> {
    if (!this._requested) {
      const response = this.http
        .get<RunwayAndGradeChartData>(this._endpoint)
        .pipe(
          map((value: RunwayAndGradeChartData) => {
            console.log(
              "[RunwayAndGradeChartService] in pipe() -> map()",
              value
            );

            const categories = value.official_runways.map((value) => {
              const defaultSelected = [
                "RWY 1",
                "RWY 15",
                "RWY 19",
                "RWY 22/19",
                "RWY 33",
                "RWY 4",
              ].includes(value);

              return {
                name: value,
                selected: defaultSelected,
              };
            });

            this._dataSubject.next(value);
            this._filterOptionState.next({
              categories,
            });

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }

  public setFilterOptionState(categories: any[]): void {
    this._filterOptionState.next({ categories });
  }
}
