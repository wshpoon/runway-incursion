import { Injectable } from "@angular/core";
import { share, map } from "rxjs/operators";
import { OneYearRollingOperationChartData } from "@app/models/one-year-rolling-operation-chart-data";
import { of, Observable, BehaviorSubject } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class OneYearRollingOperationChartService {
  private _requested: boolean = false;
  private _endpoint: string =
    "/api/incursions/daily-one-year-rolling-incursion-chart";
  private _dataSubject = new BehaviorSubject<OneYearRollingOperationChartData>({
    data: [],
    years: [],
    facilities: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<OneYearRollingOperationChartData> {
    console.log("[HourlyAccuracyChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<OneYearRollingOperationChartData>(this._endpoint)
        .pipe(
          map((value: OneYearRollingOperationChartData) => {
            console.log(
              "[HourlyAccuracyChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
