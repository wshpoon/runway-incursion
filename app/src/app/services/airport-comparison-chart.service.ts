import { Injectable } from "@angular/core";
import { AirportComparisonChartData } from "@app/models/airport-comparison-chart-data";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class AirportComparisonChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/airport-comparison-chart";
  private _dataSubject = new BehaviorSubject<AirportComparisonChartData>(null);
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<AirportComparisonChartData> {
    console.log("[HourlyAccuracyChartService] getData() called...");

    if (!this._requested) {
      const response = this.http
        .get<AirportComparisonChartData>(this._endpoint)
        .pipe(
          map((value: AirportComparisonChartData) => {
            console.log(
              "[HourlyAccuracyChartService] in pipe() -> map()",
              value
            );

            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
