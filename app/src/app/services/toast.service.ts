import { Injectable } from "@angular/core";
import {
  ToastController,
  IonicSafeString,
  AnimationBuilder,
} from "@ionic/angular";

export interface ToastOptions {
  header?: string;
  message?: string | IonicSafeString;
  cssClass?: string | string[];
  duration?: number;
  buttons?: (ToastButton | string)[];
  position?: "top" | "bottom" | "middle";
  color?:
    | "primary"
    | "secondary"
    | "tertiary"
    | "success"
    | "warning"
    | "danger"
    | "light"
    | "medium"
    | "dark";
  translucent?: boolean;
  animated?: boolean;
  keyboardClose?: boolean;
  id?: string;
  enterAnimation?: AnimationBuilder;
  leaveAnimation?: AnimationBuilder;
}

export interface ToastButton {
  text?: string;
  icon?: string;
  side?: "start" | "end";
  role?: "cancel" | string;
  cssClass?: string | string[];
  handler?: () => boolean | void | Promise<boolean | void>;
}

@Injectable({
  providedIn: "root",
})
export class ToastService {
  constructor(private toastController: ToastController) {}

  /**
   * Dispatch the toast notification.
   * 
   * @param options { ToastOptions }
   */
  async showToast({
    header = "Alert",
    message = "No message",
    position = "bottom",
    duration = 2000,
  }: ToastOptions) {
    const toast = await this.toastController.create({
      header,
      message,
      position,
      duration,
      color: "dark",
      buttons: [
        {
          icon: "close-outline",
          side: "end",
          handler: () => {
            toast.dismiss();
          },
        },
      ],
    });

    toast.present();
  }
}
