import { TestBed } from '@angular/core/testing';

import { OneYearRollingOperationChartService } from './one-year-rolling-operation-chart.service';

describe('OneYearRollingOperationChartService', () => {
  let service: OneYearRollingOperationChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OneYearRollingOperationChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
