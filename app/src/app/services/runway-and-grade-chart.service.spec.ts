import { TestBed } from '@angular/core/testing';

import { RunwayAndGradeChartService } from './runway-and-grade-chart.service';

describe('RunwayAndGradeChartService', () => {
  let service: RunwayAndGradeChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RunwayAndGradeChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
