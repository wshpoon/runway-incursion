import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export type AppTheme = "light" | "dark";

@Injectable({
  providedIn: "root",
})
export class ThemeService {
  private _themeSubject = new BehaviorSubject<AppTheme>("light");

  get theme() {
    return this._themeSubject.value;
  }

  set theme(value: AppTheme) {
    this._themeSubject.next(value);
  }

  public get isDark() {
    return localStorage.getItem("preferences.theme") === "dark";
  }

  constructor() {}
}
