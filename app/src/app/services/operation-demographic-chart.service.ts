import { Injectable } from "@angular/core";
import { of, BehaviorSubject, Observable } from "rxjs";
import { OperationDemographicChartData } from "@app/models/operation-demographic-chart-data";
import { HttpClient } from "@angular/common/http";
import staticData from "./static-data/operation-demographic-chart.json";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class OperationDemographicChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/operation-demographic-chart";
  private _dataSubject = new BehaviorSubject<OperationDemographicChartData>(
    null
  );

  constructor(private http: HttpClient) {}

  public getData(): Observable<OperationDemographicChartData> {
    console.log("[RunwayAndGradeChartService] getData() called...");
    if (!this._requested) {
      return this.http.get<OperationDemographicChartData>(this._endpoint).pipe(
        map((value) => {
          this._dataSubject.next(value);
          this._requested = true;

          return value;
        })
      );
    }

    return of(this._dataSubject.value);
  }

  public search(filters): Observable<OperationDemographicChartData> {
    console.log("[OperationDemographicChartService] search", filters);

    return this.http
      .get<OperationDemographicChartData>(
        this._endpoint +
          `/search/${filters.airport}/${filters.minDate}/${filters.maxDate}`
      )
      .pipe(
        map((value) => {
          this._dataSubject.next(value);

          return value;
        })
      );
  }

  public get mappedData() {
    const object = this._dataSubject.value;

    const labels = Object.keys(object);
    const values = Object.values(object);

    return {
      labels,
      values,
    };
  }
}
