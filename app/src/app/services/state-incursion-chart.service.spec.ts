import { TestBed } from '@angular/core/testing';

import { StateIncursionChartService } from './state-incursion-chart.service';

describe('StateIncursionChartService', () => {
  let service: StateIncursionChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StateIncursionChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
