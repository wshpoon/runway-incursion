import { Injectable } from "@angular/core";
import { of, BehaviorSubject, Observable } from "rxjs";
import { TypeAndGradeChartData } from "@app/models/type-and-grade-chart-data";
import { HttpClient } from "@angular/common/http";
import staticData from "./static-data/type-and-grade-chart.json";
import { share, map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class TypeAndGradeChartService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/type-and-grade-chart";
  private _dataSubject = new BehaviorSubject<TypeAndGradeChartData>({
    data: {},
    labels: [],
  });
  public data = this._dataSubject.asObservable();

  private _filterOptionState = new BehaviorSubject<{ categories: any[] }>({
    categories: [],
  });

  public get filterOptionState() {
    return this._filterOptionState;
  }

  constructor(private http: HttpClient) {}

  public getData(): Observable<TypeAndGradeChartData> {
    if (!this._requested) {
      const response = this.http
        .get<TypeAndGradeChartData>(this._endpoint)
        .pipe(
          map((value: TypeAndGradeChartData) => {
            console.log("[TypeAndGradeChartService] in pipe() -> map()", value);

            this._dataSubject.next(value);
            this._filterOptionState.next({
              categories: value.labels,
            });

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    }

    return of(this._dataSubject.value);
  }

  public setFilterOptionState(categories: any[]): void {
    this._filterOptionState.next({ categories });
  }
}
