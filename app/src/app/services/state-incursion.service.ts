import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StateIncursionChartData } from "@app/models/state-incursion-chart-data";
import { BehaviorSubject, Observable, of } from "rxjs";
import { map, share } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class StateIncursionService {
  private _requested: boolean = false;
  private _endpoint: string = "/api/incursions/state-incursion-chart/alternative";
  private _dataSubject = new BehaviorSubject<StateIncursionChartData>({
    data: [],
    states: [],
  });
  public data = this._dataSubject.asObservable();

  constructor(private http: HttpClient) {}

  public getData(): Observable<StateIncursionChartData> {
    if (!this._requested) {
      const response = this.http
        .get<StateIncursionChartData>(this._endpoint)
        .pipe(
          map((value: StateIncursionChartData) => {
            this._dataSubject.next(value);

            return value;
          }),
          share()
        );

      this._requested = true;

      return response;
    } else {
      return of(this._dataSubject.value);
    }
  }
}
