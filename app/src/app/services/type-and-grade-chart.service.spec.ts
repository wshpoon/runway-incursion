import { TestBed } from '@angular/core/testing';

import { TypeAndGradeChartService } from './type-and-grade-chart.service';

describe('TypeAndGradeChartService', () => {
  let service: TypeAndGradeChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeAndGradeChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
