import { ToastService } from "./../../../services/toast.service";
import { AuthenticationService } from "./../../../services/authentication.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { LoadingController } from "@ionic/angular";
import { ThemeService } from "@app/services/theme.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  params: Params;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = "";
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastService: ToastService,
    public loadingController: LoadingController,
    private themeService: ThemeService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(["/app/home"]);
    }
  }

  get logo() {
    return this.themeService.isDark
      ? "assets/images/dark/airplane.png"
      : "assets/images/airplane.png";
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl =
      this.route.snapshot.queryParams["returnUrl"] || "/app/home";

    this.params = this.route.snapshot.params;
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  async onSubmit() {
    console.log("[Login Page] onSubmit");

    const loading = await this.loadingController.create({
      message: "Please wait...",
      // duration: 2000,
    });

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    await loading.present();

    this.authenticationService
      .login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (data) => {
          this.toastService.showToast({
            header: "Authenticated",
            message: "You have logged in successfully.",
          });

          this.router.navigate([this.returnUrl]);
        },
        (error) => {
          this.error = error;
          this.loading = false;

          console.log("[LoginPage] Error", error);

          this.router.navigate([this.returnUrl]);
          this.toastService.showToast({
            message: "Something went wrong.",
          });
        }
      );

    await loading.dismiss();
  }
}
