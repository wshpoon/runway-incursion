import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StateIncursionPage } from './state-incursion.page';

describe('StateIncursionPage', () => {
  let component: StateIncursionPage;
  let fixture: ComponentFixture<StateIncursionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateIncursionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StateIncursionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
