import { Component, OnInit } from "@angular/core";
import { StateIncursionChartData } from "@app/models/state-incursion-chart-data";
import { StateIncursionService } from "@app/services/state-incursion.service";
import { BehaviorSubject, Subscription } from "rxjs";

@Component({
  selector: "app-state-incursion",
  templateUrl: "./state-incursion.page.html",
  styleUrls: ["./state-incursion.page.scss"],
})
export class StateIncursionPage implements OnInit {
  private _chartData: StateIncursionChartData;
  private _chartDataSubscription: Subscription;

  // google charts
  options = { region: "US", displayMode: "regions", resolution: "provinces" };
  columns = ["State", "Incursions"];
  data = [
    ["PHL", 230],
    ["MIA", 150],
  ];

  public isLoading$ = new BehaviorSubject(false);

  constructor(private dataService: StateIncursionService) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log("[StateIncursionPage] _chartDataSubscription", observer);

        this._chartData = observer;
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[StateIncursionPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;

    this.data = chartData.data.map((item) => {
      return [item.state, Number(item.total_incursions)];
    });
  }
}
