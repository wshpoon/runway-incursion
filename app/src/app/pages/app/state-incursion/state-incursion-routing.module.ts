import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StateIncursionPage } from './state-incursion.page';

const routes: Routes = [
  {
    path: '',
    component: StateIncursionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StateIncursionPageRoutingModule {}
