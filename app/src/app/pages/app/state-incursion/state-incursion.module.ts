import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StateIncursionPageRoutingModule } from './state-incursion-routing.module';

import { StateIncursionPage } from './state-incursion.page';
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StateIncursionPageRoutingModule,
    SharedModule
  ],
  declarations: [StateIncursionPage]
})
export class StateIncursionPageModule {}
