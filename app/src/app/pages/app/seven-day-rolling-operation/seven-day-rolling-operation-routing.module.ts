import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SevenDayRollingOperationPage } from './seven-day-rolling-operation.page';

const routes: Routes = [
  {
    path: '',
    component: SevenDayRollingOperationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SevenDayRollingOperationPageRoutingModule {}
