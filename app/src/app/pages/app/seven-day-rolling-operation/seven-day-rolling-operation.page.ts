import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  SevenDayRollingOperation,
  SevenDayRollingOperationChartData,
} from "@app/models/seven-day-rolling-operation";
import { ChartDataSets } from "chart.js";
import { Subscription, BehaviorSubject } from "rxjs";
import { SevenDayRollingOperationChartService } from "@app/services/seven-day-rolling-operation-chart.service";
import { Label } from "ng2-charts";
import { ApexAxisChartSeries } from "ng-apexcharts";

@Component({
  selector: "app-seven-day-rolling-operation",
  templateUrl: "./seven-day-rolling-operation.page.html",
  styleUrls: ["./seven-day-rolling-operation.page.scss"],
})
export class SevenDayRollingOperationPage implements OnInit, OnDestroy {
  private _chartData: SevenDayRollingOperationChartData = {};
  private _chartDataSubscription: Subscription;

  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _dates: string[] = [];

  get series() {
    return this._series;
  }

  get categories() {
    return this._categories;
  }

  get dates() {
    return this._dates;
  }

  public isLoading$ = new BehaviorSubject(false);

  constructor(private dataService: SevenDayRollingOperationChartService) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (data: SevenDayRollingOperationChartData) => {
        console.log(
          "[SevenDayRollingOperationPage] ngOnInit -> _chartDataSubscription",
          data
        );

        this._chartData = data;
        this._categories = data.labels;
        this._dates = data.dates;
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[SevenDayRollingOperationPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let currentYear = new Date().getFullYear().toString();
    let series: ApexAxisChartSeries = [];

    for (const key in chartData.years) {
      console.log("[SevenDayRollingOperationPage] year " + key);

      const data = chartData.years[key];

      let _data = [];

      data.forEach((element: SevenDayRollingOperation) => {
        _data.push(Number(element.average_operations));
      });

      series.push({
        data: _data,
        name: key,
      });
    }

    this._series = series;
    this._categories = categories;

    console.log(series, categories);
  }
}
