import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SevenDayRollingOperationPage } from './seven-day-rolling-operation.page';

describe('SevenDayRollingOperationPage', () => {
  let component: SevenDayRollingOperationPage;
  let fixture: ComponentFixture<SevenDayRollingOperationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SevenDayRollingOperationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SevenDayRollingOperationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
