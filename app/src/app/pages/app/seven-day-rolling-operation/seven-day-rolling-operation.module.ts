import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";

import { SevenDayRollingOperationPageRoutingModule } from "./seven-day-rolling-operation-routing.module";
import { SevenDayRollingOperationPage } from "./seven-day-rolling-operation.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SevenDayRollingOperationPageRoutingModule,
    SharedModule,
  ],
  declarations: [SevenDayRollingOperationPage],
})
export class SevenDayRollingOperationPageModule {}
