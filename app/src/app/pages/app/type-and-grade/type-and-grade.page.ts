import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  TypeAndGrade,
  TypeAndGradeChartData,
} from "@app/models/type-and-grade-chart-data";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { ModalController } from "@ionic/angular";
import { TypeAndGradeChartService } from "@app/services/type-and-grade-chart.service";
import { Router } from "@angular/router";
import { Subscription, BehaviorSubject } from "rxjs";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastService } from "@app/services/toast.service";
import { FilterSelectionComponent } from "@app/components/app/filter-selection/filter-selection.component";

@Component({
  selector: "app-type-and-grade",
  templateUrl: "./type-and-grade.page.html",
  styleUrls: ["./type-and-grade.page.scss"],
})
export class TypeAndGradePage implements OnInit, OnDestroy {
  private _categoriesSubscription: Subscription;
  private _chartDataSubscription: Subscription;
  private _chartData: TypeAndGradeChartData = null;

  /**
   * The array data based on selected categories.
   *
   * @return array
   */
  private _series: ApexAxisChartSeries = [];
  private _allCategories: any[] = [];

  /**
   * The selected categories to be listed in chart.
   *
   * @return string[]
   */
  private _categories: string[] = [];
  private _aircraftTypes: string[] = [];
  public isLoading$ = new BehaviorSubject<boolean>(false);
  public filtersFormGroup: FormGroup;

  public get series(): ApexAxisChartSeries {
    return this._series;
  }

  public get categories(): string[] {
    return this._categories;
  }

  public get allCategories(): any[] {
    return this._allCategories;
  }

  public get aircraftTypes(): string[] {
    return this._aircraftTypes;
  }

  public get filtersFormGroupValues() {
    return this.filtersFormGroup.value;
  }

  constructor(
    private router: Router,
    private dataService: TypeAndGradeChartService,
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private toastService: ToastService
  ) {
    this.filtersFormGroup = this.formBuilder.group({
      aircraftType: [null],
      minDate: [null],
      maxDate: [null],
    });
  }

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log(
          "[TypeAndGradeStackedChartComponent] ngOnInit -> _chartDataSubscription()",
          observer
        );

        this._chartData = observer;
        this._categories = observer.labels;
        this._aircraftTypes = observer.types;
      },
      (error) => console.log(error),
      () => {
        // subscribe to categories
        this._categoriesSubscription = this.dataService.filterOptionState.subscribe(
          (data) => {
            this._allCategories = data.categories;

            this._categories = data.categories;

            // this._categories = data.categories
            //   .filter((category) => category.selected)
            //   .map((category) => category.name);

            console.log(
              "[TypeAndGradeStackedChartComponent] _categoriesSubscription",
              this._categories,
              this._chartData
            );
          },
          (error) => {
            console.log("[TypeAndGradeStackedChartComponent] ERROR", error);
          },
          () => {
            console.log("[TypeAndGradeStackedChartComponent] on complete");
          }
        );

        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[TypeAndGradePage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
    this._categoriesSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    let chartData = this._chartData;
    let categories = this._categories;
    let _series: ApexAxisChartSeries = [];

    for (const key in chartData.data) {
      const data = chartData.data[key];

      let _data: number[] = [];

      categories.forEach((label) => {
        const sum = data
          .filter(
            (element: TypeAndGrade) => element.incdnt_type_faa_code === label
          )
          .filter((element: TypeAndGrade) => {
            const formGroupValues = this.filtersFormGroup.value;

            if (formGroupValues.aircraftType === null) {
              return true;
            } else {
              if (!!formGroupValues.aircraftType) {
                const isTypeMatched =
                  formGroupValues.aircraftType === element.acft_1_type;

                return isTypeMatched;
              } else {
                return true;
              }
            }
          })
          .filter((element: TypeAndGrade) => {
            const formGroupValues = this.filtersFormGroup.value;

            if (
              formGroupValues.minDate === null &&
              formGroupValues.maxDate === null
            ) {
              return true;
            } else {
              const incursionDate = new Date(element.date);
              const minDate = new Date(formGroupValues.minDate);
              const maxDate = new Date(formGroupValues.maxDate);

              const inDateRange =
                incursionDate >= minDate && incursionDate <= maxDate;

              return inDateRange;
            }
          })
          .map((value) => value.total_incursions)
          .reduce((a: any, b: any) => parseInt(a) + parseInt(b), 0);

        _data.push(sum);
      });

      _series.push({
        name: key,
        data: _data,
      });
    }

    console.log(
      "[TypeAndGradeStackedChartComponent] setChartData() > _series",
      _series,
      categories
    );

    this._series = _series;
    this._categories = categories;
  }

  reset() {
    this.filtersFormGroup.reset();
    this.setChartOptionsData();
  }

  onSubmit() {
    if (this.filtersFormGroup.valid) {
      console.log(this.filtersFormGroup.value);

      this.setChartOptionsData();
    } else {
      this.toastService.showToast({
        message: "Please fill in the required fields.",
      });
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FilterSelectionComponent,
      componentProps: {
        dropdown: {
          autocomplete: false,
          label: "Select Airport Type",
          data: this._aircraftTypes,
        },
        defaultValues: {
          item: null,
          minDate: "2001-01-01",
          maxDate: "2020-01-01",
        },
        allRequired: false,
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log("[TypeAndGradePage] presentModal() -> onWillDismiss...", data);

    if (data.reload) {
      this._onModalDismiss(data.values);
    }
  }

  private _onModalDismiss(data) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this.filtersFormGroup.setValue({
        aircraftType: data.item,
        minDate: data.minDate,
        maxDate: data.maxDate,
      });

      this.onSubmit();

      console.log(data, this.filtersFormGroup.value);

      this.isLoading$.next(false);
    }, 500);
  }
}
