import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TypeAndGradePage } from './type-and-grade.page';

const routes: Routes = [
  {
    path: '',
    component: TypeAndGradePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TypeAndGradePageRoutingModule {}
