import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { TypeAndGradePageRoutingModule } from "./type-and-grade-routing.module";

import { TypeAndGradePage } from "./type-and-grade.page";
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TypeAndGradePageRoutingModule,
    SharedModule,
  ],
  declarations: [
    TypeAndGradePage,
  ],
})
export class TypeAndGradePageModule {}
