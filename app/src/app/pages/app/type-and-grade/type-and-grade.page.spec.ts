import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TypeAndGradePage } from './type-and-grade.page';

describe('TypeAndGradePage', () => {
  let component: TypeAndGradePage;
  let fixture: ComponentFixture<TypeAndGradePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeAndGradePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TypeAndGradePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
