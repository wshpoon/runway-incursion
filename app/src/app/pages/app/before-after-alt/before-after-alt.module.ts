import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { BeforeAfterAltPageRoutingModule } from "./before-after-alt-routing.module";

import { BeforeAfterAltPage } from "./before-after-alt.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeforeAfterAltPageRoutingModule,
    SharedModule,
  ],
  declarations: [BeforeAfterAltPage],
})
export class BeforeAfterAltPageModule {}
