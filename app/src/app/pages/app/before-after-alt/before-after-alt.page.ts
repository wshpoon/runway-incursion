import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  BeforeAfter,
  BeforeAfterChartData,
} from "@app/models/before-after-chart-data";
import { BeforeAfterChartService } from "@app/services/before-after-chart.service";
import { BehaviorSubject, Subscription } from "rxjs";

@Component({
  selector: "app-before-after-alt",
  templateUrl: "./before-after-alt.page.html",
  styleUrls: ["./before-after-alt.page.scss"],
})
export class BeforeAfterAltPage implements OnInit, OnDestroy {
  private _chartDataSubscription: Subscription;
  private _chartData: BeforeAfterChartData = {};
  private _xaxis: any[] = [];
  private _series = [];

  public isLoading$ = new BehaviorSubject<boolean>(false);

  private _data = [];
  private _options = {
    width: "auto",
    labels: ["X", "Y"],
    drawPoints: true,
    drawAxesAtZero: true,
    strokeWidth: 0.0,
  };

  get data() {
    return this._data;
  }

  get options() {
    return this._options;
  }

  get series() {
    return this._series;
  }

  get xaxis() {
    return this._xaxis;
  }

  constructor(private dataService: BeforeAfterChartService) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (data) => {
        console.log(
          "[BeforeAfterPage] ngOnInit -> _chartDataSubscription()",
          data
        );
        this._chartData = data;

        this._xaxis = data.xaxis
          .filter((item) => !!Number(item))
          .map((item) => {
            return [Number(item)];
          });

        console.log("_xaxis", this._xaxis);
      },
      (error) => {
        console.log(error);
      },
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[BeforeAfterAltPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  public setChartOptionsData() {
    const chartData = this._chartData;

    this._options.labels = ["X"];
    let _series = [];

    // labels
    for (const key in chartData.data) {
      this._options.labels.push(key);
    }

    this._options.labels.forEach((item) => {
      if (item !== "X") {
        const data: BeforeAfter[] = chartData.data[item];

        console.log("data", data);

        this._xaxis.forEach((item, index) => {
          const x = item[0];

          let yaxisData = data
            .filter((item) => Number(item.x) === Number(x))
            .map((item) => Number(item.y));

          // yaxisData.forEach((yaxisItem) => {
          //   console.log("yaxisItem", yaxisItem);

          // });

          this._xaxis[index].push(Number(yaxisData[0]) || 0);
        });
      }
    });

    this._series = _series;
    // this._options.labels = [...this._options.labels, ...this._categories];

    console.log("[BeforeAfterPage] setChartData() > _series", {
      series: this._series,
      categories: this._xaxis,
      options: this._options,
    });
  }
}
