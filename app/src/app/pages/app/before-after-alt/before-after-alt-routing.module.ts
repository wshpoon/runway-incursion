import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeforeAfterAltPage } from './before-after-alt.page';

const routes: Routes = [
  {
    path: '',
    component: BeforeAfterAltPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeforeAfterAltPageRoutingModule {}
