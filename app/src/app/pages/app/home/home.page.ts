import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { BeforeAfterChartService } from "@app/services/before-after-chart.service";
import { RunwayAndGradeChartService } from "@app/services/runway-and-grade-chart.service";
import { Observable, of, Subscription } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { ThemeService } from "@app/services/theme.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit, OnDestroy {
  public params: Params;
  public appPages = [
    {
      title: "Before-After",
      url: "/app/before-after",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/scatter-chart.png`,
    },
    {
      title: "Airport Comparison",
      url: "/app/airport-comparison",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/bar-chart.png`,
    },
    {
      title: "Seven Day Rolling Operation",
      url: "/app/seven-day-rolling-operation",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/line-chart.png`,
    },
    {
      title: "Operation Demographic",
      url: "/app/operation-demographic",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/pie-chart.png`,
    },
    {
      title: "Type and Grade",
      url: "/app/type-and-grade",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/bar-chart.png`,
    },
    {
      title: "Runway and Grade",
      url: "/app/runway-and-grade",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/bar-chart.png`,
    },
    {
      title: "Incursion Totals",
      url: "/app/incursion-total",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/bar-chart.png`,
    },
    {
      title: "Hourly Operation",
      url: "/app/hourly-operation",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/line-chart.png`,
    },
    {
      title: "Hourly Data Accuracy",
      url: "/app/hourly-accuracy",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/line-chart.png`,
    },
    {
      title: "Hourly Incursion Rates",
      url: "/app/hourly-incursion",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/line-chart.png`,
    },
    {
      title: "Daily One-Year Rolling Incursion Rates",
      url: "/app/one-year-rolling-operation",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/line-chart.png`,
    },
    {
      title: "State Incursion",
      url: "/app/state-incursion",
      image: `assets/charts${
        this.themeService.isDark ? "/dark" : ""
      }/line-chart.png`,
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private themeService: ThemeService
  ) {}

  ngOnInit() {
    this.params = this.route.snapshot.params;
  }

  ngOnDestroy() {}

  navigateTo(page: any) {
    this.router.navigate([page.url]);
  }
}
