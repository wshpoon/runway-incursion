import { User } from "./../../models/user";
import { Router } from "@angular/router";
import { AuthenticationService } from "@app/services/authentication.service";
import { Component, OnInit } from "@angular/core";
import { ThemeService } from "@app/services/theme.service";
import { ModalController, AlertController } from "@ionic/angular";
import { SettingsComponent } from "@app/components/settings/settings.component";

@Component({
  selector: "app-app",
  templateUrl: "./app.page.html",
  styleUrls: ["./app.page.scss"],
})
export class AppPage implements OnInit {
  currentUser: User;

  public selectedIndex = 0;
  public appPages = [
    {
      title: "Home",
      url: "/app/home",
      icon: "home",
    },
    // {
    //   title: "Before-After",
    //   url: "/app/before-after",
    //   icon: "home",
    // },
    {
      title: "Incursion Rate",
      url: "/app/incursion-rate",
      icon: "home",
    },
    {
      title: "Airport Comparison",
      url: "/app/airport-comparison",
      icon: "home",
    },
    {
      title: "Seven Day Rolling Operation",
      url: "/app/seven-day-rolling-operation",
      icon: "home",
    },
    {
      title: "Operation Demographic",
      url: "/app/operation-demographic",
      icon: "home",
    },
    {
      title: "Type and Grade",
      url: "/app/type-and-grade",
      icon: "home",
    },
    {
      title: "Runway and Grade",
      url: "/app/runway-and-grade",
      icon: "home",
    },
    {
      title: "Incursion Totals",
      url: "/app/incursion-total",
      icon: "home",
    },
    {
      title: "Hourly Operation",
      url: "/app/hourly-operation",
      icon: "home",
    },
    {
      title: "Hourly Data Accuracy",
      url: "/app/hourly-accuracy",
      icon: "home",
    },
    {
      title: "Hourly Incursion Rates",
      url: "/app/hourly-incursion",
      icon: "home",
    },
    {
      title: "Daily One-Year Rolling Incursion Rates",
      url: "/app/one-year-rolling-operation",
      icon: "home",
    },
    {
      title: "State Incursions",
      url: "/app/state-incursion",
      icon: "home",
    },
  ];

  get logo() {
    return this.themeService.isDark
      ? "assets/images/dark/airplane.png"
      : "assets/images/airplane.png";
  }

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private themeService: ThemeService,
    private modalController: ModalController,
    private alertController: AlertController
  ) {
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  ngOnInit() {
    console.log("[App Page] ngOnInit", this.currentUser);
  }

  async attemptLogout() {
    const alert = await this.alertController.create({
      header: "Logout Confirmation",
      message: "Are you sure you want to <strong>logout</strong>?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            alert.dismiss();
          },
        },
        {
          text: "Yes",
          role: "logout",
          handler: async (value) => {
            console.log(value);

            await this.logout();
          },
        },
      ],
    });

    await alert.present();
  }

  async logout() {
    this.authenticationService.logout();
    await this.router.navigate(["/auth/login"]);
  }

  async showSettingsModal() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      keyboardClose: true,
      swipeToClose: true,
    });

    await modal.present();
  }
}
