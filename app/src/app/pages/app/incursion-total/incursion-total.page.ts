import { Component, OnDestroy, OnInit } from "@angular/core";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { IncursionTotalChartService } from "@app/services/incursion-total-chart.service";
import { Subscription, BehaviorSubject } from "rxjs";
import {
  IncursionTotalChartData,
  IncursionTotal,
} from "@app/models/incursion-total-chart-data";
import { ModalController } from "@ionic/angular";

import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";
import { AppCharts, Charts } from '@app/enums/charts.enum';

@Component({
  selector: "app-incursion-total",
  templateUrl: "./incursion-total.page.html",
  styleUrls: ["./incursion-total.page.scss"],
})
export class IncursionTotalPage implements OnInit, OnDestroy {
  private _chartData: IncursionTotalChartData;
  private _chartDataSubscription: Subscription;

  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _allCategories: any[] = [];

  public get categories() {
    return this._categories;
  }

  public get series() {
    return this._series;
  }

  public get allCategories() {
    return this._allCategories;
  }

  // The available charts in the page
  private _pageCharts: Array<string> = [];

  public get pageCharts() {
    return this._pageCharts;
  }

  public get charts() {
    return {
      bar: Charts.BarChart,
      radar: Charts.RadarChart,
    };
  }

  private _currentChart: string | null = null;

  get currentChart() {
    return this._currentChart;
  }

  set currentChart(value: string) {
    this._currentChart = value;
  }

  public isLoading$ = new BehaviorSubject(false);

  constructor(
    private dataService: IncursionTotalChartService,
    private modalController: ModalController
  ) {
    const charts = [Charts.BarChart, Charts.RadarChart];

    this._currentChart = Charts.BarChart;

    this._pageCharts = AppCharts.filter((item) => {
      return charts.includes(item);
    });
  }

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log("[IncursionTotalPage] _chartDataSubscription", observer);

        this._chartData = observer;

        this._allCategories = observer.facilities.map((facility) => {
          const defaultSelected = [
            "CRJ2",
            "B737",
            "E170",
            "E145",
            "A320",
            "B738",
            "E190",
            "A319",
            "CRJ7",
            "CRJ9",
            "GLF5",
            "BE20",
          ].includes(facility);

          return {
            name: facility.toString(),
            selected: defaultSelected,
          };
        });

        this._categories = observer.facilities
          .map((facility) => facility.toString())
          .filter((item) =>
            this._allCategories
              .filter((item) => item.selected)
              .map((item) => item.name)
              .includes(item)
          );

        console.log("[IncursionTotalPage] this._categories", this._categories);
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[IncursionTotalPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let series: ApexAxisChartSeries = [];

    for (const key in chartData.data) {
      const data = chartData.data[key];

      let _data: number[] = [];

      categories.forEach((acft_1_rwy_sfty_type) => {
        const sum = data
          .filter(
            (element: IncursionTotal) =>
              element.acft_1_rwy_sfty_type === acft_1_rwy_sfty_type
          )
          .map((value) => Number(value.total_incursions))
          .reduce((a, b) => a + b, 0);

        _data.push(sum);
      });

      series.push({
        name: key,
        data: _data,
      });
    }

    this._categories = categories;
    this._series = series.filter((seriesData) => {
      return this._allCategories
        .filter((item) => item.selected)
        .map((item) => item.name)
        .includes(seriesData.name);
    });

    console.log(
      "[IncursionTotalPage] setChartOptionsData() > _series",
      series,
      categories
    );
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CategorySelectionComponent,
      componentProps: {
        categories: this._allCategories,
        title: "Filter by Type of Aircraft",
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(
      "[IncursionTotalPage] presentModal() -> onWillDismiss...",
      data
    );

    if (data.reload) {
      this._onModalDismiss(data.categories);
    }
  }

  private _onModalDismiss(categories: any[]) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this._categories = categories
        .filter((category) => category.selected)
        .map((category) => category.name);

      this._allCategories = categories;

      this.setChartOptionsData();
      this.isLoading$.next(false);
    }, 1000);
  }
}
