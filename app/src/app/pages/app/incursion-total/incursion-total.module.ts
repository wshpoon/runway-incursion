import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { IncursionTotalPageRoutingModule } from "./incursion-total-routing.module";

import { IncursionTotalPage } from "./incursion-total.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncursionTotalPageRoutingModule,
    SharedModule,
  ],
  declarations: [IncursionTotalPage],
})
export class IncursionTotalPageModule {}
