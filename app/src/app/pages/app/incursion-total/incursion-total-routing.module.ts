import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncursionTotalPage } from './incursion-total.page';

const routes: Routes = [
  {
    path: '',
    component: IncursionTotalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncursionTotalPageRoutingModule {}
