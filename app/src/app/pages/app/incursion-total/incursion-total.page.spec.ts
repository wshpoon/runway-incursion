import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IncursionTotalPage } from './incursion-total.page';

describe('IncursionTotalPage', () => {
  let component: IncursionTotalPage;
  let fixture: ComponentFixture<IncursionTotalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncursionTotalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IncursionTotalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
