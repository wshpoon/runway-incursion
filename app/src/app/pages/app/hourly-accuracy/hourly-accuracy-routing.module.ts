import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HourlyAccuracyPage } from './hourly-accuracy.page';

const routes: Routes = [
  {
    path: '',
    component: HourlyAccuracyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HourlyAccuracyPageRoutingModule {}
