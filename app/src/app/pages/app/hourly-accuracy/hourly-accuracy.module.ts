import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { HourlyAccuracyPageRoutingModule } from "./hourly-accuracy-routing.module";

import { HourlyAccuracyPage } from "./hourly-accuracy.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HourlyAccuracyPageRoutingModule,
    SharedModule,
  ],
  declarations: [HourlyAccuracyPage],
})
export class HourlyAccuracyPageModule {}
