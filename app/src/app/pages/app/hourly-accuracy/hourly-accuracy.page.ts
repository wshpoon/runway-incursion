import { Component, OnDestroy, OnInit } from "@angular/core";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { HourlyAccuracyChartService } from "@app/services/hourly-accuracy-chart.service";
import { HourlyAccuracyChartData } from "@app/models/hourly-accuracy-chart-data";
import { Subscription, BehaviorSubject } from "rxjs";
import { ModalController } from "@ionic/angular";

import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";

@Component({
  selector: "app-hourly-accuracy",
  templateUrl: "./hourly-accuracy.page.html",
  styleUrls: ["./hourly-accuracy.page.scss"],
})
export class HourlyAccuracyPage implements OnInit, OnDestroy {
  private _chartData: HourlyAccuracyChartData;
  private _chartDataSubscription: Subscription;

  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _allCategories: any[] = [];

  public get categories() {
    return this._categories;
  }

  public get allCategories() {
    return this._allCategories;
  }

  public get series() {
    return this._series;
  }

  isLoading$ = new BehaviorSubject(false);

  constructor(
    private dataService: HourlyAccuracyChartService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log("[HourlyOperationPage] _chartDataSubscription", observer);

        this._chartData = observer;

        this._allCategories = observer.facilities.map((facility) => {
          const defaultSelected = ["ATL", "JAN", "MSP"].includes(facility);

          return {
            name: facility.toString(),
            selected: defaultSelected,
          };
        });

        this._categories = observer.facilities
          .map((facility) => facility.toString())
          .filter((item) =>
            this._allCategories
              .filter((item) => item.selected)
              .map((item) => item.name)
              .includes(item)
          );
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[HourlyAccuracyPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let series: ApexAxisChartSeries = [];

    console.log(
      "[RunwayAndGradeStackedChartComponent] setChartData() > _series",
      series,
      categories
    );

    categories.forEach((facility) => {
      series.push({
        name: facility,
        data: chartData.data
          .filter((item) => item.facility.toString() === facility.toString())
          .map((item) => Number(item.total_events)),
      });
    });

    this._categories = categories;
    this._series = series;

    console.log(series, categories);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CategorySelectionComponent,
      componentProps: {
        categories: this._allCategories,
        title: "Filter by Airport",
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(
      "[IncursionTotalPage] presentModal() -> onWillDismiss...",
      data
    );

    if (data.reload) {
      this._onModalDismiss(data.categories);
    }
  }

  private _onModalDismiss(categories: any[]) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this._categories = categories
        .filter((category) => category.selected)
        .map((category) => category.name);

      this._allCategories = categories;

      this.setChartOptionsData();
      this.isLoading$.next(false);
    }, 1000);
  }
}
