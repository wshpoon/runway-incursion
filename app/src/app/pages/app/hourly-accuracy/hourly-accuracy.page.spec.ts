import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HourlyAccuracyPage } from './hourly-accuracy.page';

describe('HourlyAccuracyPage', () => {
  let component: HourlyAccuracyPage;
  let fixture: ComponentFixture<HourlyAccuracyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HourlyAccuracyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HourlyAccuracyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
