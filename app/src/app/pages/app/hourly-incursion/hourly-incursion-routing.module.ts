import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HourlyIncursionPage } from './hourly-incursion.page';

const routes: Routes = [
  {
    path: '',
    component: HourlyIncursionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HourlyIncursionPageRoutingModule {}
