import { Component, OnDestroy, OnInit } from "@angular/core";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { Subscription, BehaviorSubject } from "rxjs";
import { HourlyIncursionChartService } from "@app/services/hourly-incursion-chart.service";
import { ModalController } from "@ionic/angular";

import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";
import {
  HourlyIncursion,
  HourlyIncursionChartData,
} from "@app/models/hourly-incursion-chart-data";

@Component({
  selector: "app-hourly-incursion",
  templateUrl: "./hourly-incursion.page.html",
  styleUrls: ["./hourly-incursion.page.scss"],
})
export class HourlyIncursionPage implements OnInit, OnDestroy {
  private _chartData: HourlyIncursionChartData;
  private _chartDataSubscription: Subscription;

  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _facilities: string[] = [];
  private _allCategories: any[] = [];

  public get categories() {
    return this._categories;
  }

  public get allCategories() {
    return this._allCategories;
  }

  public get facilities() {
    return this._facilities;
  }

  public get series() {
    return this._series;
  }

  isLoading$ = new BehaviorSubject(false);

  constructor(
    private dataService: HourlyIncursionChartService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log("[HourlyOperationPage] _chartDataSubscription", observer);

        this._chartData = observer;

        this._categories = observer.hours.map((hour) => hour.toString());

        this._allCategories = observer.facilities.map((facility) => {
          const defaultSelected = ["ATL", "JAN", "MSP"].includes(facility);

          return {
            name: facility.toString(),
            selected: defaultSelected,
          };
        });

        this._facilities = observer.facilities
          .map((facility) => facility.toString())
          .filter((item) =>
            this._allCategories
              .filter((item) => item.selected)
              .map((item) => item.name)
              .includes(item)
          );
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[HourlyIncursionPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    const facilities = this._facilities;
    let series: ApexAxisChartSeries = [];

    console.log(
      "[RunwayAndGradeStackedChartComponent] setChartData() > _series",
      series,
      categories
    );

    // facilities.forEach((facility) => {
    //   series.push({
    //     name: facility,
    //     data: chartData.data
    //       .filter((item) => item.facility.toString() === facility.toString())
    //       .map((item) => Number(item.incursion_rates)),
    //   });
    // });

    for (const key in chartData.data) {
      const data = chartData.data[key];

      let _data: number[] = [];

      categories.forEach((hour) => {
        const sum = data
          .filter(
            (element: HourlyIncursion) => element.hour.toString() === hour
          )
          .map((value) => value.incursion_rates)
          .reduce((a, b) => a + b, 0);

        _data.push(sum);
      });

      series.push({
        name: key,
        data: _data,
      });
    }

    this._categories = categories;
    this._series = series.filter((seriesData) => {
      return this._allCategories
        .filter((item) => item.selected)
        .map((item) => item.name)
        .includes(seriesData.name);
    });

    console.log(this._series, this._categories);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CategorySelectionComponent,
      componentProps: {
        categories: this._allCategories,
        title: "Filter by Airport",
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(
      "[IncursionTotalPage] presentModal() -> onWillDismiss...",
      data
    );

    if (data.reload) {
      this._onModalDismiss(data.categories);
    }
  }

  private _onModalDismiss(categories: any[]) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this._facilities = categories
        .filter((category) => category.selected)
        .map((category) => category.name);

      this._allCategories = categories;

      this.setChartOptionsData();
      this.isLoading$.next(false);
    }, 1000);
  }
}
