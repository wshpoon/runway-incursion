import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HourlyIncursionPage } from './hourly-incursion.page';

describe('HourlyIncursionPage', () => {
  let component: HourlyIncursionPage;
  let fixture: ComponentFixture<HourlyIncursionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HourlyIncursionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HourlyIncursionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
