import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { HourlyIncursionPageRoutingModule } from "./hourly-incursion-routing.module";

import { HourlyIncursionPage } from "./hourly-incursion.page";
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HourlyIncursionPageRoutingModule,
    SharedModule,
  ],
  declarations: [
    HourlyIncursionPage,
  ],
})
export class HourlyIncursionPageModule {}
