import { Component, OnDestroy, OnInit } from "@angular/core";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { AirportComparisonChartService } from "@app/services/airport-comparison-chart.service";
import { Subscription, BehaviorSubject } from "rxjs";
import {
  AirportComparisonChartData,
  AirportComparison,
} from "@app/models/airport-comparison-chart-data";
import { ModalController } from "@ionic/angular";
import { onlyUnique } from "@app/utils/main.util";

import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";
import { AppCharts, Charts } from "@app/enums/charts.enum";

@Component({
  selector: "app-airport-comparison",
  templateUrl: "./airport-comparison.page.html",
  styleUrls: ["./airport-comparison.page.scss"],
})
export class AirportComparisonPage implements OnInit, OnDestroy {
  private _chartData: AirportComparisonChartData;
  private _chartDataSubscription: Subscription;
  private _seriesIncursionRates: ApexAxisChartSeries = [];
  private _seriesOperationTotals: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _allCategories: any[] = [];

  public get categories() {
    return this._categories;
  }

  public get allCategories() {
    return this._allCategories;
  }

  public get seriesIncursionRates() {
    return this._seriesIncursionRates;
  }

  public get seriesOperationTotals() {
    return this._seriesOperationTotals;
  }

  // The available charts in the page
  private _pageCharts: Array<string> = [];

  public get pageCharts() {
    return this._pageCharts;
  }

  public get charts() {
    return {
      bar: Charts.BarChart,
      radar: Charts.RadarChart,
    };
  }

  private _currentChart: string | null = null;

  get currentChart() {
    return this._currentChart;
  }

  set currentChart(value: string) {
    this._currentChart = value;
  }

  public isLoading$ = new BehaviorSubject<boolean>(false);

  constructor(
    private dataService: AirportComparisonChartService,
    private modalController: ModalController
  ) {
    const charts = [Charts.BarChart, Charts.RadarChart];

    this._currentChart = Charts.BarChart;

    this._pageCharts = AppCharts.filter((item) => {
      return charts.includes(item);
    });
  }

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log("[AirportComparison] _chartDataSubscription...", observer);

        this._chartData = observer;

        this._allCategories = observer.facilities
          .map((item) => item.replace(/ /g, "")) // remove whitespace, since duplicates have whitespace
          .filter(onlyUnique)
          .map((facility) => {
            const defaultSelected = [
              "CLT",
              "BOS",
              "SFO",
              "LAX",
              "MDW",
              "HNL",
              "SEA",
              "SLC",
              "PHL",
              "MIA",
              "DTW",
              "FLL",
              "DCA",
              "ORD",
              "LAS",
              "MSP",
              "BWI",
              "EWR",
              "DFW",
              "TPA",
              "JFK",
              "ATL",
              "PHX",
              "MEM",
              "LGA",
              "IAD",
              "SAN",
              "DEN",
              "IAH",
              "MCO",
            ].includes(facility);

            return {
              name: facility.toString(),
              selected: defaultSelected,
            };
          });

        this._categories = observer.facilities
          .map((facility) => facility.toString())
          .filter((item) =>
            this._allCategories
              .filter((item) => item.selected)
              .map((item) => item.name)
              .includes(item)
          );
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[AirportComparisonPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * @todo
   */
  filter() {}

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let seriesIncursionRates: ApexAxisChartSeries = [];
    let seriesOperationTotals: ApexAxisChartSeries = [];

    console.log(
      "[AirportComparisonPage] setChartOptionsData() > _series",
      seriesOperationTotals,
      seriesIncursionRates,
      categories
    );

    /** operation totals */
    for (const key in chartData.operation_totals) {
      const data = chartData.operation_totals[key];

      let _dataOperationTotals: number[] = [];

      categories.forEach((facility) => {
        const sumOperationTotals = data
          .filter((element: AirportComparison) => element.facility === facility)
          .map((value) => Number(value.total_operations))
          .reduce((a, b) => Number(a) + Number(b), 0);

        _dataOperationTotals.push(sumOperationTotals);
      });

      seriesOperationTotals.push({
        name: key,
        data: _dataOperationTotals,
      });
    }

    /** incursion rates */
    for (const key in chartData.operation_totals) {
      const data = chartData.operation_totals[key];

      let _dataIncursionRates: number[] = [];

      categories.forEach((facility) => {
        const sumIncursionRates = data
          .filter((element: AirportComparison) => element.facility === facility)
          .map((value) => Number(value.total_incursion_rate))
          .reduce((a, b) => Number(a) + Number(b), 0);

        _dataIncursionRates.push(sumIncursionRates);
      });

      seriesIncursionRates.push({
        name: key,
        data: _dataIncursionRates,
      });
    }

    this._categories = categories;

    this._seriesIncursionRates = seriesIncursionRates.filter((seriesData) => {
      return this._allCategories
        .filter((item) => item.selected)
        .map((item) => item.name)
        .includes(seriesData.name);
    });

    this._seriesOperationTotals = seriesOperationTotals.filter((seriesData) => {
      return this._allCategories
        .filter((item) => item.selected)
        .map((item) => item.name)
        .includes(seriesData.name);
    });

    console.log(
      this._categories,
      this._seriesIncursionRates,
      this._seriesOperationTotals
    );
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CategorySelectionComponent,
      componentProps: {
        categories: this._allCategories,
        title: "Filter by Airport",
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(
      "[IncursionTotalPage] presentModal() -> onWillDismiss...",
      data
    );

    if (data.reload) {
      this._onModalDismiss(data.categories);
    }
  }

  private _onModalDismiss(categories: any[]) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this._categories = categories
        .filter((category) => category.selected)
        .map((category) => category.name);

      this._allCategories = categories;

      this.setChartOptionsData();
      this.isLoading$.next(false);
    }, 1000);
  }
}
