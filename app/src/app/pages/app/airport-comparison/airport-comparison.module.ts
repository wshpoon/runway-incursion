import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { AirportComparisonPageRoutingModule } from "./airport-comparison-routing.module";

import { AirportComparisonPage } from "./airport-comparison.page";
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AirportComparisonPageRoutingModule,
    SharedModule,
  ],
  declarations: [
    AirportComparisonPage,
  ],
})
export class AirportComparisonPageModule {}
