import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AirportComparisonPage } from './airport-comparison.page';

const routes: Routes = [
  {
    path: '',
    component: AirportComparisonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AirportComparisonPageRoutingModule {}
