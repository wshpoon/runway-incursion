import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AirportComparisonPage } from './airport-comparison.page';

describe('AirportComparisonPage', () => {
  let component: AirportComparisonPage;
  let fixture: ComponentFixture<AirportComparisonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirportComparisonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AirportComparisonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
