import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  HourlyOperationChartData,
  HourlyOperation1,
  HourlyOperation2,
} from "@app/models/hourly-operation-chart-data";
import { Subscription, BehaviorSubject } from "rxjs";
import { HourlyOperationChartService } from "@app/services/hourly-operation-chart.service";
import { ApexAxisChartSeries } from "ng-apexcharts";

@Component({
  selector: "app-hourly-operation",
  templateUrl: "./hourly-operation.page.html",
  styleUrls: ["./hourly-operation.page.scss"],
})
export class HourlyOperationPage implements OnInit, OnDestroy {
  private _chartData: HourlyOperationChartData;
  private _chartDataSubscription: Subscription;

  private _series_1: ApexAxisChartSeries = [];
  private _series_2: ApexAxisChartSeries = [];
  private _categories: string[] = [];

  public get categories() {
    return this._categories;
  }

  public get series_1() {
    return this._series_1;
  }

  public get series_2() {
    return this._series_2;
  }

  isLoading$ = new BehaviorSubject(false);

  constructor(private dataService: HourlyOperationChartService) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log("[HourlyOperationPage] _chartDataSubscription", observer);

        this._chartData = observer;
        this._categories = observer.hours.map((hour) => hour.toString());
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[HourlyOperationPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let series_1: ApexAxisChartSeries = [];
    let series_2: ApexAxisChartSeries = [];

    const fields_1 = [
      { text: "Average Operations", value: "average_operations" },
      { text: "Total Departures", value: "total_departures" },
      { text: "Total Arrivals", value: "total_arrivals" },
    ];

    const fields_2 = [
      { text: "Average Operations", value: "average_operations" },
      { text: "Commercial Arrivals", value: "commercial_arrivals" },
      { text: "Commercial Departures", value: "commercial_departures" },
      { text: "GA Arrivals", value: "ga_arrivals" },
      { text: "GA Departures", value: "ga_departures" },
      { text: "Military Arrivals", value: "military_arrivals" },
      { text: "Military Departures", value: "military_departures" },
    ];

    // #1
    fields_1.forEach((field) => {
      let _data: number[] = [];

      categories.forEach((hour) => {
        const data = chartData.data.average_hourly_operations_1[hour];

        const sum = data
          .filter(
            (element: HourlyOperation1) => element.hour.toString() === hour
          )
          .map((value) => value[field.value])
          .reduce((a, b) => a + b, 0);

        _data.push(sum);
      });

      series_1.push({
        name: field.text,
        data: _data,
      });
    });

    // #2
    fields_2.forEach((field) => {
      let _data: number[] = [];

      categories.forEach((hour) => {
        const data = chartData.data.average_hourly_operations_2[hour];

        const sum = data
          .filter(
            (element: HourlyOperation1) => element.hour.toString() === hour
          )
          .map((value) => value[field.value])
          .reduce((a, b) => a + b, 0);

        _data.push(sum);
      });

      series_2.push({
        name: field.text,
        data: _data,
      });
    });

    this._categories = categories;
    this._series_1 = series_1;
    this._series_2 = series_2;

    console.log(
      "[RunwayAndGradeStackedChartComponent] setChartData() > _series_1 && _series_2",
      { series_1, series_2, categories }
    );
  }
}
