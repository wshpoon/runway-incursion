import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { HourlyOperationPageRoutingModule } from "./hourly-operation-routing.module";

import { HourlyOperationPage } from "./hourly-operation.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HourlyOperationPageRoutingModule,
    SharedModule,
  ],
  declarations: [HourlyOperationPage],
})
export class HourlyOperationPageModule {}
