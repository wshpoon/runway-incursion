import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HourlyOperationPage } from './hourly-operation.page';

describe('HourlyOperationPage', () => {
  let component: HourlyOperationPage;
  let fixture: ComponentFixture<HourlyOperationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HourlyOperationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HourlyOperationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
