import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription, BehaviorSubject } from "rxjs";
import {
  BeforeAfterChartData,
  BeforeAfter,
} from "@app/models/before-after-chart-data";
import { BeforeAfterChartService } from "@app/services/before-after-chart.service";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { ChartDataSets, ChartData } from "chart.js";
import { ModalController } from "@ionic/angular";
import {
  DynamicFilterComponent,
  DynamicFilterComponentProps,
  InputField,
} from "@app/components/app/dynamic-filter/dynamic-filter.component";
import { isObjectEmpty } from "@app/utils/main.util";

@Component({
  selector: "app-before-after",
  templateUrl: "./before-after.page.html",
  styleUrls: ["./before-after.page.scss"],
})
export class BeforeAfterPage implements OnInit, OnDestroy {
  private _chartDataSubscription: Subscription;
  private _chartData: BeforeAfterChartData = {};
  private _categories: string[] = [];
  private _series = [];
  private _filteredSeries = [];
  public isLoading$ = new BehaviorSubject<boolean>(false);
  filters = {
    type: "AIRPORT",
    xMin: 0,
    xMax: 350000,
    yMin: 0,
    yMax: 200000,
  };

  get series() {
    return this._series;
  }

  get filteredSeries() {
    return this._filteredSeries;
  }

  get categories(): string[] {
    return this._categories;
  }

  constructor(
    private dataService: BeforeAfterChartService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.isLoading$.next(true);
    this._chartDataSubscription = this.dataService.getData().subscribe(
      (data) => {
        this._chartData = data;
        this._categories = data.labels;
        this._series = data.series;
        this._filteredSeries = data.series;
      },
      (error) => {
        console.log(error);
      },
      () => {
        // this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    this._chartDataSubscription.unsubscribe();
  }

  public setChartOptionsData() {
    const chartData = this._chartData;
    const categories = this._categories;

    let _series = [];

    for (const key in chartData.data) {
      const data: BeforeAfter[] = chartData.data[key];

      _series.push({
        name: key,
        type: "scatter",
        data: data.map((item) => [Number(item.x), Number(item.y)]) as [
          number,
          number
        ][],
      });
    }

    this._series = _series;
  }

  async presentDynamicFilterModal() {
    const modal = await this.modalController.create({
      component: DynamicFilterComponent,
      swipeToClose: true,
      presentingElement: await this.modalController.getTop(),
      componentProps: {
        fields: [
          {
            label: "Type",
            type: "dropdown",
            autocomplete: false,
            formControlName: "type",
            required: false,
            initialValue: this.filters.type || "AIRPORT",
            options: this._categories.map((item) => ({
              text: item,
              value: item,
            })),
          },
          {
            label: "X-Axis Min Value",
            type: "number",
            autocomplete: false,
            formControlName: "xMin",
            required: true,
            initialValue: this.filters.xMin || 0,
          },
          {
            label: "X-Axis Max Value",
            type: "number",
            autocomplete: false,
            formControlName: "xMax",
            required: true,
            initialValue: this.filters.xMax || 0,
          },
          {
            label: "Y-Axis Min Value",
            type: "number",
            autocomplete: false,
            formControlName: "yMin",
            required: true,
            initialValue: this.filters.yMin || 0,
          },
          {
            label: "Y-Axis Max Value",
            type: "number",
            autocomplete: false,
            formControlName: "yMax",
            required: true,
            initialValue: this.filters.yMax || 0,
          },
        ] as InputField[],
      } as DynamicFilterComponentProps,
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();

    console.log("[BeforeAfterPage] presentModal() -> onWillDismiss...", data);

    if (data !== undefined && data.reload) {
      this.onDynamicFilterModalDismiss(data.values);
    }
  }

  private onDynamicFilterModalDismiss(data: any) {
    this.isLoading$.next(true);

    setTimeout(() => {
      for (const key in data) {
        if (key !== "type") data[key] = Number(data[key]);
      }

      this.filters = { ...this.filters, ...data };
      this._filteredSeries = this._series.filter((item) => {
        if (this.filters.type === null) return true;

        return this.filters.type.includes(item.name);
      });

      console.log("filters", this.filters);

      this.isLoading$.next(false);
    }, 500);
  }
}
