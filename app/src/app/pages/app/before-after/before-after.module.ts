import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { BeforeAfterPageRoutingModule } from "./before-after-routing.module";

import { BeforeAfterPage } from "./before-after.page";
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeforeAfterPageRoutingModule,
    SharedModule,
  ],
  declarations: [
    BeforeAfterPage,
  ],
})
export class BeforeAfterPageModule {}
