import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  IncursionRate,
  IncursionRateChartData,
} from "@app/models/incursion-rate-chart-data";
import { IncursionRateChartService } from "@app/services/incursion-rate-chart.service";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { BehaviorSubject, Subscription } from "rxjs";

@Component({
  selector: "app-incursion-rate",
  templateUrl: "./incursion-rate.page.html",
  styleUrls: ["./incursion-rate.page.scss"],
})
export class IncursionRatePage implements OnInit, OnDestroy {
  private _chartData: IncursionRateChartData = {};
  private _chartDataSubscription: Subscription;

  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _dates: string[] = [];

  get series() {
    return this._series;
  }

  get categories() {
    return this._categories;
  }

  get dates() {
    return this._dates;
  }

  public isLoading$ = new BehaviorSubject(false);

  constructor(private dataService: IncursionRateChartService) {}

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (data: IncursionRateChartData) => {
        console.log(
          "[IncursionRatePage] ngOnInit -> _chartDataSubscription",
          data
        );

        this._chartData = data;
        this._categories = data.labels;
        this._dates = data.dates;
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[IncursionRatePage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let currentYear = new Date().getFullYear().toString();
    let series: ApexAxisChartSeries = [];

    for (const key in chartData.years) {
      console.log("[IncursionRatePage] year " + key);

      const data = chartData.years[key];

      // console.log(
      //   data.length,
      //   data.filter((item) => !!!Number(item.incursion_rate)).length
      // );

      let _data = [];

      data
        .filter((item) => !!Number(item.incursion_rate))
        .forEach((item) => {
          _data.push(Number(item.incursion_rate || 0));
        });

      series.push({
        data: _data,
        name: key,
      });
    }

    this._series = series;
    this._categories = categories;

    console.log(series, categories);
  }
}
