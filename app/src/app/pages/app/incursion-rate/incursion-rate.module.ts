import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { IncursionRatePageRoutingModule } from "./incursion-rate-routing.module";

import { IncursionRatePage } from "./incursion-rate.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncursionRatePageRoutingModule,
    SharedModule,
  ],
  declarations: [IncursionRatePage],
})
export class IncursionRatePageModule {}
