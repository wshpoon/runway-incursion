import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IncursionRatePage } from './incursion-rate.page';

describe('IncursionRatePage', () => {
  let component: IncursionRatePage;
  let fixture: ComponentFixture<IncursionRatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncursionRatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IncursionRatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
