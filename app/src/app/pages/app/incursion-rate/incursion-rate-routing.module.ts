import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncursionRatePage } from './incursion-rate.page';

const routes: Routes = [
  {
    path: '',
    component: IncursionRatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncursionRatePageRoutingModule {}
