import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OperationDemographicPage } from './operation-demographic.page';

describe('OperationDemographicPage', () => {
  let component: OperationDemographicPage;
  let fixture: ComponentFixture<OperationDemographicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDemographicPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OperationDemographicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
