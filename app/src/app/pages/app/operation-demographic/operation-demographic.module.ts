import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { OperationDemographicPageRoutingModule } from "./operation-demographic-routing.module";

import { OperationDemographicPage } from "./operation-demographic.page";
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OperationDemographicPageRoutingModule,
    SharedModule,
  ],
  declarations: [
    OperationDemographicPage,
  ],
})
export class OperationDemographicPageModule {}
