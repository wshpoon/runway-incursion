import { Component, OnDestroy, OnInit } from "@angular/core";
import { OperationDemographicChartService } from "@app/services/operation-demographic-chart.service";
import { Subscription, BehaviorSubject } from "rxjs";
import { ApexAxisChartSeries, ApexNonAxisChartSeries } from "ng-apexcharts";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ToastService } from "@app/services/toast.service";
import { ModalController } from "@ionic/angular";

import { FilterSelectionComponent } from "@app/components/app/filter-selection/filter-selection.component";

@Component({
  selector: "app-operation-demographic",
  templateUrl: "./operation-demographic.page.html",
  styleUrls: ["./operation-demographic.page.scss"],
})
export class OperationDemographicPage implements OnInit, OnDestroy {
  public isLoading$ = new BehaviorSubject<boolean>(false);
  public filtersFormGroup: FormGroup;
  public search = "";

  private _series: ApexNonAxisChartSeries = [];
  private _labels: string[] = [];
  private _airports: string[] = [];
  private _chartDataSubscription: Subscription;
  private _searchSubscription: Subscription;

  public get series(): ApexNonAxisChartSeries {
    return this._series;
  }

  public get labels(): string[] {
    return this._labels;
  }

  public get airports(): string[] {
    return this._airports;
  }

  constructor(
    private dataService: OperationDemographicChartService,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private modalController: ModalController
  ) {
    this.filtersFormGroup = this.formBuilder.group({
      airport: [null, Validators.required],
      minDate: ["2001-01-01", Validators.required],
      maxDate: ["2020-01-01", Validators.required],
    });
  }

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer) => {
        console.log(
          "[OperationDemographicPage] _chartDataSubscription",
          observer
        );

        const labels = Object.keys(observer.data);
        const values = Object.values(observer.data);

        console.log(values, labels, observer.airports);

        labels.shift();
        values.shift();

        this._series = values.map((item: any) => parseInt(item));
        this._labels = labels;
        this._airports = observer.airports;

        console.log(values, labels, observer.airports);
      },
      (error) => console.log(error),
      () => {
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[OperationDemographicPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  onSubmit() {
    if (this.filtersFormGroup.valid) {
      this.isLoading$.next(true);

      console.log(
        "[OperationDemographicPage] onSubmit",
        this.filtersFormGroup.value
      );

      this._searchSubscription = this.dataService
        .search(this.filtersFormGroup.value)
        .subscribe((observer) => {
          const labels = Object.keys(observer.data);
          const values = Object.values(observer.data);

          labels.shift();
          values.shift();

          console.log(values, labels, observer.airports);

          this._series = values.map((item: any) => parseInt(item));
          this._labels = labels;
          this._airports = observer.airports;

          this.isLoading$.next(false);

          console.log(values, labels, observer.airports);
        });
    } else {
      this.toastService.showToast({
        message:
          "Please fill in the required fields before dispatching your query.",
      });
    }
  }

  onChange(event: Event) {
    console.log(event);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FilterSelectionComponent,
      componentProps: {
        dropdown: {
          autocomplete: true,
          label: "Select Airport",
          data: this._airports,
        },
        defaultValues: {
          item: null,
          minDate: "2001-01-01",
          maxDate: "2020-01-01",
        },
        allRequired: true,
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(
      "[OperationDemographicPage] presentModal() -> onWillDismiss...",
      data
    );

    if (data.reload) {
      this._onModalDismiss(data.values);
    }
  }

  private _onModalDismiss(data) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this.filtersFormGroup.setValue({
        airport: data.item,
        minDate: data.minDate,
        maxDate: data.maxDate,
      });

      this.onSubmit();

      console.log(data, this.filtersFormGroup.value);

      this.isLoading$.next(false);
    }, 500);
  }
}
