import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OperationDemographicPage } from './operation-demographic.page';

const routes: Routes = [
  {
    path: '',
    component: OperationDemographicPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OperationDemographicPageRoutingModule {}
