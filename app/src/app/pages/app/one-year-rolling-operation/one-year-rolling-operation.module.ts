import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { OneYearRollingOperationPageRoutingModule } from "./one-year-rolling-operation-routing.module";

import { OneYearRollingOperationPage } from "./one-year-rolling-operation.page";
import { SharedModule } from "@app/modules/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OneYearRollingOperationPageRoutingModule,
    SharedModule,
  ],
  declarations: [OneYearRollingOperationPage],
})
export class OneYearRollingOperationPageModule {}
