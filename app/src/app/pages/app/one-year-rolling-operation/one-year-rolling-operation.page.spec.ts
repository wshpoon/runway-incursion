import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OneYearRollingOperationPage } from './one-year-rolling-operation.page';

describe('OneYearRollingOperationPage', () => {
  let component: OneYearRollingOperationPage;
  let fixture: ComponentFixture<OneYearRollingOperationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneYearRollingOperationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OneYearRollingOperationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
