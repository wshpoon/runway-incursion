import { Component, OnDestroy, OnInit } from "@angular/core";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { OneYearRollingOperationChartService } from "@app/services/one-year-rolling-operation-chart.service";
import { BehaviorSubject, Subscription } from "rxjs";
import {
  OneYearRollingOperationChartData,
  OneYearRollingOperation,
} from "@app/models/one-year-rolling-operation-chart-data";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastService } from "@app/services/toast.service";
import { ModalController } from "@ionic/angular";

import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";
import { FilterSelectionComponent } from "@app/components/app/filter-selection/filter-selection.component";

@Component({
  selector: "app-one-year-rolling-operation",
  templateUrl: "./one-year-rolling-operation.page.html",
  styleUrls: ["./one-year-rolling-operation.page.scss"],
})
export class OneYearRollingOperationPage implements OnInit, OnDestroy {
  private _chartData: OneYearRollingOperationChartData = {};
  private _chartDataSubscription: Subscription;

  private _series: ApexAxisChartSeries = [];
  private _categories: string[] = [];
  private _allCategories: string[] = [];
  private _facilities: string[] = [];

  public get series() {
    return this._series;
  }

  public get categories() {
    return this._categories;
  }

  public get allCategories() {
    return this._allCategories;
  }

  public get facilities() {
    return this._facilities;
  }

  public isLoading$ = new BehaviorSubject(false);
  public filtersFormGroup: FormGroup;

  constructor(
    private dataService: OneYearRollingOperationChartService,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private modalController: ModalController
  ) {
    this.filtersFormGroup = this.formBuilder.group({
      facility: [null],
      minDate: [null],
      maxDate: [null],
    });
  }

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (observer: OneYearRollingOperationChartData) => {
        console.log(
          "[OneYearRollingOperationPage] ngOnInit -> _chartDataSubscription",
          observer
        );

        this._chartData = observer;

        this._categories = observer.years;

        this._allCategories = observer.years;

        this._facilities = observer.facilities;
      },
      (error) => console.log(error),
      () => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }
    );
  }

  ngOnDestroy() {
    console.log("[OneYearRollingOperationPage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let series: ApexAxisChartSeries = [];

    let seriesNationalAverageData: number[] = [];
    let seriesSelectedAirportData: number[] = [];

    let groupedByYears = {};

    categories.forEach((year) => {
      const groupedByYear = chartData.data.filter(
        (item) =>
          new Date(item.date).getFullYear().toString() === year.toString()
      );

      groupedByYears[year] = groupedByYear;
    });

    console.log(groupedByYears);

    const filterByDateRange = (item: OneYearRollingOperation) => {
      const data = this.filtersFormGroup.value;

      if (data.minDate === null || data.maxDate === null) {
        return true;
      }

      return (
        new Date(item.date) >= new Date(data.minDate) &&
        new Date(item.date) <= new Date(data.maxDate)
      );
    };

    // National Average
    categories.forEach((year) => {
      const data: OneYearRollingOperation[] = groupedByYears[year];

      seriesNationalAverageData.push(
        data
          .filter(filterByDateRange)
          .map((item) => Number(item.incursion_rate))
          .reduce((a, b) => a + b, 0)
      );
    });

    const filterByFacility = (item: OneYearRollingOperation) => {
      const data = this.filtersFormGroup.value;

      return data !== null ? item.facility === data.facility : true;
    };

    // Selected Airport
    categories.forEach((year) => {
      const data: OneYearRollingOperation[] = groupedByYears[year];

      seriesSelectedAirportData.push(
        data
          .filter(filterByDateRange)
          .filter(filterByFacility)
          .map((item) => Number(item.incursion_rate))
          .reduce((a, b) => a + b, 0)
      );
    });

    // push the data to series array
    series.push({
      name: "National Average",
      data: seriesNationalAverageData,
    });

    series.push({
      name: "Selected Airport",
      data: seriesSelectedAirportData,
    });

    this._series = series;

    this._categories = categories;

    console.log(series, categories);
  }

  public onSubmit() {
    if (this.filtersFormGroup.valid) {
      this.isLoading$.next(true);

      console.log(
        "[OneYearRollingOperationPage] onSubmit()",
        this.filtersFormGroup.value
      );

      setTimeout(() => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }, 500);
    } else {
      this.toastService.showToast({
        message: "Please fill in required fields.",
      });
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FilterSelectionComponent,
      componentProps: {
        dropdown: {
          label: "Select Airport",
          data: this._facilities,
        },
        defaultValues: {
          item: null,
          minDate: "2001-01-01",
          maxDate: "2020-01-01",
        },
        allRequired: false,
        title: "Filter by Airport & Date Range",
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();

    if (data.reload) {
      this._onModalDismiss(data.values);
    }
  }

  private _onModalDismiss(data) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this.filtersFormGroup.setValue({
        facility: data.item,
        minDate: data.minDate,
        maxDate: data.maxDate,
      });

      this.onSubmit();

      this.isLoading$.next(false);
    }, 500);
  }
}
