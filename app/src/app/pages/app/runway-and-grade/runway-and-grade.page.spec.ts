import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RunwayAndGradePage } from './runway-and-grade.page';

describe('RunwayAndGradePage', () => {
  let component: RunwayAndGradePage;
  let fixture: ComponentFixture<RunwayAndGradePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunwayAndGradePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RunwayAndGradePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
