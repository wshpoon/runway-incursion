import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { RunwayAndGradePageRoutingModule } from "./runway-and-grade-routing.module";

import { RunwayAndGradePage } from "./runway-and-grade.page";
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RunwayAndGradePageRoutingModule,
    SharedModule,
  ],
  declarations: [
    RunwayAndGradePage,
  ],
})
export class RunwayAndGradePageModule {}
