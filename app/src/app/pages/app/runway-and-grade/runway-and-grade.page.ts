import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { RunwayAndGradeChartService } from "@app/services/runway-and-grade-chart.service";
import { Subscription, BehaviorSubject } from "rxjs";
import {
  RunwayAndGradeChartData,
  RunwayAndGrade,
} from "@app/models/runway-and-grade-chart-data";
import { ApexAxisChartSeries } from "ng-apexcharts";
import { ModalController, ActionSheetController } from "@ionic/angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastService } from "@app/services/toast.service";

import { CategorySelectionComponent } from "@app/components/app/category-selection/category-selection.component";
import { FilterSelectionComponent } from "@app/components/app/filter-selection/filter-selection.component";

@Component({
  selector: "app-runway-and-grade",
  templateUrl: "./runway-and-grade.page.html",
  styleUrls: ["./runway-and-grade.page.scss"],
})
export class RunwayAndGradePage implements OnInit, OnDestroy {
  private _categoriesSubscription: Subscription;
  private _chartDataSubscription: Subscription;
  private _chartData: RunwayAndGradeChartData = null;

  /**
   * The array data based on selected categories.
   *
   * @return array
   */
  private _series: ApexAxisChartSeries = [];
  private _allCategories: any[] = [];

  /**
   * The selected categories to be listed in chart.
   *
   * @return string[]
   */
  private _categories: string[] = [];
  private _airports: string[] = [];

  public isLoading$ = new BehaviorSubject<boolean>(false);
  public filtersFormGroup: FormGroup;

  public get series(): ApexAxisChartSeries {
    return this._series;
  }

  public get categories(): string[] {
    return this._categories;
  }

  public get allCategories(): any[] {
    return this._allCategories;
  }

  public get airports(): string[] {
    return this._airports;
  }

  public get filtersFormGroupValues() {
    return this.filtersFormGroup.value;
  }

  constructor(
    private router: Router,
    private dataService: RunwayAndGradeChartService,
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private actionSheetController: ActionSheetController
  ) {
    this.filtersFormGroup = this.formBuilder.group({
      airport: [null],
      minDate: [null],
      maxDate: [null],
    });
  }

  ngOnInit() {
    this.isLoading$.next(true);

    this._chartDataSubscription = this.dataService.getData().subscribe(
      (data) => {
        console.log(
          "[RunwayAndGradeStackedChartComponent] ngOnInit -> _chartDataSubscription()",
          data
        );

        this._chartData = data;

        this._airports = data.airports;
      },
      (error) => console.log(error),
      () => {
        // subscribe to categories
        this._categoriesSubscription = this.dataService.filterOptionState.subscribe(
          (data) => {
            this._allCategories = data.categories;

            this._categories = data.categories
              .filter((category) => category.selected)
              .map((category) => category.name);

            console.log(
              "[RunwayAndGradeStackedChartComponent] _categoriesSubscription",
              this._categories,
              this._chartData
            );

            this.setChartOptionsData();
            this.isLoading$.next(false);
          },
          (error) => console.log(error),
          () => {
            this.isLoading$.next(false);
          }
        );
      }
    );
  }

  ngOnDestroy() {
    console.log("[RunwayAndGradePage] ngOnDestroy() called");

    this._chartDataSubscription.unsubscribe();
    this._categoriesSubscription.unsubscribe();
  }

  async presentCategoryModal() {
    const modal = await this.modalController.create({
      component: CategorySelectionComponent,
      componentProps: {
        categories: this._allCategories,
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(
      "[RunwayAndGradePage] presentModal() -> onWillDismiss...",
      data
    );

    if (data.reload) {
      this._onCategoryModalDismiss(data.categories);
    }
  }

  private _onCategoryModalDismiss(categories: any[]) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this._categories = categories
        .filter((category) => category.selected)
        .map((category) => category.name);

      this._allCategories = categories;

      this.setChartOptionsData();
      this.isLoading$.next(false);
    }, 500);
  }

  async presentFiltersModal() {
    const modal = await this.modalController.create({
      component: FilterSelectionComponent,
      componentProps: {
        dropdown: {
          label: "Select Airport",
          data: this._airports,
        },
        defaultValues: {
          item: null,
          minDate: "2001-01-01",
          maxDate: "2020-01-01",
        },
        allRequired: false,
        title: "Filter by Airport & Date Range",
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log("presentFiltersModal", data);

    if (data.reload) {
      this._onFiltersModalDismiss(data.values);
    }
  }
  private _onFiltersModalDismiss(data) {
    this.isLoading$.next(true);

    setTimeout(() => {
      this.filtersFormGroup.setValue({
        airport: data.item,
        minDate: data.minDate,
        maxDate: data.maxDate,
      });

      this.onSubmit();

      this.isLoading$.next(false);
    }, 500);
  }

  /**
   * Set chart options data.
   *
   * @return { void }
   */
  public setChartOptionsData(): void {
    const chartData = this._chartData;
    const categories = this._categories;
    let series: ApexAxisChartSeries = [];

    for (const key in chartData.codes) {
      const data = chartData.codes[key];

      let _data: number[] = [];

      categories.forEach((category) => {
        const sum = data
          .filter((item) => {
            if (this.filtersFormGroup.value.airport === null) return true;

            return item.facility === this.filtersFormGroup.value.airport;
          })
          .filter((element: RunwayAndGrade) => {
            const formGroupValues = this.filtersFormGroup.value;

            if (
              formGroupValues.minDate === null &&
              formGroupValues.maxDate === null
            ) {
              return true;
            } else {
              const incursionDate = new Date(element.date);
              const minDate = new Date(formGroupValues.minDate);
              const maxDate = new Date(formGroupValues.maxDate);

              const inDateRange =
                incursionDate >= minDate && incursionDate <= maxDate;

              return inDateRange;
            }
          })
          .filter(
            (element: RunwayAndGrade) =>
              element.event_tkof_lndg_desc === category
          )
          .map((value) => value.total_incursions)
          .reduce((a, b) => parseInt(a) + parseInt(b), 0);

        _data.push(sum);
      });

      series.push({
        name: key,
        data: _data,
      });
    }

    console.log(
      "[RunwayAndGradeStackedChartComponent] setChartData() > _series",
      series,
      categories
    );

    this._series = series;
    this._categories = categories;
  }

  public onSubmit() {
    if (this.filtersFormGroup.valid) {
      this.isLoading$.next(true);

      setTimeout(() => {
        this.setChartOptionsData();
        this.isLoading$.next(false);
      }, 500);
    } else {
      this.toastService.showToast({
        message: "Please fill in required fields.",
      });
    }
  }

  resetFilters() {
    this.filtersFormGroup.reset();

    this.setChartOptionsData();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: "Filter Options",
      buttons: [
        {
          text: "Select Runways",
          handler: () => {
            this.presentCategoryModal();
          },
        },
        {
          text: "Date & Airport",
          handler: () => {
            this.presentFiltersModal();
          },
        },
        {
          text: "Reset Filters",
          handler: () => {
            this.resetFilters();
          },
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    await actionSheet.present();
  }
}
