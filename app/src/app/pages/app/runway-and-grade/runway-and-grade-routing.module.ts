import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RunwayAndGradePage } from './runway-and-grade.page';

const routes: Routes = [
  {
    path: '',
    component: RunwayAndGradePage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RunwayAndGradePageRoutingModule {}
