import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AppPage } from "./app.page";

const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "",
    component: AppPage,
    children: [
      {
        path: "home",
        loadChildren: () =>
          import("./home/home.module").then((m) => m.HomePageModule),
      },
      {
        path: "before-after",
        loadChildren: () =>
          import("./before-after/before-after.module").then(
            (m) => m.BeforeAfterPageModule
          ),
      },
      // alt
      {
        path: "before-after-alt",
        loadChildren: () =>
          import("./before-after-alt/before-after-alt.module").then(
            (m) => m.BeforeAfterAltPageModule
          ),
      },
      {
        path: "seven-day-rolling-operation",
        loadChildren: () =>
          import(
            "./seven-day-rolling-operation/seven-day-rolling-operation.module"
          ).then((m) => m.SevenDayRollingOperationPageModule),
      },
      {
        path: "runway-and-grade",
        loadChildren: () =>
          import("./runway-and-grade/runway-and-grade.module").then(
            (m) => m.RunwayAndGradePageModule
          ),
      },
      {
        path: "type-and-grade",
        loadChildren: () =>
          import("./type-and-grade/type-and-grade.module").then(
            (m) => m.TypeAndGradePageModule
          ),
      },
      {
        path: "operation-demographic",
        loadChildren: () =>
          import("./operation-demographic/operation-demographic.module").then(
            (m) => m.OperationDemographicPageModule
          ),
      },
      {
        path: "hourly-operation",
        loadChildren: () =>
          import("./hourly-operation/hourly-operation.module").then(
            (m) => m.HourlyOperationPageModule
          ),
      },
      {
        path: "hourly-incursion",
        loadChildren: () =>
          import("./hourly-incursion/hourly-incursion.module").then(
            (m) => m.HourlyIncursionPageModule
          ),
      },
      {
        path: "hourly-accuracy",
        loadChildren: () =>
          import("./hourly-accuracy/hourly-accuracy.module").then(
            (m) => m.HourlyAccuracyPageModule
          ),
      },
      {
        path: "airport-comparison",
        loadChildren: () =>
          import("./airport-comparison/airport-comparison.module").then(
            (m) => m.AirportComparisonPageModule
          ),
      },
      {
        path: "incursion-total",
        loadChildren: () =>
          import("./incursion-total/incursion-total.module").then(
            (m) => m.IncursionTotalPageModule
          ),
      },
      {
        path: "one-year-rolling-operation",
        loadChildren: () =>
          import(
            "./one-year-rolling-operation/one-year-rolling-operation.module"
          ).then((m) => m.OneYearRollingOperationPageModule),
      },
      {
        path: "state-incursion",
        loadChildren: () =>
          import("./state-incursion/state-incursion.module").then(
            (m) => m.StateIncursionPageModule
          ),
      },
      {
        path: "incursion-rate",
        loadChildren: () =>
          import("./incursion-rate/incursion-rate.module").then(
            (m) => m.IncursionRatePageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppPageRoutingModule {}
