export function onlyUnique(value: any, index: number, self: any) {
  return self.indexOf(value) === index;
}

/**
 * Checks if object is empty.
 */
export const isObjectEmpty = (object: object) => {
  return Object.keys(object).length === 0 && object.constructor === Object;
};
