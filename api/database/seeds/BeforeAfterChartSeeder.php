<?php

use App\Repository\BeforeAfterChartRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BeforeAfterChartSeeder extends Seeder
{
    protected $beforeAfterChartRepositoryInterface;

    public function __construct(BeforeAfterChartRepositoryInterface $beforeAfterChartRepositoryInterface)
    {
        $this->beforeAfterChartRepositoryInterface = $beforeAfterChartRepositoryInterface;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timeStart = Carbon::parse(now()->timestamp);

        print_r("[BeforeAfterChartSeeder] Executing... " . PHP_EOL . PHP_EOL);

        print_r("[BeforeAfterChartSeeder] beforeAfterChartRepository -> createBeforeTable() executing..." . PHP_EOL);
        $this->beforeAfterChartRepositoryInterface->createBeforeTable();
        print_r("[BeforeAfterChartSeeder] beforeAfterChartRepository -> createBeforeTable() done!" . PHP_EOL);
        print_r("[BeforeAfterChartSeeder] since ... " . $timeStart->diffForHumans() . PHP_EOL . PHP_EOL);

        print_r("[BeforeAfterChartSeeder] beforeAfterChartRepository -> createAfterTable() executing..." . PHP_EOL);
        $this->beforeAfterChartRepositoryInterface->createAfterTable();
        print_r("[BeforeAfterChartSeeder] beforeAfterChartRepository -> createAfterTable() done!" . PHP_EOL);
        print_r("[BeforeAfterChartSeeder] since ... " . $timeStart->diffForHumans() . PHP_EOL . PHP_EOL);

        print_r("[BeforeAfterChartSeeder] Done!" . PHP_EOL);
    }
}
