<?php

use App\Import\ImportAirportInformation;
use Illuminate\Database\Seeder;

class AirportInformationSeeder extends Seeder
{
    protected $importAirportInformation;

    public function __construct(ImportAirportInformation $importAirportInformation)
    {
        $this->importAirportInformation = $importAirportInformation;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->importAirportInformation->execute(
            '/home/carlomigueldy/Documents/Runway Incursion/airport-information-ii.csv'
        );
    }
}
