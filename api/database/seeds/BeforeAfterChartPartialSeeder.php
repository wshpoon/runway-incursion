<?php

use App\Repository\BeforeAfterChartRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BeforeAfterChartPartialSeeder extends Seeder
{
    /**
     * @var BeforeAfterChartRepositoryInterface
     */
    protected $repository;

    /**
     * @param BeforeAfterChartRepositoryInterface
     */
    public function __construct(BeforeAfterChartRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // current total count 73223
        // recorded at 6:45pm GMT +8

        $types = [
            // "BEFORE",
            "AFTER"
        ];

        function getYearRangeByType($type) {
            if ($type == "BEFORE") {
                return range(2003, 2012);
            } else if ($type == "AFTER") {
                return range(2019, 2020);
            }

            return null;
        }

        $timeStart = Carbon::parse(now()->timestamp);

        foreach($types as $type) {
            print_r("[BeforeAfterChartPartialSeeder] Executing... " . PHP_EOL . PHP_EOL);

            foreach(getYearRangeByType($type) as $year) {
                $_yearTimeStart = Carbon::parse(now()->timestamp);
                $startingMonth = $year == 2019 ? 12 : 1;

                print_r("[BeforeAfterChartPartialSeeder] Looping through from year $year ... ". $timeStart->diffForHumans() . PHP_EOL . PHP_EOL);

                foreach (range($startingMonth,12) as $month) {
                    $_monthTimeStart = Carbon::parse(now()->timestamp);

                    print_r("[BeforeAfterChartPartialSeeder] Looping through from month $month ... " . $timeStart->diffForHumans() . PHP_EOL . PHP_EOL);
                    $query = $this->repository->getBeforeAfterQuery(
                        $type,
                        now()->year($year)->month($month)->startOfMonth()->format('Y-m-d'),
                        now()->year($year)->month($month)->endOfMonth()->format('Y-m-d'),
                    );

                    $table = $type == 'BEFORE' ? 'before_table' : 'after_table';

                    $statement = "INSERT INTO ". $table ." (
                        label,
                        date,
                        facility,
                        state,
                        total_events,
                        total_incursions,
                        average_incursions,
                        total_operations,
                        average_operations,
                        incursion_rate)
                        $query
                    ";

                    info($statement);

                    // execute statement
                    DB::select(DB::raw($statement));

                    print_r("[BeforeAfterChartPartialSeeder] Inserted records with in month of $month and year $year ... " . $timeStart->diffForHumans() . PHP_EOL . PHP_EOL);

                    print_r("[BeforeAfterChartPartialSeeder] MONTH $month done ... " . $_monthTimeStart->diffForHumans() . PHP_EOL . PHP_EOL);
                }

                print_r("[BeforeAfterChartPartialSeeder] YEAR $year done ... " . $_yearTimeStart->diffForHumans() . PHP_EOL . PHP_EOL);
            }

            print_r("[BeforeAfterChartPartialSeeder] Done! ... TOTAL EXEC TIME " . $timeStart->diffForHumans() . PHP_EOL);
        }
    }
}
