<?php

use App\Import\ImportIncursion;
use Illuminate\Database\Seeder;

class IncursionSeeder extends Seeder
{
    protected $importIncursion;

    public function __construct(ImportIncursion $importIncursion)
    {
        $this->importIncursion = $importIncursion;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        print_r("[IncursionSeeder] executing import script..." . PHP_EOL . PHP_EOL);

        $this->importIncursion->execute(
            '/home/carlomigueldy/Documents/Runway Incursion/incursions.csv'
        );

        print_r("[IncursionSeeder] import done!" . PHP_EOL . PHP_EOL);
    }
}
