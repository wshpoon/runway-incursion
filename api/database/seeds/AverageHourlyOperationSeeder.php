<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AverageHourlyOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // average_hourly_operations table
        DB::select(
            DB::raw("CREATE TABLE average_hourly_operations (SELECT facility,
            hour,
            label,
            ( `departure_ac+at` / events )              AS commercial_departures,
            ( departure_general_aviation / events )         AS ga_departures,
            ( departure_military / events )                 AS military_departures,
            ( departure_total / events )                    AS total_departures,
            ( `arrival_ac+at` / events )                AS commercial_arrivals,
            ( arrival_general_aviation / events )           AS ga_arrivals,
            ( arrival_military / events )                   AS military_arrivals,
            ( arrival_total / events )                      AS total_arrivals,
            ( total_operations / events )                   AS average_operations,
            ( incursion_count / events )                    AS total_incursions,
            ( incursion_count / total_operations * 100000 ) AS incursion_rates,
            ( events )                                      AS total_events
    FROM   summarized_incursions_operations )")
        );

        // average_hourly_operations_1 table
        DB::select(
            DB::raw("CREATE TABLE average_hourly_operations_1 (SELECT hour, total_departures, total_arrivals, average_operations FROM
            average_hourly_operations);")
        );

        // average_hourly_operations_2 table
        DB::select(
            DB::raw("CREATE TABLE average_hourly_operations_2 (SELECT hour,
            commercial_departures,
            commercial_arrivals,
            ga_departures,
            ga_arrivals,
            military_departures,
            military_arrivals,
            average_operations FROM
            average_hourly_operations);")
        );
    }
}
