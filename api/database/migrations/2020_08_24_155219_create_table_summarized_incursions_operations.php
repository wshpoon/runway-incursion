<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSummarizedIncursionsOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summarized_incursions_operations', function (Blueprint $table) {
            $table->id();
            $table->string('facility')->nullable();
            $table->integer('hour')->nullable();
            $table->integer('unnamed_3')->nullable();
            $table->integer('departure_ac+at')->nullable();
            $table->integer('departure_general_aviation')->nullable();
            $table->integer('departure_military')->nullable();
            $table->integer('departure_total')->nullable();
            $table->integer('arrival_ac+at')->nullable();
            $table->integer('arrival_general_aviation')->nullable();
            $table->integer('arrival_military')->nullable();
            $table->integer('arrival_total')->nullable();
            $table->integer('total_operations')->nullable();
            $table->integer('incursion_count')->nullable();
            $table->integer('events')->nullable();
            $table->string('label')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_summarized_incursions_operations');
    }
}
