<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncursionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incursions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->string('incdnt_type_faa_code')->nullable();
            $table->boolean('operational')->nullable();
            $table->boolean('aircraft')->nullable();
            $table->boolean('other')->nullable();

            // renamed into date @ 2020_08_05_221620_rename_columns_event_lcl_date_and_event_arpt_id_in_incursions_table
            $table->timestamp('event_lcl_date')->nullable();

            $table->char('rwy_sfty_ri_cat_rnk_code')->nullable();
            $table->boolean('a')->nullable();
            $table->boolean('b')->nullable();
            $table->boolean('c')->nullable();
            $table->boolean('d')->nullable();
            $table->boolean('e')->nullable();
            $table->boolean('p')->nullable();
            $table->boolean('unk')->nullable();

            // renamed into facility @ 2020_08_05_221620_rename_columns_event_lcl_date_and_event_arpt_id_in_incursions_table
            $table->string('event_arpt_id')->nullable();

            $table->string('event_loc_desc')->nullable();
            $table->string('acft_1_rwy_sfty_type')->nullable();
            $table->string('acft_1_type')->nullable();
            $table->boolean('runway_incursion')->nullable();
            $table->string('acft_2_rwy_sfty_type')->nullable();
            $table->string('acft_1_fltcndt_code')->nullable();
            $table->string('acft_2_fltcndt_code')->nullable();
            $table->string('wx_cond_desc')->nullable();
            $table->string('event_tkof_lndg_desc')->nullable();
            $table->timestamp('metar_time')->nullable();
            $table->string('airport_location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incursions');
    }
}
