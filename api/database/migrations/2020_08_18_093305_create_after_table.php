<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('after_table', function (Blueprint $table) {
            $table->string('label', 32)->nullable();
            // $table->string('date', 10)->nullable();
            $table->date('date')->nullable();
            $table->string('facility')->nullable();
            $table->string('state')->nullable();
            $table->decimal('total_events', 32, 0)->nullable();
            $table->decimal('total_incursions', 42, 0)->nullable();
            $table->decimal('average_incursions', 24, 4)->nullable();
            $table->decimal('total_operations', 32, 0)->nullable();
            $table->decimal('average_operations', 14, 4)->nullable();
            $table->decimal('incursion_rate', 52, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('after_table');
    }
}
