<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBottledOxygenTypeToAirportInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('airport_information', function (Blueprint $table) {
            $table->string('bottled_oxygen_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('airport_information', function (Blueprint $table) {
            $table->dropColumn('bottled_oxygen_type');
        });
    }
}
