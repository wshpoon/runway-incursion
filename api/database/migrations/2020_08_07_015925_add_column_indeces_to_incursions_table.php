<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIndecesToIncursionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incursions', function (Blueprint $table) {
            $table->index([
                'runway_incursion',
                'facility',
                'date'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incursions', function (Blueprint $table) {
            $table->dropIndex([
                'runway_incursion',
                'facility',
                'date'
            ]);
        });
    }
}
