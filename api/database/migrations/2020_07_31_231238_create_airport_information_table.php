<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airport_information', function (Blueprint $table) {
            $table->id();
            $table->text('site_number')->nullable();
            $table->text('type')->nullable();
            $table->text('aspm_77')->nullable();
            $table->text('opsnet_45')->nullable();
            $table->text('core_30')->nullable();
            $table->text('label')->nullable();
            $table->text('location_id')->nullable();
            $table->date('effective_date')->nullable();
            $table->text('region')->nullable();
            $table->text('district_office')->nullable();
            $table->text('state')->nullable();
            $table->text('state_name')->nullable();
            $table->text('county')->nullable();
            $table->text('county_state')->nullable();

            // this column will be added in a different migration
            // $table->string('region_2')->nullable();

            $table->text('city')->nullable();
            $table->text('facility_name')->nullable();
            $table->text('ownership')->nullable();
            $table->text('use')->nullable();
            $table->text('owner')->nullable();
            $table->text('owner_address')->nullable();
            $table->text('owner_csz')->nullable();
            $table->text('owner_phone')->nullable();
            $table->text('manager')->nullable();
            $table->text('manager_address')->nullable();
            $table->text('manager_csz')->nullable();
            $table->text('manager_phone')->nullable();
            $table->text('apr_latitude')->nullable();
            $table->text('apr_latitude_s')->nullable();
            $table->text('apr_longitude')->nullable();
            $table->text('apr_longitude_s')->nullable();
            $table->float('apr_latitude_calc', 15, 2)->nullable();
            $table->float('apr_longitude_calc', 15, 2)->nullable();
            $table->char('apr_method', 1)->nullable();
            $table->integer('apr_elevation')->nullable();
            $table->char('apr_elevation_method', 1)->nullable();
            $table->text('magnetic_variation')->nullable();
            $table->year('magnetic_variation_year')->nullable();
            $table->integer('traffic_pattern_altitude')->nullable();
            $table->text('chart_name')->nullable();
            $table->integer('distance_from_cbd')->nullable();
            $table->text('direction_from_cbd')->nullable();
            $table->integer('land_area_covered_by_airport')->nullable();
            $table->text('boundary_artcc_id')->nullable();
            $table->text('boundary_artcc_name')->nullable();
            $table->text('responsible_artcc_id')->nullable();
            $table->text('responsible_artcc_computer_id')->nullable();
            $table->text('responsible_artcc_name')->nullable();
            $table->char('tie_in_fss', 1)->nullable();
            $table->text('tie_in_fss_id')->nullable();
            $table->text('tie_in_fss_name')->nullable();
            $table->text('airport_to_fss_phone_number')->nullable();
            $table->text('tie_in_fss_toll_free_number')->nullable();
            $table->text('alternate_fss_id')->nullable();
            $table->text('alternate_fss_name')->nullable();
            $table->text('alternate_fss_toll_free_number')->nullable();
            $table->text('notam_facility_id')->nullable();
            $table->text('notam_service')->nullable();
            $table->date('activation_date')->nullable();
            $table->char('airport_status_code', 1)->nullable();
            $table->date('certification_type_date')->nullable();
            $table->text('federal_agreements')->nullable();
            $table->text('airspace_determination')->nullable();
            $table->char('customs_airport_of_entry', 1)->nullable();
            $table->char('customs_landing_rights', 1)->nullable();
            $table->text('military_joint_use')->nullable();
            $table->text('military_landing_rights')->nullable();
            $table->integer('inspection_method')->nullable();
            $table->char('inspection_group', 1)->nullable();
            $table->text('last_inspection_date')->nullable();
            $table->text('last_owner_information_date')->nullable();
            $table->text('full_types')->nullable(); // should be fuel_types
            $table->text('airframe_repair')->nullable();
            $table->text('power_plant_repair')->nullable();
            $table->text('bulk_oxygen_type')->nullable();
            $table->text('lighting_schedule')->nullable();
            $table->text('beacon_schedule')->nullable();
            $table->char('atct', 1)->nullable();
            $table->text('unicom_frequencies')->nullable();
            $table->text('ctaf_frequency')->nullable();
            $table->char('segmented_circle', 1)->nullable();
            $table->text('beacon_color')->nullable();
            $table->char('non_commercial_landing_fee', 1)->nullable();
            $table->text('medical_use')->nullable();
            $table->integer('single_engine_ga')->nullable();
            $table->integer('multi_engine_ga')->nullable();
            $table->integer('jet_engine_ga')->nullable();
            $table->integer('helicopters_ga')->nullable();
            $table->integer('gliders_operational')->nullable();
            $table->integer('military_operational')->nullable();
            $table->integer('ultralights')->nullable();
            $table->integer('operations_commercial')->nullable();
            $table->integer('operations_commuter')->nullable();
            $table->integer('operations_air_taxi')->nullable();
            $table->integer('operations_ga_local')->nullable();
            $table->integer('operations_ga_itin')->nullable();
            $table->integer('operations_military')->nullable();
            $table->date('operations_date')->nullable();
            $table->text('airport_position_source')->nullable();
            $table->text('airport_position_source_date')->nullable();
            $table->text('airport_elevation_source')->nullable();
            $table->text('airport_elevation_source_date')->nullable();
            $table->text('contract_fuel_available')->nullable();
            $table->text('transient_storage')->nullable();
            $table->text('other_services')->nullable();
            $table->text('wind_inicator')->nullable();
            $table->text('icao_identifer')->nullable();
            $table->char('minimum_operational_network', 1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airport_information');
    }
}
