<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndecesToBeforeAndAfterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("before_table", function (Blueprint $table) {
            $table->index("facility");
            $table->index("incursion_rate");
        });

        Schema::table("after_table", function (Blueprint $table) {
            $table->index("facility");
            $table->index("incursion_rate");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("before_table", function (Blueprint $table) {
            $table->dropIndex("facility");
            $table->dropIndex("incursion_rate");
        });

        Schema::table("after_table", function (Blueprint $table) {
            $table->dropIndex("facility");
            $table->dropIndex("incursion_rate");
        });
    }
}
