<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsAprToArpInAirportInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('airport_information', function (Blueprint $table) {
            $table->renameColumn('apr_latitude', 'arp_latitude');
            $table->renameColumn('apr_latitude_s', 'arp_latitude_s');
            $table->renameColumn('apr_longitude', 'arp_longitude');
            $table->renameColumn('apr_longitude_s', 'arp_longitude_s');
            $table->renameColumn('apr_latitude_calc', 'arp_latitude_calc');
            $table->renameColumn('apr_longitude_calc', 'arp_longitude_calc');
            $table->renameColumn('apr_method', 'arp_method');
            $table->renameColumn('apr_elevation', 'arp_elevation');
            $table->renameColumn('apr_elevation_method', 'arp_elevation_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('airport_information', function (Blueprint $table) {
            $table->dropColumn([
                'arp_latitude',
                'arp_latitude_s',
                'arp_longitude',
                'arp_longitude_s',
                'arp_latitude_calc',
                'arp_longitude_calc',
                'arp_method',
                'arp_elevation',
                'arp_elevation_method',
            ]);
        });
    }
}
