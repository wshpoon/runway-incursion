<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBoundaryArtccComputerIdToAirportInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('airport_information', function (Blueprint $table) {
            $table->string('boundary_artcc_computer_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('airport_information', function (Blueprint $table) {
            $table->dropColumn('boundary_artcc_computer_id');
        });
    }
}
