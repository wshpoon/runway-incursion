<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->string('facility')->nullable();
            $table->string('state')->nullable();
            $table->string('ddso_service_area')->nullable();
            $table->string('class')->nullable();
            $table->string('region')->nullable();
            $table->integer('ifr_itinerant_air_carrier')->nullable();
            $table->integer('ifr_itinerant_air_taxi')->nullable();
            $table->integer('ifr_itinerant_general_aviation')->nullable();
            $table->integer('ifr_itinerant_military')->nullable();
            $table->integer('ifr_itinerant_total')->nullable();
            $table->integer('vfr_itinerant_air_carrier')->nullable();
            $table->integer('vfr_itinerant_air_taxi')->nullable();
            $table->integer('vfr_itinerant_general_aviation')->nullable();
            $table->integer('vfr_itinerant_military')->nullable();
            $table->integer('vfr_itinerant_total')->nullable();
            $table->integer('itinerant_air_carrier')->nullable();
            $table->integer('itinerant_air_taxi')->nullable();
            $table->integer('itinerant_general_aviation')->nullable();
            $table->integer('itinerant_military')->nullable();
            $table->integer('itinerant_total')->nullable();
            $table->integer('local_civil')->nullable();
            $table->integer('local_military')->nullable();
            $table->integer('local_total')->nullable();
            $table->integer('total_operations')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
