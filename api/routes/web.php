<?php

use App\Import\ImportAirportInformation;
use App\Import\ImportIncursion;
use App\Import\ImportOperation;
use Illuminate\Support\Facades\Route;
use Rap2hpoutre\FastExcel\FastExcel;

// use App\Imp

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('import-csv/incursions', function (
//     ImportIncursion $importIncursion
// ) {

//     return $importIncursion->execute(
//         '/home/carlomigueldy/Documents/Runway Incursion/incursions.csv'
//     );
// });

Route::get('import-csv/airport-information', function (
    ImportAirportInformation $importAirportInformation
) {

    return $importAirportInformation->execute(
        '/home/carlomigueldy/Documents/Runway Incursion/airport-information-ii.csv'
    );
});
