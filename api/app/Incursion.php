<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Incursion extends Model
{
    protected $guarded = [];

    /**
     * Sets the metar time attribute formatted to string.
     * 
     * @param Carbon $value
     * @return void
     */
    public function setMetarTimeAttribute($value)
    {
        if (gettype($value) != 'string') {
            $this->attributes['metar_time'] = $value->format('Y-m-d H:i:s');
        }
    }
}
