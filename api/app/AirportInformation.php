<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirportInformation extends Model
{
    protected $guarded = [];

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setEffectiveDateAttribute($value)
    {
        if (gettype($value) != 'string') {
            $this->attributes['effective_date'] = $value->format('Y-m-d');
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setActivationDateAttribute($value)
    {
        if (gettype($value) != 'string') {
            $this->attributes['activation_date'] = $value->format('Y-m-d');
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setCertificationTypeDateAttribute($value)
    {
        if (gettype($value) != 'string') {
            $this->attributes['certification_type_date'] = $value->format('Y-m-d');
        } else if (isEmpty($value)) {
            $this->attributes['certification_type_date'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setSingleEngineGaAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['single_engine_ga'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setMultiEngineGaAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['multi_engine_ga'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setJetEngineGaAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['jet_engine_ga'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setGlidersOperationalAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['gliders_operational'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setMilitaryOperationalAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['military_operational'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setArpElevationAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['arp_elevation'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setArpElevationMethodAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['arp_elevation_method'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setUltralightsAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['ultralights'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsCommercialAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['operations_commercial'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsCommuterAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['operations_commuter'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsAirTaxiAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['operations_air_taxi'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsGaLocalAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['operations_ga_local'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsGaItinAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['operations_ga_itin'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsMilitaryAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['operations_military'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setOperationsDateAttribute($value)
    {
        if (gettype($value) != 'string') {
            $this->attributes['operations_date'] = $value->format('Y-m-d');
        } else if (isEmpty($value)) {
            $this->attributes['operations_date'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setHelicoptersGaAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['helicopters_ga'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setMagneticVariationYearAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['magnetic_variation_year'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setInspectionMethodAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['inspection_method'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setDistanceFromCbdAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['distance_from_cbd'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setTrafficPatternAltitudeAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['traffic_pattern_altitude'] = null;
        }
    }

    /**
     * Sets the effective date attribute formatted to string.
     *
     * @param Carbon $value
     * @return void
     */
    public function setLandAreaCoveredByAirportAttribute($value)
    {
        if (isEmpty($value)) {
            $this->attributes['land_area_covered_by_airport'] = null;
        }
    }
}
