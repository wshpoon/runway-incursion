<?php

namespace App\Providers;

use App\Repository\AirportComparisonRepositoryInterface;
use App\Repository\BeforeAfterRepositoryInterface;
use App\Repository\Eloquent\AirportComparisonRepository;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\BeforeAfterRepository;
use App\Repository\Eloquent\HourlyAccuracyRepository;
use App\Repository\Eloquent\HourlyIncursionRepository;
use App\Repository\Eloquent\HourlyOperationRepository;
use App\Repository\Eloquent\IncursionRateRepository;
use App\Repository\Eloquent\IncursionTotalRepository;
use App\Repository\Eloquent\OneYearRollingOperationRepository;
use App\Repository\Eloquent\OperationDemographicRepository;
use App\Repository\Eloquent\RunwayAndGradeRepository;
use App\Repository\Eloquent\SevenDayRollingOperationRepository;
use App\Repository\Eloquent\StateIncursionRepository;
use App\Repository\Eloquent\TypeAndGradeRepository;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\HourlyAccuracyRepositoryInterface;
use App\Repository\HourlyIncursionRepositoryInterface;
use App\Repository\HourlyOperationRepositoryInterface;
use App\Repository\IncursionRateRepositoryInterface;
use App\Repository\IncursionTotalRepositoryInterface;
use App\Repository\OneYearRollingOperationRepositoryInterface;
use App\Repository\OperationDemographicRepositoryInterface;
use App\Repository\RunwayAndGradeRepositoryInterface;
use App\Repository\SevenDayRollingOperationRepositoryInterface;
use App\Repository\StateIncursionRepositoryInterface;
use App\Repository\TypeAndGradeRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(BeforeAfterRepositoryInterface::class, BeforeAfterRepository::class);
        $this->app->bind(SevenDayRollingOperationRepositoryInterface::class, SevenDayRollingOperationRepository::class);
        $this->app->bind(OneYearRollingOperationRepositoryInterface::class, OneYearRollingOperationRepository::class);
        $this->app->bind(TypeAndGradeRepositoryInterface::class, TypeAndGradeRepository::class);
        $this->app->bind(RunwayAndGradeRepositoryInterface::class, RunwayAndGradeRepository::class);
        $this->app->bind(OperationDemographicRepositoryInterface::class, OperationDemographicRepository::class);
        $this->app->bind(HourlyAccuracyRepositoryInterface::class, HourlyAccuracyRepository::class);
        $this->app->bind(HourlyIncursionRepositoryInterface::class, HourlyIncursionRepository::class);
        $this->app->bind(HourlyOperationRepositoryInterface::class, HourlyOperationRepository::class);
        $this->app->bind(AirportComparisonRepositoryInterface::class, AirportComparisonRepository::class);
        $this->app->bind(IncursionTotalRepositoryInterface::class, IncursionTotalRepository::class);
        $this->app->bind(StateIncursionRepositoryInterface::class, StateIncursionRepository::class);
        $this->app->bind(IncursionRateRepositoryInterface::class, IncursionRateRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
