<?php

namespace App\Repository;

use Illuminate\Support\Collection;

interface IncursionRateRepositoryInterface
{
    /**
     * Get query to retrieve all data.
     *
     * @return Collection
     */
    public function get(): Collection;
}
