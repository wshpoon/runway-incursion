<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

interface BeforeAfterRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get();

    // /**
    //  * Query the before table.
    //  *
    //  * @return Collection
    //  */
    // public function queryBefore(): Collection;

    // /**
    //  * Query the after table.
    //  *
    //  * @return Collection
    //  */
    // public function queryAfter(): Collection;

    // /**
    //  * Get query results from before table.
    //  *
    //  * @return Collection
    //  */
    // public function getBeforeTable(): Collection;

    // /**
    //  * Get query results from after table.
    //  *
    //  * @return Collection
    //  */
    // public function getAfterTable(): Collection;

    // /**
    //  * Get merged results from before and after table.
    //  *
    //  * @return Collection
    //  */
    // public function getMergedBeforeAndAfterTable(): Collection;

    // /**
    //  * @return Collection
    //  */
    // public function getLookup(): Collection;

    // /**
    //  * Create before table by using the optimization query.
    //  *
    //  * @return Collection
    //  */
    // public function createBeforeTable(): Collection;

    // /**
    //  * Create after table by using the optimization query.
    //  *
    //  * @return Collection
    //  */
    // public function createAfterTable(): Collection;

    // /**
    //  * Get the optimized query.
    //  *
    //  * @param string $type AFTER|BEFORE
    //  * @param string $from date from
    //  * @param string $to date to
    //  * @return string query
    //  */
    // public function getBeforeAfterQuery(
    //     string $type,
    //     string $from = '2001-01-01',
    //     string $to = '2001-01-31'
    // ): string;
}
