<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

interface HourlyOperationRepositoryInterface
{
    /**
     * Get query to retrieve all data.
     *
     * @return array
     */
    public function get(): array;
}
