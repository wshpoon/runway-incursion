<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

interface HourlyAccuracyRepositoryInterface
{
    /**
     * Get query to retrieve all data.
     *
     * @return Collection
     */
    public function get(): Collection;
}
