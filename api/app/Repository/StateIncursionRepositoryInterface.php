<?php

namespace App\Repository;

use Illuminate\Support\Collection;

interface StateIncursionRepositoryInterface
{
    public function get(): Collection;

    public function getAlternative(): Collection;
}
