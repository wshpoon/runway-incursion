<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

interface RunwayAndGradeRepositoryInterface
{
    /**
     * Get runway and grade chart data via query.
     *
     * @return Collection
     */
    public function get(): Collection;
}
