<?php

namespace App\Repository\Eloquent;

use App\Repository\IncursionRateRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class IncursionRateRepository implements IncursionRateRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/incursion_totals/_search?size=15000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = DB::table("before_after")
            ->selectRaw("
                date,
                YEAR(date) AS year,
                MONTH(date) AS month,
                DATE_FORMAT(date, '%m-%d') AS day,
                label,
                ROUND(incursion_rate) AS incursion_rate
            ")
            ->where("label", "CORE 30")
            ->where("incursion_rate", "<=", 50000)
            ->whereNotNull("date")
            ->whereNotNull("incursion_rate")
            ->groupByRaw("date, label, incursion_rate")
            ->limit(365)
            ->get();

        return $data;
    }
}
