<?php

namespace App\Repository\Eloquent;

use App\Repository\HourlyOperationRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class HourlyOperationRepository implements HourlyOperationRepositoryInterface
{
    /**
     * @return array
     */
    public function get(): array
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return array
     */
    private function elasticSearchQuery(): array
    {
        // average_hourly_operations_1 table
        $queryAlpha = Http::get('http://localhost:9200/hourly_operations1/_search?size=15000');
        $dataAlpha = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $queryAlpha->json()['hits']['hits'])
        );

        // average_hourly_operations_2 table
        $queryBravo = Http::get('http://localhost:9200/hourly_operations2/_search?size=15000');
        $dataBravo = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $queryBravo->json()['hits']['hits'])
        );


        // info("[HourlyOperationRepository] elasticsearch query", [
        //     'average_hourly_operations_1' => $dataAlpha,
        //     'average_hourly_operations_2' => $dataBravo,
        // ]);

        return [
            'average_hourly_operations_1' => $dataAlpha,
            'average_hourly_operations_2' => $dataBravo,
        ];
    }

    /**
     * Get data from local DB query.
     *
     * @return array
     */
    private function localDbQuery(): array
    {
        $dataAlpha = DB::table('average_hourly_operations_1')->get();
        $dataBravo = DB::table('average_hourly_operations_2')->get();

        // info("[HourlyOperationRepository] local DB query", [
        //     'average_hourly_operations_1' => $dataAlpha,
        //     'average_hourly_operations_2' => $dataBravo,
        // ]);

        return [
            'average_hourly_operations_1' => $dataAlpha,
            'average_hourly_operations_2' => $dataBravo,
        ];
    }
}
