<?php

namespace App\Repository\Eloquent;

use App\Repository\TypeAndGradeRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class TypeAndGradeRepository implements TypeAndGradeRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/typegrade/_search?size=90000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[TypeAndGradeRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = collect(
            DB::select(
                DB::raw("SELECT airport_information.facility,
                        airport_information.label,
                        incursions.date,
                        incursions.incdnt_type_faa_code,
                        Sum(incursions.total_incursions) AS total_incursions,
                        incursions.rwy_sfty_ri_cat_rnk_code,
                        incursions.acft_1_type
                FROM   (SELECT location_id AS facility,
                                label
                        FROM   airport_information) airport_information
                        LEFT JOIN (SELECT facility,
                                        incdnt_type_faa_code,
                                        acft_1_type,
                                        IF(rwy_sfty_ri_cat_rnk_code = '', 'NA', rwy_sfty_ri_cat_rnk_code) AS rwy_sfty_ri_cat_rnk_code,
                                        date,
                                        Sum(runway_incursion) AS total_incursions
                                FROM   incursions
                                GROUP  BY facility,
                                            incdnt_type_faa_code,
                                            rwy_sfty_ri_cat_rnk_code,
                                            acft_1_type,
                                            runway_incursion,
                                            date) incursions
                            ON airport_information.facility = incursions.facility
                GROUP BY
                airport_information.facility,
                        airport_information.label,
                        incursions.date,
                        incursions.incdnt_type_faa_code,
                        incursions.rwy_sfty_ri_cat_rnk_code,
                        incursions.acft_1_type ; ")
            )
        );

        // info("[TypeAndGradeRepository] local DB query", $data->toArray());

        return $data;
    }
}
