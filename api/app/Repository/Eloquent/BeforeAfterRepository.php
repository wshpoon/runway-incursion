<?php

namespace App\Repository\Eloquent;

use App\Repository\BeforeAfterRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class BeforeAfterRepository implements BeforeAfterRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        // return $this->localDbMappedQuery();

        // return $this->elasticSearchQuery();

        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbMappedQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/before-after/_search?size=10000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        )->map(function ($item) {
            $item = (array) $item;

            $item['series'] = json_decode($item['series']);

            return (object) $item;
        });

        // info("[BeforeAfterRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $query = "SELECT
            b.label,
            b.date,
            a.date,
            b.incursion_rate AS x,
            a.incursion_rate AS y,
            (a.average_operations + b.average_operations) AS average_operations
        FROM
            (SELECT
                label, date, incursion_rate, average_operations
            FROM
                before_table
            WHERE date < '2012-01-01'
            ORDER BY date DESC) AS b,
            (SELECT
                label, date, incursion_rate, average_operations
            FROM
                after_table
            WHERE date > '2012-01-01'
            ORDER BY date ASC) AS a
        WHERE
            b.label = a.label
                AND DATE_FORMAT(b.date, '%m-%d') = DATE_FORMAT(a.date, '%m-%d')
        LIMIT 100000;";

        return collect(
            DB::select(DB::raw($query))
        );
    }

    private function localDbMappedQuery(): Collection
    {
        $query = "SELECT
                label,
                CONCAT('{\"marker\": { \"radius\": 1.5 }, \"tooltip\": { \"followPointer\": false,  \"pointFormat\": \"[{point.x:.1f}, {point.y:.1f}]\" },
                    \"type\": \"scatter\",\"name\": \"',
                        label,
                        '\", \"data\": ',
                        CONCAT('[',
                                GROUP_CONCAT(DISTINCT CONCAT('[', x, ',', y, ']')),
                                ']'),
                        '}') AS series
            FROM
                (SELECT
                    b.label,
                        b.date AS before_date,
                        a.date AS after_date,
                        ROUND(b.incursion_rate) AS x,
                        ROUND(a.incursion_rate) AS y,
                        (a.average_operations + b.average_operations) AS average_operations
                FROM
                    (SELECT
                    label, date, incursion_rate, average_operations
                FROM
                    before_table
                WHERE
                    date < '2012-01-01'
                ORDER BY date DESC) AS b, (SELECT
                    label, date, incursion_rate, average_operations
                FROM
                    after_table
                WHERE
                    date > '2012-01-01'
                ORDER BY date ASC) AS a
                WHERE
                    b.label = a.label
                        AND DATE_FORMAT(b.date, '%m-%d') = DATE_FORMAT(a.date, '%m-%d')
                LIMIT 90000) AS before_after
            GROUP BY label;";

        return collect(
            DB::select(DB::raw($query))
        )->map(function ($item) {
            $item = (array) $item;

            $item['series'] = json_decode($item['series']);

            return (object) $item;
        });
    }

    private function localDbDygraphsQuery()
    {
        $query = "SELECT
                CONCAT('[\"X\", ', GROUP_CONCAT(DISTINCT CONCAT('\"', label, '\"')), ']') AS labels,
                CONCAT('[', GROUP_CONCAT(DISTINCT CONCAT('[', x, ',', y, ']') ) ) AS data
            FROM
                (SELECT
                    b.label,
                        b.date AS before_date,
                        a.date AS after_date,
                        b.incursion_rate AS x,
                        a.incursion_rate AS y,
                        (a.average_operations + b.average_operations) AS average_operations
                FROM
                    (SELECT
                    label, date, incursion_rate, average_operations
                FROM
                    before_table
                WHERE
                    date < '2012-01-01'
                ORDER BY date DESC) AS b, (SELECT
                    label, date, incursion_rate, average_operations
                FROM
                    after_table
                WHERE
                    date > '2012-01-01'
                ORDER BY date ASC) AS a
                WHERE
                    b.label = a.label
                        AND DATE_FORMAT(b.date, '%m-%d') = DATE_FORMAT(a.date, '%m-%d')
                LIMIT 90000) AS before_after
            ;";

        return collect(
            DB::select(DB::raw($query))
        )->map(function ($item) {
            $item = (array) $item;

            $item['data'] = json_decode($item['data']);
            $item['labels'] = json_decode($item['labels']);

            return (object) $item;
        });
    }

    // private $beforeTable = "before_table";
    // private $afterTable = "after_table";

    // /**
    //  * @return Collection
    //  */
    // public function queryBefore(): Collection
    // {
    //     return collect(DB::select(
    //         DB::raw($this->getBeforeAfterQuery("BEFORE"))
    //     ));
    // }

    // /**
    //  * @return Collection
    //  */
    // public function queryAfter(): Collection
    // {
    //     return collect(DB::select(
    //         DB::raw($this->getBeforeAfterQuery("AFTER"))
    //     ));
    // }

    // /**
    //  * @return Collection
    //  */
    // public function getBeforeTable(): Collection
    // {
    //     return DB::table($this->beforeTable)->whereNotNull('date')->orderBy('date')->get();
    // }

    // /**
    //  * @return Collection
    //  */
    // public function getAfterTable(): Collection
    // {
    //     return DB::table($this->afterTable)->whereNotNull('date')->orderBy('date')->get();
    // }

    // /**
    //  * @return Collection
    //  */
    // public function getMergedBeforeAndAfterTable(): Collection
    // {
    //     $query = "SELECT
    //         date,
    //         facility,
    //         state,
    //         label,
    //         DATE_FORMAT(date, '%Y') AS year,
    //         DATE_FORMAT(date, '%m-%d') AS month_day,
    //         AVG(merged.total_operations) AS average_operations_per_day,
    //         SUM(incursion_rate) AS incursion_rate_per_day
    //     FROM
    //         (SELECT
    //             *
    //         FROM
    //             before_table UNION ALL SELECT
    //             *
    //         FROM
    //             after_table) merged
    //     GROUP BY merged.date , merged.facility , merged.state , merged.label;
    //     ";

    //     return collect(
    //         DB::select(
    //             DB::raw(
    //                 "SELECT
    //                     *
    //                 FROM
    //                     (SELECT
    //                         *
    //                     FROM
    //                         before_table UNION ALL SELECT
    //                         *
    //                     FROM
    //                         after_table) merged;"
    //             )
    //         )
    //     );
    // }

    // public function getLookup(): Collection
    // {
    //     return collect(
    //         DB::select(
    //             DB::raw(
    //                 "SELECT
    //                     facility
    //                 FROM
    //                     (SELECT
    //                         facility
    //                     FROM
    //                         before_table
    //                     UNION ALL
    //                     SELECT
    //                         facility
    //                     FROM
    //                         after_table
    //                     )
    //                 merged
    //                 GROUP BY facility;"
    //             )
    //         )
    //     );
    // }

    // /**
    //  * @return Collection
    //  */
    // public function createBeforeTable(): Collection
    // {
    //     return collect(DB::select(
    //         DB::raw(
    //             "CREATE TABLE " . $this->beforeTable . " " . $this->getBeforeAfterQuery("BEFORE")
    //         )
    //     ));
    // }

    // /**
    //  * @return Collection
    //  */
    // public function createAfterTable(): Collection
    // {
    //     return collect(DB::select(
    //         DB::raw(
    //             "CREATE TABLE " . $this->afterTable . " " . $this->getBeforeAfterQuery("AFTER")
    //         )
    //     ));
    // }

    // /**
    //  * @param string $type BEFORE | AFTER
    //  * @return string
    //  */
    // public function getBeforeAfterQuery(
    //     string $type,
    //     string $from = '2001-01-01',
    //     string $to = '2001-01-31'
    // ): string {
    //     $limit = true ? "" : "LIMIT 10000";

    //     if ($type == "BEFORE") {
    //         $query =
    //         "SELECT
    //             airport_information.label AS label,
    //             operations_incursions.date AS date,
    //             operations_incursions.facility AS facility,
    //             operations_incursions.state AS state,
    //             SUM(operations_incursions.events) AS total_events,
    //             SUM(operations_incursions.total_incursions) AS total_incursions,
    //             AVG(operations_incursions.total_incursions) AS average_incursions,
    //             SUM(operations_incursions.total_operations) AS total_operations,
    //             AVG(operations_incursions.total_operations) AS average_operations,
    //             ((SUM(IFNULL(operations_incursions.total_incursions, 0)) / SUM(NULLIF(operations_incursions.total_operations, 0))) * 100000) AS incursion_rate
    //         FROM
    //             airport_information
    //                 LEFT JOIN
    //             (SELECT
    //                 DATE_FORMAT(operations.date, '%Y-%m-%d') AS date,
    //                     operations.facility,
    //                     operations.state,
    //                     (1) AS events,
    //                     operations.total_operations,
    //                     COUNT(incursions.id) AS total_incursions
    //             FROM
    //                 incursions
    //             LEFT JOIN (SELECT
    //                 DATE_FORMAT(operations.date, '%Y-%m-%d') AS date,
    //                     operations.facility,
    //                     operations.state,
    //                     operations.total_operations
    //             FROM
    //                 operations
    //             WHERE
    //                 operations.date >= '".$from."'
    //                     AND operations.date <= '".$to."'
    //                     AND (operations.facility , operations.date) NOT IN (SELECT
    //                         DATE_FORMAT(date, '%Y-%m-%d') AS date, facility
    //                     FROM
    //                         incursions)
    //             GROUP BY operations.date , operations.facility , operations.state , operations.total_operations
    //             " . $limit . ") operations ON incursions.facility = operations.facility
    //             GROUP BY operations.date , operations.facility , operations.state , operations.total_operations) operations_incursions ON airport_information.district_office = operations_incursions.facility
    //         GROUP BY airport_information.label , operations_incursions.date , operations_incursions.facility , operations_incursions.state , operations_incursions.total_operations , operations_incursions.total_incursions
    //         " . $limit . ";";
    //     } else {
    //         $from = $from ?? '2012-01-01';
    //         $to = $to ?? '2020-12-31';

    //         $query =
    //         "SELECT
    //             airport_information.label AS label,
    //             operations_incursions.date AS date,
    //             operations_incursions.facility AS facility,
    //             operations_incursions.state AS state,
    //             SUM(operations_incursions.events) AS total_events,
    //             SUM(operations_incursions.total_incursions) AS total_incursions,
    //             AVG(operations_incursions.total_incursions) AS average_incursions,
    //             SUM(operations_incursions.total_operations) AS total_operations,
    //             AVG(operations_incursions.total_operations) AS average_operations,
    //             ((SUM(IFNULL(operations_incursions.total_incursions, 0)) / SUM(NULLIF(operations_incursions.total_operations, 0))) * 100000) AS incursion_rate
    //         FROM
    //             airport_information
    //                 LEFT JOIN
    //             (SELECT
    //                 DATE_FORMAT(operations.date, '%Y-%m-%d') AS date,
    //                     operations.facility,
    //                     operations.state,
    //                     (1) AS events,
    //                     operations.total_operations,
    //                     COUNT(incursions.id) AS total_incursions
    //             FROM
    //                 incursions
    //             LEFT JOIN (SELECT
    //                 DATE_FORMAT(operations.date, '%Y-%m-%d') AS date,
    //                     operations.facility,
    //                     operations.state,
    //                     operations.total_operations
    //             FROM
    //                 operations
    //             WHERE
    //                 operations.date >= '".$from."'
    //                     AND operations.date <= '".$to."'
    //                     AND (operations.facility , operations.date) NOT IN (SELECT
    //                         DATE_FORMAT(date, '%Y-%m-%d') AS date, facility
    //                     FROM
    //                         incursions)
    //             GROUP BY operations.date , operations.facility , operations.state , operations.total_operations
    //             " . $limit . ") operations ON incursions.facility = operations.facility
    //             GROUP BY operations.date , operations.facility , operations.state , operations.total_operations) operations_incursions ON airport_information.district_office = operations_incursions.facility
    //         GROUP BY airport_information.label , operations_incursions.date , operations_incursions.facility , operations_incursions.state , operations_incursions.total_operations , operations_incursions.total_incursions
    //         " . $limit . ";";
    //     }

    //     info($query);

    //     return $query;
    // }
}
