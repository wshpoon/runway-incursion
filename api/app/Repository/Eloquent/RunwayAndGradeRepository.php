<?php

namespace App\Repository\Eloquent;

use App\Repository\RunwayAndGradeRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class RunwayAndGradeRepository implements RunwayAndGradeRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/runway_grade/_search?size=25000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[RunwayAndGradeRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = DB::table('incursions')->selectRaw(
            "facility,
            event_tkof_lndg_desc,
            acft_1_type,
            IF(rwy_sfty_ri_cat_rnk_code = '', 'NA', rwy_sfty_ri_cat_rnk_code) AS rwy_sfty_ri_cat_rnk_code,
            SUM(runway_incursion) AS total_incursions,
            COUNT(id) AS total_incursions_b"
        )->groupByRaw(
            "facility,
            acft_1_type,
            rwy_sfty_ri_cat_rnk_code,
            event_tkof_lndg_desc"
        )->get();

        // info("[RunwayAndGradeRepository] local DB query", $data->toArray());

        return $data;
    }
}
