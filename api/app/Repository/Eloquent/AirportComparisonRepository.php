<?php

namespace App\Repository\Eloquent;

use App\Repository\AirportComparisonRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class AirportComparisonRepository implements AirportComparisonRepositoryInterface
{
    /**
     * Get chart data for Airport Comparison
     *
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/airport_comparison/_search?size=75000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[AirportComparison] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = DB::table("incursions_operations")
            ->selectRaw("facility,
            label,
            SUM(IFNULL(total_operations, 0)) AS total_operations,
            SUM(IFNULL(total_incursions, 0)) AS total_incursions,
            SUM(IFNULL(incursion_rate, 0)) AS total_incursion_rate
        ")->groupBy([
                "facility",
                "label",
                "incursion_rate"
            ])->get();

        // info("[AirportComparison] local DB query", $data->toArray());

        return $data;
    }
}
