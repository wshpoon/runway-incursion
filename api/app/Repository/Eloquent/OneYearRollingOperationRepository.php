<?php

namespace App\Repository\Eloquent;

use App\Repository\OneYearRollingOperationRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class OneYearRollingOperationRepository implements OneYearRollingOperationRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/oneyear_rolling/_search?size=100000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[OneYearRollingOperationRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data =  DB::table('incursions_operations')->get();

        // info("[OneYearRollingOperationRepository] local DB query", $data->toArray());

        return $data;
    }
}
