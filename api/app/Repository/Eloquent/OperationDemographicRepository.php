<?php

namespace App\Repository\Eloquent;

use App\Repository\OperationDemographicRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class OperationDemographicRepository implements OperationDemographicRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * @return array
     */
    public function search(string $facility, Carbon $minDate, Carbon $maxDate)
    {
        return DB::table("operations")->selectRaw(
            "facility, IFNULL((SUM(itinerant_air_carrier) + SUM(itinerant_air_taxi)), 0) AS commercial,
                IFNULL((SUM(itinerant_general_aviation) + SUM(local_civil)), 0) AS general_aviation,
                IFNULL((SUM(local_military) + SUM(itinerant_military)), 0) AS military"
        )
            ->where('facility', $facility)
            ->whereBetween('date', [
                $minDate->format('Y-m-d'),
                $maxDate->format('Y-m-d'),
            ])
            ->groupBy("facility")
            ->first();
    }

    /**
     * @return Collection
     */
    public function getFacilities(): Collection
    {
        return DB::table('operations')->distinct('facility')->get('facility')->pluck('facility');
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/operation_demographic/_search?size=1000000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[OperationDemographicRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = DB::table('operations')->selectRaw(
            "facility, IFNULL((SUM(itinerant_air_carrier) + SUM(itinerant_air_taxi)), 0) AS commercial,
            IFNULL((SUM(itinerant_general_aviation) + SUM(local_civil)), 0) AS general_aviation,
            IFNULL((SUM(local_military) + SUM(itinerant_military)), 0) AS military"
        )->groupBy("facility")->first();

        // info("[OperationDemographicRepository] local DB query", $data->toArray());

        return $data;
    }
}
