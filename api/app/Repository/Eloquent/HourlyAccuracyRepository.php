<?php

namespace App\Repository\Eloquent;

use App\Repository\HourlyAccuracyRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class HourlyAccuracyRepository implements HourlyAccuracyRepositoryInterface
{
    /**
     * Get chart data for Hourly Accuracy.
     *
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/hourly_incursion/_search?size=15000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[HourlyAccuracyRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = DB::table("average_hourly_operations")->get();

        // info("[HourlyAccuracyRepository] local DB query", $data->toArray());

        return $data;
    }
}
