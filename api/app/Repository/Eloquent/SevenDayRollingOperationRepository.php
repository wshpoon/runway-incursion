<?php

namespace App\Repository\Eloquent;

use App\Repository\SevenDayRollingOperationRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class SevenDayRollingOperationRepository implements SevenDayRollingOperationRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/seven_day_rolling/_search?size=10000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[SevenDayRollingOperationRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = collect(
            DB::select(
                DB::raw(
                    "SELECT
                airport_information_operations.date,
                year,
                day,
                SUM(airport_information_operations.total_operations) AS total_operations,
                AVG(airport_information_operations.total_operations) AS average_operations
                FROM (SELECT
                    airport_information.label,
                    operations.date,
                    DATE_FORMAT(operations.date, '%Y') AS year,
                    DATE_FORMAT(operations.date, '%m-%d') AS day,
                    SUM(operations.total_operations) AS total_operations
                FROM
                    airport_information
                        LEFT JOIN
                    operations ON airport_information.location_id = operations.facility
                WHERE
                    operations.date >= '2018-01-01'
                        AND operations.date <= '2020-12-31'
                GROUP BY operations.date , airport_information.label , operations.total_operations
                ) airport_information_operations
                GROUP BY airport_information_operations.date, airport_information_operations.day
                ORDER BY airport_information_operations.date;
                "
                )
            )
        );

        // info("[SevenDayRollingOperationRepository] local DB query", $data->toArray());

        return $data;
    }
}
