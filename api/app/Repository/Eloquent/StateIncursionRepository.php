<?php

namespace App\Repository\Eloquent;

use App\Repository\StateIncursionRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class StateIncursionRepository implements StateIncursionRepositoryInterface
{
    public function get(): Collection
    {
        /**
         * table: incursion_per_state
         *
         * SELECT ai.state, COUNT(i.runway_incursion) AS total_incursions,
         * NULL AS latitude, NULL AS longitude
         * FROM airport_information AS ai
         * INNER JOIN incursions AS i
         * ON ai.location_id = i.facility
         * GROUP BY ai.state, i.runway_incursion
         * ORDER BY ai.state
         *
         * 1 line
         * SELECT ai.state, COUNT(i.runway_incursion) AS total_incursions, NULL AS latitude, NULL AS longitude FROM airport_information AS ai INNER JOIN incursions AS i ON ai.location_id = i.facility GROUP BY ai.state, i.runway_incursion ORDER BY ai.state
         */

        // return DB::table("incursions_per_state")->get();

        $query = " SELECT
            NULL AS CO_LOC_REF_I,
            ai.state AS MAIL_ST_PROV_C,
            CONCAT(UPPER(SUBSTRING(ai.city,1,1)),LOWER(SUBSTRING(ai.city,2))) AS mail_city_n,
            i.event_tkof_lndg_desc AS co_loc_n,
            ai.arp_latitude_calc AS LATTD_I,
            ai.arp_longitude_calc AS LNGTD_I,
            COUNT(i.runway_incursion) AS count
        FROM
            airport_information AS ai
                INNER JOIN
            incursions AS i ON ai.location_id = i.facility
        GROUP BY ai.state , ai.city, ai.arp_latitude_calc, ai.arp_longitude_calc, i.runway_incursion, i.event_tkof_lndg_desc
        ORDER BY ai.state;";

        return collect(
            DB::select(DB::raw($query))
        );
    }

    public function getAlternative(): Collection
    {
        $query = " SELECT
            ai.state AS state,
            ai.city AS city,
            i.event_tkof_lndg_desc AS runway,
            ai.arp_latitude_calc AS latitude,
            ai.arp_longitude_calc AS longitude,
            COUNT(i.runway_incursion) AS total_incursions
        FROM
            airport_information AS ai
                INNER JOIN
            incursions AS i ON ai.location_id = i.facility
        GROUP BY ai.state , ai.city, ai.arp_latitude_calc, ai.arp_longitude_calc, i.runway_incursion, i.event_tkof_lndg_desc
        ORDER BY ai.state;";

        return collect(
            DB::select(DB::raw($query))
        );
    }
}
