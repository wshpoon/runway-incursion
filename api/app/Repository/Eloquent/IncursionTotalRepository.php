<?php

namespace App\Repository\Eloquent;

use App\Repository\IncursionTotalRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class IncursionTotalRepository implements IncursionTotalRepositoryInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if (env('APP_ELASTICSEARCH') == "enabled") {
            return $this->elasticSearchQuery();
        }

        return $this->localDbQuery();
    }

    /**
     * Get data from elasticsearch query.
     *
     * @return Collection
     */
    private function elasticSearchQuery(): Collection
    {
        $query = Http::get('http://localhost:9200/incursion_totals/_search?size=15000');

        $data = collect(
            array_map(function ($data) {
                return $data['_source'];
            }, $query->json()['hits']['hits'])
        );

        // info("[IncursionTotalRepository] elasticsearch query", $data->toArray());

        return $data;
    }

    /**
     * Get data from local DB query.
     *
     * @return Collection
     */
    private function localDbQuery(): Collection
    {
        $data = DB::table("incursions")
            ->selectRaw("acft_1_type,
                facility,
                IF(acft_1_rwy_sfty_type = '', 'NA', acft_1_rwy_sfty_type) AS acft_1_rwy_sfty_type,
                SUM(runway_incursion) AS total_incursions
            ")->groupBy([
                "acft_1_type",
                "acft_1_rwy_sfty_type",
                "facility"
            ])
            ->orderByDesc("total_incursions")
            ->get();

        // info("[IncursionTotalRepository] local DB query", $data->toArray());

        return $data;
    }
}
