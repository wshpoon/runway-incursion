<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Support\Collection;

interface OperationDemographicRepositoryInterface
{
    /**
     * Get runway and grade chart data without date field.
     *
     * @return array
     */
    public function get();

    /**
     * Get runway and grade chart data via query.
     *
     * @return array
     */
    public function search(
        string $facility,
        Carbon $minDate,
        Carbon $maxDate
    );

    /**
     * @return Collection
     */
    public function getFacilities(): Collection;
}
