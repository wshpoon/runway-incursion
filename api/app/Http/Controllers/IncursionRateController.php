<?php

namespace App\Http\Controllers;

use App\Repository\IncursionRateRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IncursionRateController extends Controller
{
    protected $repository;

    /**
     * @return IncursionRateRepositoryInterface $repository
     */
    public function __construct(IncursionRateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        $labels = $data->countBy('day')->keys()->all();

        $dates = $data->countBy('date')->keys()->all();

        return response()->json([
            // 'data' => $data,
            'labels' => $labels,
            'years' => $data->groupBy('year'),
            'dates' => $dates,
        ]);
    }
}
