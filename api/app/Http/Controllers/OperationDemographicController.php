<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\OperationDemographicRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OperationDemographicController extends Controller
{
    /**
     * @var OperationDemographicRepositoryInterface
     */
    protected $repository;

    /**
     * @param OperationDemographicRepositoryInterface $repository
     */
    public function __construct(
        OperationDemographicRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * Get operation demographic  data.
     *
     * @param string $facility
     * @param string $minDate
     * @param string $maxDate
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'data' => $this->repository->search(
                "ABI",
                now()->year(2001)->startOfYear(),
                now()->endOfYear()
            ),
            'airports' => $this->repository->getFacilities()
        ]);
    }

    /**
     * Get operation demographic  data.
     *
     * @param string $facility
     * @param string $minDate
     * @param string $maxDate
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(
        string $facility,
        string $minDate,
        string $maxDate
    ): JsonResponse {

        return response()->json([
            'data' => $this->repository->search(
                $facility,
                Carbon::parse($minDate),
                Carbon::parse($maxDate)
            ),
            'airports' => $this->repository->getFacilities()
        ]);
    }
}
