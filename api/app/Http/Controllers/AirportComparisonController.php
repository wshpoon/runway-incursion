<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\AirportComparisonRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AirportComparisonController extends Controller
{
    protected $repository;

    /**
     * @return AirportComparisonRepositoryInterface $repository
     */
    public function __construct(AirportComparisonRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        $incursionRates = $data->sortByDesc("total_incursion_rate")
            ->values()->groupBy("facility");

        $operationTotals = $data->sortByDesc("total_operation")
            ->values()->groupBy("facility");

        return response()->json([
            'incursion_rates' => $incursionRates,
            'operation_totals' => $operationTotals,
            'facilities' => $data->countBy('facility')->sortKeys()->keys()
        ]);
    }
}
