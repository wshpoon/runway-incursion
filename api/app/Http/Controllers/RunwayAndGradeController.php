<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\RunwayAndGradeRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class RunwayAndGradeController extends Controller
{
    /**
     * @var RunwayAndGradeRepositoryInterface
     */
    protected $repository;

    /**
     * @param RunwayAndGradeRepositoryInterface $repository
     */
    public function __construct(
        RunwayAndGradeRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        $labels = $data->groupBy('rwy_sfty_ri_cat_rnk_code')->keys();

        return response()->json([
            'codes' => $data->groupBy('rwy_sfty_ri_cat_rnk_code')->sortKeys(),
            'official_runways' => $data->groupBy('event_tkof_lndg_desc')->sortKeys()->keys(),
            'airports' => $data->countBy('facility')->sortKeys()->keys(),
            'labels' => $labels
        ]);
    }
}
