<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\BeforeAfterRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class BeforeAfterController extends Controller
{
    protected $repository;

    /**
     * @param BeforeAfterRepositoryInterface $repository
     */
    public function __construct(
        BeforeAfterRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        return response()->json([
            'series' => $data->map(function ($item) {
                return $item->series;
            }),
            'data' => [],
            'xaxis' => [],
            'labels' => $data->countBy("label")->sortKeysDesc()->keys(),

            // 'data' => $data->groupBy("label"),
            // 'xaxis' => $data->filter(function ($item) {
            //     $item = (object) $item;

            //     return isset($item->x);
            // })->values()->countBy('x')->keys(),
        ]);
    }
}
