<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\HourlyOperationRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HourlyOperationController extends Controller
{
    protected $repository;

    /**
     * @return HourlyOperationRepositoryInterface $repository
     */
    public function __construct(HourlyOperationRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        return response()->json([
            'data' => [
                'average_hourly_operations_1' => $data['average_hourly_operations_1']->groupBy("hour"),
                'average_hourly_operations_2' => $data['average_hourly_operations_2']->groupBy("hour"),
            ],
            'hours' => range(0, 23)
        ]);
    }
}
