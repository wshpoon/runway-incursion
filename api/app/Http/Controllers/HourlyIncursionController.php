<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\HourlyIncursionRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HourlyIncursionController extends Controller
{
    protected $repository;

    /**
     * @return HourlyIncursionRepositoryInterface $repository
     */
    public function __construct(HourlyIncursionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        return response()->json([
            'data' => $data->groupBy("facility"),
            'hours' => range(0, 23),
            'facilities' => $data->countBy("facility")->sortKeys()->keys()
        ]);
    }
}
