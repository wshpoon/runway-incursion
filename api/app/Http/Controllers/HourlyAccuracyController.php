<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\HourlyAccuracyRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HourlyAccuracyController extends Controller
{
    protected $repository;

    /**
     * @return HourlyAccuracyRepositoryInterface $repository
     */
    public function __construct(HourlyAccuracyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        return response()->json([
            'data' => $data,
            'facilities' => $data->countBy('facility')->sortKeys()->keys()
        ]);
    }
}
