<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\SevenDayRollingOperationRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SevenDayRollingOperationController extends Controller
{
    /**
     * @var SevenDayRollingOperationRepositoryInterface
     */
    protected $repository;

    /**
     * @param SevenDayRollingOperationRepositoryInterface $repository
     */
    public function __construct(
        SevenDayRollingOperationRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(
        string $facility,
        string $minDate,
        string $maxDate
    ): JsonResponse {
        return response()->json(
            $this->repository->get(
                $facility,
                Carbon::parse($minDate),
                Carbon::parse($maxDate)
            )
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        $labels = $data->countBy('day')->keys()->all();

        $dates = $data->countBy('date')->keys()->all();

        return response()->json([
            'years' => $data->groupBy('year'),
            'labels' => $labels,
            'dates' => $dates,
        ]);
    }
}
