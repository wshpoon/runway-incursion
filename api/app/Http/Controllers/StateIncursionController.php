<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\StateIncursionRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StateIncursionController extends Controller
{
    protected $repository;

    /**
     * @return StateIncursionRepositoryInterface $repository
     */
    public function __construct(StateIncursionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        return response()->json([
            'query_results' => $data,
            'states' => $data->countBy('state')->keys()
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function alternative(): JsonResponse
    {
        $data = $this->repository->getAlternative();

        return response()->json([
            'data' => $data,
            'states' => $data->countBy('state')->keys()
        ]);
    }
}
