<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\IncursionTotalRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IncursionTotalController extends Controller
{
    protected $repository;

    /**
     * @return IncursionTotalRepositoryInterface $repository
     */
    public function __construct(IncursionTotalRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        $grouped = $data->groupBy("acft_1_rwy_sfty_type");

        return response()->json([
            'data' => $grouped,
            'facilities' => $data->countBy("acft_1_rwy_sfty_type")->sortKeys()->keys()
        ]);
    }
}
