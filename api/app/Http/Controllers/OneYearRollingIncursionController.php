<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\OneYearRollingOperationRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OneYearRollingIncursionController extends Controller
{
    protected $repository;

    /**
     * @return OneYearRollingOperationRepositoryInterface $repository
     */
    public function __construct(OneYearRollingOperationRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        $years = $data->pluck('date')->map(function ($date) {
            return \Carbon\Carbon::parse($date)->year;
        })->countBy()->sortKeys()->keys();

        return response()->json([
            'data' => $data,
            'years' => $years,
            'facilities' => $data->countBy("facility")->sortKeys()->keys()
        ]);
    }
}
