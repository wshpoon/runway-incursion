<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\TypeAndGradeRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TypeAndGradeController extends Controller
{
    protected $repository;

    /**
     * @return TypeAndGradeRepositoryInterface $repository
     */
    public function __construct(TypeAndGradeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->repository->get();

        return response()->json([
            'data' => $data->groupBy('rwy_sfty_ri_cat_rnk_code')->sortKeys(),
            'labels' => $data->countBy('incdnt_type_faa_code')->sortKeys()->keys(),
            'types' => $data->countBy('acft_1_type')->sortKeys()->keys(),
        ]);
    }
}
