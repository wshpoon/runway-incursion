<?php

namespace App\Import;

use App\Incursion;

class ImportIncursion extends BaseImport
{
	/**
	 * Save the Incursion data into DB.
	 * 
	 * @param array $data
	 * @return Incursion
	 */
	public function create(array $data): Incursion 
	{
		return Incursion::create([
			'event_id' => $data[0],
			'incdnt_type_faa_code' => $data[1],
			'operational' => $data[2],
			'aircraft' => $data[3],
			'other' => $data[4],
			'event_lcl_date' => $data[5],
			'rwy_sfty_ri_cat_rnk_code' => $data[6],
			'a' => $data[7],
			'b' => $data[8],
			'c' => $data[9],
			'd' => $data[10],
			'e' => $data[11],
			'p' => $data[12],
			'unk' => $data[13],
			'event_arpt_id' => $data[14],
			'event_loc_desc' => $data[15],
			'acft_1_rwy_sfty_type' => $data[16],
			'acft_1_type' => $data[17],
			'runway_incursion' => $data[18],
			'acft_2_rwy_sfty_type' => $data[19],
			'acft_1_fltcndt_code' => $data[20],
			'acft_2_fltcndt_code' => $data[21],
			'wx_cond_desc' => $data[22],
			'event_tkof_lndg_desc' => $data[23],
			'metar_time' => $data[24],
			'airport_location' => $data[25],
		]);
	}
}
