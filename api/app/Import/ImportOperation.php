<?php

namespace App\Import;

// ini_set('max_execution_time', '0');

use App\Operation;
use Carbon\Carbon;

class ImportOperation extends BaseImport
{
    /**
     * Save the Operation data into DB.
     *
     * @param array $data
     * @return Operation
     */
    public function create(array $data)
    {
        return Operation::create([
            'date' => $data['Date'],
            'facility' => $data['Facility'],
            'state' => $data['State'],
            'ddso_service_area' => $data['DDSO Service Area'],
            'class' => $data['Class'],
            'region' => $data['Region'],
            'ifr_itinerant_air_carrier' => $data['IFR Itinerant Air Carrier'],
            'ifr_itinerant_air_taxi' => $data['IFR Itinerant Air Taxi'],
            'ifr_itinerant_general_aviation' => $data['IFR Itinerant General Aviation'],
            'ifr_itinerant_military' => $data['IFR Itinerant Military'],
            'ifr_itinerant_total' => $data['IFR Itinerant Total'],
            'vfr_itinerant_air_carrier' => $data['VFR Itinerant Air Carrier'],
            'vfr_itinerant_air_taxi' => $data['VFR Itinerant Air Taxi'],
            'vfr_itinerant_general_aviation' => $data['VFR Itinerant General Aviation'],
            'vfr_itinerant_military' => $data['VFR Itinerant Military'],
            'vfr_itinerant_total' => $data['VFR Itinerant Total'],
            'itinerant_air_carrier' => $data['Itinerant Air Carrier'],
            'itinerant_air_taxi' => $data['Itinerant Air Taxi'],
            'itinerant_general_aviation' => $data['Itinerant General Aviation'],
            'itinerant_military' => $data['Itinerant Military'],
            'itinerant_total' => $data['Itinerant Total'],
            'local_civil' => $data['Local Civil'],
            'local_military' => $data['Local Military'],
            'local_total' => $data['Local Total'],
            'total_operations' => $data['Total Operations'],
        ]);
    }

    /**
     * Save the Operation data into DB.
     *
     * @deprecated Now using FastExcel package.
     *
     * @param array $data
     * @return Operation
     */
    public function createDeprecated(array $data): Operation
    {
        return Operation::create([
            'date' => $data[0],
            'facility' => $data[1],
            'state' => $data[2],
            'ddso_service_area' => $data[3],
            'class' => $data[4],
            'region' => $data[5],
            'ifr_itinerant_air_carrier' => $data[6],
            'ifr_itinerant_air_taxi' => $data[7],
            'ifr_itinerant_general_aviation' => $data[8],
            'ifr_itinerant_military' => $data[9],
            'ifr_itinerant_total' => $data[10],
            'vfr_itinerant_air_carrier' => $data[11],
            'vfr_itinerant_air_taxi' => $data[12],
            'vfr_itinerant_general_aviation' => $data[13],
            'vfr_itinerant_military' => $data[14],
            'vfr_itinerant_total' => $data[15],
            'itinerant_air_carrier' => $data[16],
            'itinerant_air_taxi' => $data[17],
            'itinerant_general_aviation' => $data[18],
            'itinerant_military' => $data[19],
            'itinerant_total' => $data[20],
            'local_civil' => $data[21],
            'local_military' => $data[22],
            'local_total' => $data[23],
            'total_operations' => $data[24],
        ]);
    }
}
