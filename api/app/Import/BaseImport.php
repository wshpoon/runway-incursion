<?php

namespace App\Import;

use Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;

class BaseImport
{
    /**
     * Import local CSV file then save to DB.
     *
     * @param string $path
     * @return array
     */
    public function execute(string $path = "")
    {
        return (new FastExcel())->import($path, function ($data) {
            // $date = Carbon::parse($data['Date']);

            // info("[BaseImport] Attempting to import data of date " . $date->format('Y-m-d'));

            // if ($this->dateInRange($date)) {
            //     info("[BaseImport] Date is acceptable (" . $date->format('Y-m-d') . ")");

            //     return $this->create($data);
            // }

            // return null;
            return $this->create($data);
        });
    }

    /**
     * Checks if date is in range.
     *
     * @param Carbon $date
     * @return boolean
     */
    public function dateInRange(Carbon $date)
    {
        // slowly increment the month number
        // for everytime it maxes out
        // just how I dealt with importing
        // a million rows of data
        //
        // current count: 1215818
        // latest data date: "2007-10-09" (id: 1226500)
        $minDate = now()->year(2007)->month(1)->startOfMonth();
        $maxDate = now()->year(2007)->month(12)->endOfMonth();

        if ($date >= $minDate && $date <= $maxDate) return true;

        return false;
    }

    /**
     * Import local CSV file then save to DB.
     *
     * @deprecated Now using FastExcel package.
     *
     * @param string $path
     * @return array
     */
    public function executeDeprecated(
        string $path = "/home/carlomigueldy/Documents/Runway Incursion/airport-information.csv"
    ): array {
        info('[BaseImport] The path provided is ' . $path);

        $file = fopen($path, "r");

        $array = [];

        while (!feof($file)) {
            $record = fgetcsv($file);

            if (gettype($record) == 'array') {
                array_push($array, $record);
            }
        }

        info('[BaseImport] The number of rows from the CSV is ' . count($array));

        foreach ($array as $key => $data) {
            if ($key != 0) {
                $this->create($data);
            }
        }

        return $array;
    }

    /**
     * Save the Incursion data into DB.
     *
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        return $data;
    }
}
